<?php
class EmailMessage
{
	protected $body;
	protected $subject;
	protected $email_host;
	protected $email_username;
	protected $email_password;
	protected $from_email;
	protected $from_name;

    public function __construct($subject, $body, $from_email=NULL, $from_name=NULL){
		$this->email_host = EMAIL_HOST;
		$this->email_username = EMAIL_USERNAME;
		$this->email_password = EMAIL_PASSWORD;

		$this->from_email = is_null($from_email) ? EMAIL_FROM_ADDRESS : $from_email;
		$this->from_name = is_null($from_name) ? EMAIL_FROM_NAME : $from_name;

        $this->body = $body;
        $this->subject = $subject;
    }

	function getSubject(){
		return $this->subject;
	}
	function getBody(){
		return $this->body;
	}

	function send($to_email, $bccs = array()){
		$smtp = new PHPMailer();
		$smtp->IsSMTP();
		$smtp->Host     = $this->email_host;
		$smtp->SMTPAuth = true;
		$smtp->Username = $this->email_username;
		$smtp->Password = $this->email_password;
		$smtp->From     = $this->from_email;
		$smtp->FromName = $this->from_name;
		$smtp->IsHTML(true);

		foreach($bccs as $bcc){
			$smtp->addBcc($bcc);
		}

		if(is_array($to_email)){
			foreach($to_email as $email){
				$smtp->AddAddress($email);
			}
		}else{
			$smtp->AddAddress($to_email);
		}

		$smtp->Subject = $this->getSubject();
		$smtp->Body = $this->getBody();

		$send_result = $smtp->Send();

		return $send_result;
	}
}

class TwigMailGenerator
{
    static function render($identifier, $parameters = array()){
    	$twigEnv = self::getTwigEnvironment();
        $template = $twigEnv->loadTemplate('/email/' . $identifier . '.twig');

        $subject = $template->renderBlock('subject', $parameters);
        $bodyHtml = $template->renderBlock('body_html', $parameters);

        $message = new EmailMessage($subject, $bodyHtml);

        return $message;
    }

    static function getTwigEnvironment(){
		$app = \Slim\Slim::getInstance();
		$view = $app->view();
		$twigEnv = $view->getEnvironment();

		return $twigEnv;
	}
}
