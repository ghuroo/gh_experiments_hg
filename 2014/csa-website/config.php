<?php
/*************** SESSION ***************/
session_name('csa');
session_start();

/*************** CONSTANTS ***************/
define('PRODUCTIONSERVER', true);
define('LANG', 'pt');

if(PRODUCTIONSERVER) {
	define('BASE', 'http://csa.ghuroo.com/');
	define('DEBUGMODE', false);
}else{
	define('BASE', '/csa-website/www/');
	define('DEBUGMODE', true);
}

define('ASSETS', BASE.'assets/');
define('UPLOADS', BASE.'admin/uploads/');

define('c_name', 'csa-language');
$c_val = '_pt';

if (empty($_COOKIE[c_name])) {

	$c_val = '_pt';
	setcookie(c_name, $c_val, time() + (86400 * 30), "/");

} else { $c_val = $_COOKIE[c_name]; }

// AUX PATHS
if(!defined('ABSPATH')){define('ABSPATH', dirname(__FILE__));}
define('LIBPATH', ABSPATH . '/libs');
define('VENDORPATH', ABSPATH . '/vendor');
define('MODELSPATH', ABSPATH . '/models');
define('VIEWSPATH', ABSPATH . '/views');
define('CONTROLLERPATH', ABSPATH . '/controllers');
define('LANGPATH', ABSPATH . '/lang');

// EMAIL
define('EMAIL_FROM_ADDRESS', 'app.hora.certa@gmail.com');
define('EMAIL_FROM_NAME', 'Hora Certa');
define('EMAIL_HOST', 'smtp.gmail.com');
define('EMAIL_USERNAME', 'app.hora.certa@gmail.com');
define('EMAIL_PASSWORD', '8OP,L1cQCy9MsOx');
define('EMAIL_ASSETS', ABSPATH . '/assets/img/');
define('ANALYTICS', '');


/*************** DEBUGGING ***************/
DEBUGMODE ?
ini_set('display_errors', '1') :
ini_set('display_errors', 0);

/*************** LOAD SLIM ***************/
// Slim PHP
require(VENDORPATH . '/Slim/Slim.php');
\Slim\Slim::registerAutoloader();

// Twig
require(VENDORPATH . '/Twig/Autoloader.php');
Twig_Autoloader::register();

// Twig [Template]
foreach (glob(VENDORPATH . '/Slim-Views-master/*.php') as $filename) { require_once $filename; }

/*************** APP SETUP **************/
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
));

/*************** APP CONFIG ***************/
$view = $app->view();
$view->parserOptions = array( 'debug' => DEBUGMODE );
$view->twigTemplateDirs = array( realpath(VIEWSPATH) );
$view->parserExtensions = array(
    new \Slim\Views\TwigExtension(),
);

$app->getLog()->setEnabled(PRODUCTIONSERVER);

/*************** PUBLIC LIBS ***************/
require(VENDORPATH . '/RedBean/rb.php');

/*************** MY LIBS ***************/
foreach (glob(LIBPATH . '/*.php') as $filename) { require_once $filename; }

/*************** MODELS ***************/
foreach (glob(MODELSPATH . '/*.php') as $filename) { require_once $filename; }

/*************** CONTROLLERS ***************/
foreach (glob(CONTROLLERPATH . '/*.php') as $filename) { require_once $filename; }

/*************** MYSQL CONFIG ***************/
PRODUCTIONSERVER ?
R::setup('mysql:host=ghuroo.com;dbname=ghuroo_csa','ghuroo_root','x&RXr6lhNpoB') :
R::setup('mysql:host=localhost;dbname=ghuroo_csa', 'root', '');
R::setStrictTyping(false);

