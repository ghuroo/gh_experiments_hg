<?php
define('VERSION', '2.3');
define('ABSPATH', dirname(__FILE__));
require(ABSPATH . '/config.php');

/*************** EXCEPTIONS ***************/
$app->notFound(function () use ($app) {
  $data['section'] = 'f0f';

  $app->render('exceptions/404.twig', $data);
});

$app->error(function (\Exception $e) use ($app) {
  $data['section'] = 'error';
  $data['error'] = $e;

  $app->render('exceptions/error.twig', $data);
});

/*************** APP INIT ***************/
$app->run();
