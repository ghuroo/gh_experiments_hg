<?php
// HOME
$app->get('/(home)(/)', function () use($app){
  $data = array();
  $refresh = Fn::setLang($app->request()->get());
  if ($refresh) { $app->redirect(BASE.'home/'); }

  $data['section'] = 'home';
  $data['title'] = '';
  $data['categories'] = Categories::ReadAll();
  $data['banners'] = Banners::ReadAll();
  $data['locale'] = Locale::Read();

  if (empty($_COOKIE['csa-language'])) {
    $data['lang'] = '_pt';
  } else {
    $data['lang'] = $_COOKIE['csa-language'];
  }

  render('partials/home', $data);

})->name('home');

// ABOUT
$app->get('/about(/)', function () use($app){
  $data = array();
  $refresh = Fn::setLang($app->request()->get());
  if ($refresh) { $app->redirect(BASE.'about/'); }

  if (empty($_COOKIE['csa-language'])) {
    $data['lang'] = '_pt';
  } else {
    $data['lang'] = $_COOKIE['csa-language'];
  }

  $data['lang'] == '_pt' ? $l = '' : $l = '_en';
  $data['area'] = Areas::Read(1);
  $data['section'] = 'about';
  $data['title'] = ' - '.$data['area']['title'.$l];
  $data['categories'] = Categories::ReadAll();
  $data['locale'] = Locale::Read();

  render('partials/generic', $data);

})->name('about');

// SERVICES
$app->get('/services(/)', function () use($app){
  $data = array();
  $refresh = Fn::setLang($app->request()->get());
  if ($refresh) { $app->redirect(BASE.'services/'); }

  if (empty($_COOKIE['csa-language'])) {
    $data['lang'] = '_pt';
  } else {
    $data['lang'] = $_COOKIE['csa-language'];
  }

  $data['lang'] == '_pt' ? $l = '' : $l = '_en';
  $data['area'] = Areas::Read(2);
  $data['section'] = 'services';
  $data['title'] = ' - '.$data['area']['title'.$l];
  $data['categories'] = Categories::ReadAll();
  $data['locale'] = Locale::Read();

  render('partials/generic', $data);

})->name('services');

// LOCATION
$app->get('/location(/)', function () use($app){
  $data = array();
  $refresh = Fn::setLang($app->request()->get());
  if ($refresh) { $app->redirect(BASE.'location/'); }

  if (empty($_COOKIE['csa-language'])) {
    $data['lang'] = '_pt';
  } else {
    $data['lang'] = $_COOKIE['csa-language'];
  }

  $data['lang'] == '_pt' ? $l = '' : $l = '_en';
  $data['area'] = Areas::Read(3);
  $data['section'] = 'location';
  $data['title'] = ' - '.$data['area']['title'.$l];
  $data['categories'] = Categories::ReadAll();
  $data['locale'] = Locale::Read();

  render('partials/generic', $data);

})->name('location');

// CATEGORIES
$app->get('/categories(/)', function () use($app){
  $data = array();
  $refresh = Fn::setLang($app->request()->get());
  if ($refresh) { $app->redirect(BASE.'categories/'); }

  if (empty($_COOKIE['csa-language'])) {
    $data['lang'] = '_pt';
  } else {
    $data['lang'] = $_COOKIE['csa-language'];
  }

  $data['lang'] == '_pt' ? $l = '' : $l = '_en';
  $data['area'] = Areas::Read(6);
  $data['section'] = 'categories';
  $data['title'] = ' - '.$data['area']['title'.$l];
  $data['categories'] = Categories::ReadAll();
  $data['locale'] = Locale::Read();

  $w = count($data['categories']);
  if ($w > 0) { $w = 100/$w; } else { $w = '100'; }

  $data['width'] = $w;

  render('partials/categories', $data);

})->name('categories');

// CATEGORY
$app->get('/category/:title(/)', function ($title = NULL) use($app){
  $data = array();
  $refresh = Fn::setLang($app->request()->get());
  if ($refresh) { $app->redirect(BASE.'category/'.$title); }

  $data['section'] = 'category';
  $data['categories'] = Categories::ReadAll();
  $data['locale'] = Locale::Read();

  $c = Fn::Sanitize($title);
  $data['category'] = Categories::ReadOne($c);
  $data['title'] = ' - '.$data['category']['title'];

  if (empty($data['category']['id'])) {
    $app->redirect(BASE.'categories');
  } else {
    $c_id = $data['category']['id'];
    $data['projects'] = Projects::ReadAll($c_id);

    render('partials/category', $data);
  }

})->name('category');

// PROJECTS
$app->get('/projects(/)', function () use($app){
  $data = array();
  $refresh = Fn::setLang($app->request()->get());
  if ($refresh) { $app->redirect(BASE.'projects/'); }

  $data['section'] = 'projects';
  $data['categories'] = Categories::ReadAll();
  $data['projects'] = Projects::ReadAll("");
  $data['locale'] = Locale::Read();
  $data['area'] = Areas::Read(7);

  foreach ($data['projects'] as $key => $value) {
    $data['projects'][$key]['images'] = json_decode($value['images'], true);
  }

  if (empty($_COOKIE['csa-language'])) {
    $data['lang'] = '_pt';
  } else {
    $data['lang'] = $_COOKIE['csa-language'];
  }

  $data['lang'] == '_pt' ? $l = '' : $l = '_en';
  $data['title'] = ' - '.$data['area']['title'.$l];

  render('partials/projects', $data);

})->name('projects');

// PROJECT
$app->get('/project/:title(/)', function ($title = NULL) use($app){
  $data = array();
  $refresh = Fn::setLang($app->request()->get());
  if ($refresh) { $app->redirect(BASE.'project/'.$title); }

  $data['section'] = 'project';
  $data['categories'] = Categories::ReadAll();
  $data['locale'] = Locale::Read();

  if (empty($_COOKIE['csa-language'])) {
    $data['lang'] = '_pt';
  } else {
    $data['lang'] = $_COOKIE['csa-language'];
  }

  $t = Fn::Sanitize($title);
  $data['project'] = Projects::ReadOne($t);
  $data['title'] = ' - '.$data['project']['title'];

  if (empty($data['project'])) {
    $app->redirect(BASE.'categories');
  } else {
    $d = $data['project']['description'];
    $breaks = array("\r\n", "\n", "\r");
    $data['project']['description'] = str_replace($breaks, "<br/>", $d);

    render('partials/project', $data);
  }

})->name('project');

// CONTACTS
$app->get('/contact(/)', function () use($app){
  $data = array();
  $refresh = Fn::setLang($app->request()->get());
  if ($refresh) { $app->redirect(BASE.'contact/'); }

  $data['section'] = 'contact';
  $data['title'] = ' - Contact';
  $data['categories'] = Categories::ReadAll();
  $data['locale'] = Locale::Read();

  if (empty($_COOKIE['csa-language'])) {
    $data['lang'] = '_pt';
  } else {
    $data['lang'] = $_COOKIE['csa-language'];
  }

  $data['area'] = Areas::Read(5);

  render('partials/generic', $data);

})->name('contact');

// $email-details = array('subject' => 'Subject');
// $email = TwigMailGenerator::render('email-template', $email-details);
// $email->send(example@mail.com);