//PT
var 
LANG = { 
    //BTNs
    'btn-open': 'Abrir',
    'btn-close': 'Fechar',
    //EMAIL
    'email-required': 'Email obrigatório.',
    'email-invalid': 'Email inválido.'
},

MSG = { // MENSAGENS
    'success': {
        'insert': 'O registo foi inserido com sucesso.',
        'delete': 'O registo foi eliminado com sucesso.',
        'send': 'Envio efectuado com sucesso.',
        'suspend': 'O registo foi suspenso com sucesso.',
    },
    
    'error':{
        'required_name': 'Nome obrigatório.',
        'required_email': 'Email obrigatório.',
        'required_password': 'Password obrigatória.',
        'required_text': 'Texto obrigatório.',
        'required_file': 'Ficheiro obrigatório.',
        
        'invalid_email': 	'Email inválido.',
        'invalid_password': 'Password inválida.',
        'invalid_file': 	'Inseriu um ficheiro inválido.',
        
        'duplicated_email': 'O email que inseriu já se encontra registado.',
        'file_max_size':    'Excedeu o peso máximo para o ficheiro.',
        'file_upload':    'Erro ao Efetuar o upload do ficheiro.',
        
        'insert': 	'Ocorreu um erro ao inserir o registo.',
        'edit': 	'Ocorreu um erro ao editar o registo.',
        'delete': 	'Ocorreu um erro ao eliminar o registo.',
        'send': 	'Ocorreu um erro no envio.',
        'suspend': 'Ocorreu um erro ao suspender o registo.',
    }
},

ALERT = {
    'success': '<strong>Sucesso!</strong> ',
    'error': '<strong>Erro!</strong> ',
    'warning': '<strong>Atenção!</strong> ',
},

HELP = { 'email': 'Contacte Research & Design.' }