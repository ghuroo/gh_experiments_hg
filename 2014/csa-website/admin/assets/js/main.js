/**
 * GLOBALS
 */
var BASE_PATH = '/',
	DELETE = {URL: false, ID: false};

$(document).ready(function(){

    $('a.banUser').unbind('click').click(function(e){
        e.preventDefault();
        var $this = $(this);
        $url = $this.attr('href');

        $.get($url, function(data){
            if(data.sucesso) alertBox('success', ALERT.success+MSG.success.suspend);
            else{ alertBox('error', ALERT.error+MSG.error.suspend); }
        });
    });
});

/**
 * MAIN
 */

/**
 * AUX
 */
function fastSearch()
{
	var query = validacao = false,
		$form = $('#f_search');

	$form.submit(function(e){ e.preventDefault(); });
	$form.find('button').unbind('click').click(function(e){
		e.preventDefault();

		query = validateInput($form.find('input[name="q"]'));

		var validacao = query;

		if( validacao ) window.location.href = $form.attr('action')+$form.find('input[name="q"]').val();
	});
}
function readModalbox()
{
	var $id,
		$a = $('[href=#myModal-read]');

	$.each($a, function(){
		var $this = $(this);
		$this.unbind('click').click(function(e){
			e.preventDefault();
			$url = $this.data('url');

			$.get($url, function(data){
				$('#myModal-read .modal-body').html(data.html);
				if(data.btns) $('#myModal-read .modal-footer').html(data.btns);
				$('#myModal-read').modal('show');
			});
		});
	});
}
function deleteModalbox()
{
	var $a = $('[data-delete-url]');

	$.each($a, function(){
		var $this = $(this);
		$this.click(function(){
			DELETE.URL = $this.data('delete-url');
			DELETE.ID = $this.data('id');
		});
	});

	deleteModalboxSubmit();
}
function deleteModalboxSubmit()
{
	$('#myModal-delete .btn-warning').click(function(){
		if(DELETE.URL && DELETE.ID){
			$.post(	DELETE.URL,
					{id: DELETE.ID},
					function(data){
						if(data.success){
							alertBox('success', ALERT.success+MSG.success.delete);
							$('table [data-id="'+DELETE.ID+'"]').parents('tr').remove();
						}else{
							alertBox('error', ALERT.error+MSG.error.delete);
						}
						$('#myModal-delete [data-dismiss="modal"]').trigger('click');
						DELETE.URL = DELETE.ID = null;
					}
			);
		}
	});
}
function gotoModalBox()
{
	$('#myModal-goto .btn-success').click(function(){
		window.location.href = $(this).data('url');
	});
}
function alertBox(_type, _msg)
{
	var $holder = $('#alert_container'),
		$alert;

	$holder.empty();
	$holder.append('<div class="alert"><a type="button" class="close" data-dismiss="alert">&times;</a></div>');

	var $alert = $holder.find('.alert');
	$alert.append(_msg);
	switch(_type){
		case 'error': $alert.addClass('alert-danger'); break;
		case 'success': $alert.addClass('alert-success'); break;
		case 'info': $alert.addClass('alert-info'); break;
	}
}

/**
 * HELPERS
 */
function tr(_i)
{
	return LANG[_i];
}
function validateEmail(_input)
{
	var r = false, re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	r = re.test(_input.val());
	r ? _input.removeClass('error') : _input.addClass('error');
    return r;
}
function validateInput(_input)
{
	var r = ph = val = false;

	$.each(_input.val().split(' '), function(k, v){ if(v!=''){ val = true; } });

	if(_input.hasClass('placeholder')){ ph = _input.attr('placeholder'); }
	if(val){
		if(ph){
			if(_input.val()!=ph) { r = true; }
			else{ r = false; }
		}
		else{ r = true; }
	}
    else { r = false; }

	r ? _input.removeClass('error') : _input.addClass('error');
	return r;
}
function setupForms()
{
	var $selects = $('select.themed');
	setupSelect($selects);
	var $inputs = $('input[type="checkbox"].themed');
	setupInputCheckbox($inputs);
	$inputs = $('input[type="radio"].themed');
	setupInputRadio($inputs);
	$inputs = $('input[type="file"].themed');
	setupInputFile($inputs);
}
function setupSelect($els)
{
	var wrap = '<div class="select-wrap" />',
		extras = '<div class="select-content"></div><div class="select-btn"></div>';

	$els.each(function(){
		var $el = $(this),
			$wrap = $el.wrap(wrap).parent().append(extras);

		$el.change(function(){
			var $el = $(this),
				$optSel = $el.find(':selected'),
				$wrap = $el.parents('.select-wrap'),
				$content = $('.select-content', $wrap);
			$content.text($optSel.text());
		}).change();
	});

	return $els;
}
function setupInputCheckbox($els)
{
	var wrap = '<div class="input-checkbox-wrap" />';

	$els.each(function(){
		var $el = $(this),
			$wrap = $el.wrap(wrap).parent();

		if($el.prop('checked')) $el.parent().addClass('active');

		$el.click(function(e)
		{
			var $this = $(this);
			if($this.parent().hasClass('active')){
				$this.parent().removeClass('active');
				$this.prop('checked', false);
			}else{
				$this.parent().addClass('active');
				$this.prop('checked', true);
			}
		});
	});

	return $els;
}
function setupInputRadio($els)
{
	var wrap = '<div class="input-radio-wrap" />';

	$els.each(function(){
		var $el = $(this),
			$wrap = $el.wrap(wrap).parent();

		if($el.prop('checked')) $el.parent().addClass('active');

		$el.click(function(e)
		{
			var $this = $(this),
				n = $el.attr('name'),
				id = $el.attr('id');

			$('input[type="radio"]').each(function(){
				if($(this).attr('name')==n){
					$(this).prop('checked', false).parent().removeClass('active');
					var i = $(this).attr('id');
					$('label').each(function(){
						if($(this).attr('for')==i){ $(this).removeClass('active'); }
					});
				}
			});
			$this.prop('checked', true).parent().addClass('active');
			$('label').each(function(){
				if($(this).attr('for')==id){ $(this).addClass('active'); }
			});
		});
	});

	return $els;
}
function setupInputFile($els)
{
	var wrap = '<div class="input-file-wrap"/>',
		extras = '<div class="input-content"></div><div class="input-btn"></div>';

	$els.each(function(){
		var $el = $(this), $wrap;
		$wrap = $el.wrap(wrap).parent().append(extras);

		$el.change(function()
		{
			var $el = $(this),
				$fileName = $el.val(),
				$wrap = $el.parents('.input-file-wrap'),
				$content = $('.input-content', $wrap);
			$content.text($fileName);
		}).change();
	});

	return $els;
}

// Custom