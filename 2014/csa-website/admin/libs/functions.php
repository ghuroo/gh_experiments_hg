<?php
/*************** APP ***************/
function render($template, $data = array()){
  $app = \Slim\Slim::getInstance();

	if(!isset($_SESSION['user']) && $data['area']!='login'){ $app->redirect(BASE); }
	else {
		$dataExtra = array(	'current_page_url' => getCurrentUrl() );

		if(isset($_SESSION['user'])) $dataExtra['SessionUser'] = $_SESSION['user'];

		$app->render($template.'.twig', array_merge($dataExtra, $data));
	}
}
function renderJSON($json){
  header('Cache-Control: no-cache, must-revalidate');
  header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
  header('Content-type: application/json');
  print(is_array($json) ? json_encode($json) : $json);
  die;
}
function getCurrentUrl(){
    $pageURL = (@$_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
	$pageURL .= $_SERVER['SERVER_NAME'];

    if ($_SERVER['SERVER_PORT'] != '80')
		{ $pageURL .= ':'.$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI']; }
	else
		{ $pageURL .= $_SERVER['REQUEST_URI']; }

    return $pageURL;
}
function checkPermissions(){
	if($_SESSION['user']['supplier_id']>0){
    	$app = \Slim\Slim::getInstance();
    	$app->redirect(BASE);
	}
}
/*************** XTRAS ***************/
function dump($dump){
	echo'<pre>';print_r($dump);echo'</pre>';
}
function permalink($str){
	$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
	$clean = preg_replace('/[^a-zA-Z0-9\/_| -]/', '', $clean);
	$clean = strtolower(trim($clean, '-'));
	$clean = preg_replace('/[\/_| -]+/', '-', $clean);

	return $clean;
}
function validateEmail($email){
	if(preg_match('/^[A-Z0-9._%-]+@[A-Z0-9][A-Z0-9.-]{0,61}[A-Z0-9]\.[A-Z]{2,6}$/i', $email) > 0)
		{ return true; }
	else
		{ return false; }
}
function randomString($length, $letters=true, $numbers=true){
	$chars = '';

	if($letters) $chars .= 'abcdefghijklmnopqrstuvwxyz';
	if($numbers) $chars .= '0123456789';

	return substr(str_shuffle($chars), 0, $length);
}
function validaURL($url){
	return preg_match('/^(([\w]+:)?\/\/)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&amp;?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/', $url);
}
function truncate($str, $limit, $append='...'){
	if(strlen($str) > $limit) {
		$str = substr($str, 0, $limit);
		$str .= $append;
	}

	return $str;
}
function checkFileExtention($filename=NULL, $allowed=array()){
	$return = false;

	if(!empty($filename) && !empty($allowed)){
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if(in_array($ext, $allowed) ) $return = true;
	}

	return $return;
}
function uploadFile($file, $dir, $name, $ext){
	ini_set('memory_limit','100M');

	// memory_limit 100M
	// upload_max_filesize 50M
	// post_max_size 50M

	if(move_uploaded_file($file['tmp_name'], $dir.$file['name'])){
		$namefile = $name.'.'.$ext;
		rename($dir.$file['name'], $dir.$namefile);
		return $namefile;
	}else{
		return false;
	}
}

// Sanitize
function Sanitize($text) {
    // 1) convert á ô => a o
    $text = preg_replace("/[áàâãªä]/u","a",$text);
    $text = preg_replace("/[ÁÀÂÃÄ]/u","A",$text);
    $text = preg_replace("/[ÍÌÎÏ]/u","I",$text);
    $text = preg_replace("/[íìîï]/u","i",$text);
    $text = preg_replace("/[éèêë]/u","e",$text);
    $text = preg_replace("/[ÉÈÊË]/u","E",$text);
    $text = preg_replace("/[óòôõºö]/u","o",$text);
    $text = preg_replace("/[ÓÒÔÕÖ]/u","O",$text);
    $text = preg_replace("/[úùûü]/u","u",$text);
    $text = preg_replace("/[ÚÙÛÜ]/u","U",$text);
    $text = preg_replace("/[’‘‹›‚]/u","'",$text);
    $text = preg_replace("/[“”«»„]/u",'"',$text);
    $text = str_replace("–","-",$text);
    $text = str_replace(" "," ",$text);
    $text = str_replace("ç","c",$text);
    $text = str_replace("Ç","C",$text);
    $text = str_replace("ñ","n",$text);
    $text = str_replace("Ñ","N",$text);
    $text = str_replace(".","-",$text);
    $text = str_replace("|","-",$text);

    //2) Translation CP1252. &ndash; => -
    $trans = get_html_translation_table(HTML_ENTITIES);
    $trans[chr(130)] = '&sbquo;';    // Single Low-9 Quotation Mark
    $trans[chr(131)] = '&fnof;';    // Latin Small Letter F With Hook
    $trans[chr(132)] = '&bdquo;';    // Double Low-9 Quotation Mark
    $trans[chr(133)] = '&hellip;';    // Horizontal Ellipsis
    $trans[chr(134)] = '&dagger;';    // Dagger
    $trans[chr(135)] = '&Dagger;';    // Double Dagger
    $trans[chr(136)] = '&circ;';    // Modifier Letter Circumflex Accent
    $trans[chr(137)] = '&permil;';    // Per Mille Sign
    $trans[chr(138)] = '&Scaron;';    // Latin Capital Letter S With Caron
    $trans[chr(139)] = '&lsaquo;';    // Single Left-Pointing Angle Quotation Mark
    $trans[chr(140)] = '&OElig;';    // Latin Capital Ligature OE
    $trans[chr(145)] = '&lsquo;';    // Left Single Quotation Mark
    $trans[chr(146)] = '&rsquo;';    // Right Single Quotation Mark
    $trans[chr(147)] = '&ldquo;';    // Left Double Quotation Mark
    $trans[chr(148)] = '&rdquo;';    // Right Double Quotation Mark
    $trans[chr(149)] = '&bull;';    // Bullet
    $trans[chr(150)] = '&ndash;';    // En Dash
    $trans[chr(151)] = '&mdash;';    // Em Dash
    $trans[chr(152)] = '&tilde;';    // Small Tilde
    $trans[chr(153)] = '&trade;';    // Trade Mark Sign
    $trans[chr(154)] = '&scaron;';    // Latin Small Letter S With Caron
    $trans[chr(155)] = '&rsaquo;';    // Single Right-Pointing Angle Quotation Mark
    $trans[chr(156)] = '&oelig;';    // Latin Small Ligature OE
    $trans[chr(159)] = '&Yuml;';    // Latin Capital Letter Y With Diaeresis
    $trans['euro'] = '&euro;';    // euro currency symbol
    ksort($trans);

    foreach ($trans as $k => $v) {
        $text = str_replace($v, $k, $text);
    }

    // 3) remove <p>, <br/> ...
    $text = strip_tags($text);

    // 4) &amp; => & &quot; => '
    $text = html_entity_decode($text);

    // 5) remove Windows-1252 symbols like "TradeMark", "Euro"...
    $text = preg_replace('/[^(\x20-\x7F)]*/','', $text);

    $targets=array('\r\n','\n','\r','\t');
    $results=array(" "," "," ","");
    $text = str_replace($targets,$results,$text);

    //XML compatible
    /*
    $text = str_replace("&", "and", $text);
    $text = str_replace("<", ".", $text);
    $text = str_replace(">", ".", $text);
    $text = str_replace("\\", "-", $text);
    $text = str_replace("/", "-", $text);
    */

    // Remove spaces, double hyphens and commas
    $text = preg_replace('/-+/', '-', $text);
    $text = str_replace(', ', '-', $text);
    $text = str_replace(' ', '-', $text);
    $text = str_replace("--","-",$text);

    // If last char is a -
    if (substr($text, -1, 1) == "-") { $text = substr($text, 0, -1); }

    // Lower-case
    $text = strtolower($text);

    return ($text);
}