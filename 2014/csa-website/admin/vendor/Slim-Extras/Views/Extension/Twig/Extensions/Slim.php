<?php

class Twig_Extensions_Slim extends \Twig_Extension
{
    public function getName()
    {
        return 'slim';
    }

    public function getFilters()
    {
        return array(
            'truncate' => new Twig_Filter_Function('twig_filter_truncate'),
            'nl2br' => new Twig_Filter_Function('twig_filter_nl2br'),
            'permalink' => new Twig_Filter_Function('twig_filter_permalink'),
            'html_substr' => new Twig_Filter_Function('twig_filter_html_substr'),
        );
    }

    public function getFunctions()
    {
        return array(
            'urlFor' => new \Twig_Function_Method($this, 'urlFor'),
            'tr' => new \Twig_Function_Method($this, 'translate')
        );
    }

    public function urlFor($name, $params = array(), $appName = 'default')
    {
        return \Slim\Slim::getInstance($appName)->urlFor($name, $params);
    }

    public function translate($key, $replacements = array())
    {
       $app = \Slim\Slim::getInstance();
       $view = $app->view();
       return $view->tr($key, $replacements);
    }
}


function twig_filter_truncate($string, $length, $end='...')
{
    if (strlen($string) > $length)
    {
        $length -= strlen($end);
        $string  = substr($string, 0, $length);
        $string .= $end;
    }
    return $string;
}
function twig_filter_nl2br($string)
{
    return nl2br($string);
}
function twig_filter_permalink($string)
{
	$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
	$clean = preg_replace('/[^a-zA-Z0-9\/_| -]/', '', $clean);
	$clean = strtolower(trim($clean, '-'));
	$clean = preg_replace('/[\/_| -]+/', '-', $clean);
	
	return $clean;
}
function twig_filter_html_substr($s, $len=NULL, $suffix='...', $strict=false)
{
	if ( is_null($len) ){ $len = strlen( $s ); }
	
	$f = 'static $strlen=0; 
			if ( $strlen >= ' . $len . ' ) { return "><"; } 
			$html_str = html_entity_decode( $a[1] );
			$subsrt = max(0, (0-$strlen));
			$sublen = ' . ( empty($strict)? '(' . $len . '-$strlen)' : 'max(@strpos( $html_str, "' . ($strict===2?'.':' ') . '", (' . $len . ' - $strlen + $subsrt - 1 )), ' . $len . ' - $strlen)' ) . ';
			$new_str = substr( $html_str, $subsrt,$sublen); 
			$strlen += $new_str_len = strlen( $new_str );
			$suffix = ' . (!empty( $suffix ) ? '($new_str_len===$sublen?"'.$suffix.'":"")' : '""' ) . ';
			return ">" . htmlentities($new_str, ENT_QUOTES, "UTF-8") . "$suffix<";';
	
	return preg_replace( array( "#<[^/][^>]+>(?R)*</[^>]+>#", "#(<(b|h)r\s?/?>){2,}$#is"), "", trim( rtrim( ltrim( preg_replace_callback( "#>([^<]+)<#", create_function(
            '$a',
          $f
        ), ">$s<"  ), ">"), "<" ) ) );
}