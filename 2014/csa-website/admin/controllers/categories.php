<?

/* List */
$app->map('/categories(/)', function() use($app){
	$data = array('area'=>'categories', 'title'=>'Categorias');

	$data['categories'] = Categories::Read();

	$data['count'] = array();
	$data['count']['categories_total'] = count(Categories::Read());
	$data['count']['categories_active'] = count(Categories::Read('active'));
	$data['count']['categories_inactive'] = count(Categories::Read('inactive'));

  render('categories', $data);
})->via('GET', 'POST')->name('categories');

// Read
$app->get('/categories/read/(:id(/))', function ($id=NULL) use($app){
    $data = array('sucesso'=>'falso');

    $category = Categories::ReadOne($id);
    $banner = Images::Read('categories', $id);

    switch ($category["active"]) {
        case 'yes': $category["active"] = 'Sim'; break;
        case 'no': $category["active"] = 'Não'; break;
    }

    $data['html'] = '<p><b>Dados Categoria:</b></p>
                    <ul>
                        <li><b>Título:</b> '.$category['title'].'</li>
                        <li><b>Título EN:</b> '.$category['title_en'].'</li>
                    </ul>';

    $data['html'] .= '<p><b>Actividade:</b></p>
                              <ul>
                                  <li><b>Adicionado em:</b> '.$category['created_at'].'</li>
                                  <li><b>Actualizado em:</b> '.$category['updated_at'].'</li>
                                  <li><b>Activo:</b> '.$category["active"].'</li>
                              </ul>';
    if (!empty($banner)) {
        $img = '<li><img src="'.BASE.'uploads/categories/'.$category['uri'].'/'.$banner[0].'" width="400" height="auto"/></li>';

        $data['html'] .= '<p><b>Banner:</b></p>
                                      <ul class="popup-images">
                                        '.$img.'
                                      </ul>';

    }

    $data['btns'] = '<a class="btn btn-default" data-dismiss="modal">Fechar</a>';

    renderJSON($data);
})->via('GET', 'POST')->name('categories-read');

// Activate
$app->map('/categories/activate/(:id(/))', function ($id=NULL) use($app){

    Categories::Activate($id);

    $data = array(
        'type'=>'success',
        'msg'=>'Alterações guardadas com sucesso.'
    );

    $app->flash('alert', $data);

    $app->redirect(BASE.'categories/');
})->via('GET', 'POST')->name('categories-activate');

// Delete
$app->map('/categories/deactivate/(:id(/))', function ($id=NULL) use($app){

    Categories::Deactivate($id);

    $data = array(
        'type'=>'success',
        'msg'=>'Alterações guardadas com sucesso.'
    );

    $app->flash('alert', $data);

    $app->redirect(BASE.'categories/');
})->via('GET', 'POST')->name('categories-deactivate');

// Update
$app->map('/categories/update/(:id(/))', function ($id=NULL) use($app){
    $data = array('area'=>'categories', 'title'=>'Categorias');

    $data['category'] = Categories::ReadOne($id);

    render('categories-update', $data);
})->via('GET', 'POST')->name('categories-update');

// Update - Save
$app->map('/categories/update/save/(:id(/))', function ($id=NULL) use($app){

    $post = $app->request()->post();

    $title = $post['title'];
    $title_en = $post['title_en'];

    Categories::Update($id, $title, $title_en);

    $data = array(
        'type'=>'success',
        'msg'=>'Alterações guardadas com sucesso.'
    );

    $app->flash('alert', $data);

    $app->redirect(BASE.'categories/update/'.$id);
})->via('GET', 'POST')->name('categories-update-save');

// Create
$app->map('/categories/create/', function () use($app){
    $data = array('area'=>'categories', 'title'=>'Criar Categoria');

    $data['categories'] = Categories::Read();

    render('categories-create', $data);
})->via('GET', 'POST')->name('categories-create');

// Create Save
$app->map('/categories/create/save/', function () use($app){

    $post = $app->request()->post();

    $uri = Sanitize($post['title']);

    $categoryExists = R::getRow('SELECT * FROM categories WHERE uri = "'.$uri.'" AND deleted="no"');

    if (!empty($categoryExists)) {
        $data = array(
            'type'=>'error',
            'msg'=>'Já existe uma categoria com o mesmo nome.'
        );

        $app->flash('alert', $data);

        $app->redirect(BASE.'categories/create/');
    } else {

        $title = $post['title'];

        $id = Categories::Create($title);
    }

    $app->redirect(BASE.'categories/update/'.$id);
})->via('GET', 'POST')->name('categories-create-save');

// Delete
$app->map('/categories/delete/(:id(/))', function ($id = NULL) use($app){
    Categories::Delete($id);

    $data = array(
        'type'=>'success',
        'msg'=>'Categoria removido com sucesso.'
    );

    $app->flash('alert', $data);

    $app->redirect(BASE.'categories/');
})->via('GET', 'POST')->name('categories-delete');