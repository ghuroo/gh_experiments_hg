<?

/* List */
$app->map('/areas(/)', function() use($app){
	$data = array('area'=>'areas', 'title'=>'Áreas');

	$data['areas'] = Areas::Read();

  render('areas', $data);
})->via('GET', 'POST')->name('areas');

// Update
$app->map('/areas/update/(:id(/))', function ($id=NULL) use($app){
    $data = array('area'=>'areas', 'title'=>'Áreas');

    $data['item'] = Areas::Read($id);

    render('areas-update', $data);
})->via('GET', 'POST')->name('areas-update');

// Update - Save
$app->map('/areas/update/save/(:id(/))', function ($id=NULL) use($app){

    $post = $app->request()->post();

    $content = $post['content'];
    $content_en = $post['content_en'];

    Areas::Update($id, $content, $content_en);

    $data = array(
        'type'=>'success',
        'msg'=>'Alterações guardadas com sucesso.'
    );

    $app->flash('alert', $data);

    $app->redirect(BASE.'areas/update/'.$id);
})->via('GET', 'POST')->name('areas-update-save');