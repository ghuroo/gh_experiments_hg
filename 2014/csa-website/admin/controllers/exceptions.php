<?
/*************** 404 ***************/
$app->notFound(function () use ($app) { 
	render('404',array('area'=>'404')); 
});
/*************** ERROR ***************/
$app->error(function (\Exception $e) use ($app) { $app->render('error'); });
