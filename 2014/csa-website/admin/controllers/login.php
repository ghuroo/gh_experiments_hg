<?php

/* Login */
$app->map('/', function () use($app){
    $post = $app->request()->post();
    if(!empty($post['user'])){
        $res = array();
        if($post['user']==='csa-admin' && $post['password']==='9m7VTUJ@M*t#gY'){
            $_SESSION['user']['name'] = 'CSA Admin';
            $app->redirect('projects');
        }else{
            $data = array('area'=>'login', 'title'=>'Login');

            $data['type'] = 'error';
            $data['msg'] = 'Dados incorrectos';

            $app->flash('alert', $data);
            render('login', $data);
        }
    }else{
        $data = array('area'=>'login', 'title'=>'Login');
        render('login', $data);
    }
})->via('GET', 'POST')->name('login');

/* Logout */
$app->map('/logout(/)', function () use($app){
	session_destroy();
	session_start();

	$app->redirect(BASE);
})->via('GET', 'POST')->name('logout');