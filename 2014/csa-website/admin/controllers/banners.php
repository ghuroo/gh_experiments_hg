<?

/* List */
$app->map('/banners(/)', function() use($app){
	$data = array('area'=>'banners', 'title'=>'Banners');

	$data['banners'] = Banners::Read();

	$data['count'] = array();
	$data['count']['banners_total'] = count(Banners::Read());
	$data['count']['banners_active'] = count(Banners::Read('active'));
	$data['count']['banners_inactive'] = count(Banners::Read('inactive'));

  render('banners', $data);
})->via('GET', 'POST')->name('banners');

// Read
$app->get('/banners/read/(:id(/))', function ($id=NULL) use($app){
    $data = array('sucesso'=>'falso');

    $category = Banners::ReadOne($id);
    $banner = Images::Read('banners', $id);

    switch ($category["active"]) {
        case 'yes': $category["active"] = 'Sim'; break;
        case 'no': $category["active"] = 'Não'; break;
    }

    $data['html'] = '<p><b>Dados Banner:</b></p>
                    <ul>
                        <li><b>Título:</b> '.$category['title'].'</li>
                    </ul>';

    $data['html'] .= '<p><b>Actividade:</b></p>
                              <ul>
                                  <li><b>Adicionado em:</b> '.$category['created_at'].'</li>
                                  <li><b>Actualizado em:</b> '.$category['updated_at'].'</li>
                                  <li><b>Activo:</b> '.$category["active"].'</li>
                              </ul>';
    if (!empty($banner)) {
        $img = '<li><img src="'.BASE.'uploads/banners/'.$id.'/'.$banner[0].'" width="400" height="auto"/></li>';

        $data['html'] .= '<p><b>Banner:</b></p>
                                      <ul class="popup-images">
                                        '.$img.'
                                      </ul>';

    }

    $data['btns'] = '<a class="btn btn-default" data-dismiss="modal">Fechar</a>';

    renderJSON($data);
})->via('GET', 'POST')->name('banners-read');

// Activate
$app->map('/banners/activate/(:id(/))', function ($id=NULL) use($app){

    Banners::Activate($id);

    $data = array(
        'type'=>'success',
        'msg'=>'Alterações guardadas com sucesso.'
    );

    $app->flash('alert', $data);

    $app->redirect(BASE.'banners/');
})->via('GET', 'POST')->name('banners-activate');

// Delete
$app->map('/banners/deactivate/(:id(/))', function ($id=NULL) use($app){

    Banners::Deactivate($id);

    $data = array(
        'type'=>'success',
        'msg'=>'Alterações guardadas com sucesso.'
    );

    $app->flash('alert', $data);

    $app->redirect(BASE.'banners/');
})->via('GET', 'POST')->name('banners-deactivate');

// Update
$app->map('/banners/update/(:id(/))', function ($id=NULL) use($app){
    $data = array('area'=>'banners', 'title'=>'Categorias');

    $data['banner'] = Banners::ReadOne($id);

    render('banners-update', $data);
})->via('GET', 'POST')->name('banners-update');

// Update - Save
$app->map('/banners/update/save/(:id(/))', function ($id=NULL) use($app){

    $post = $app->request()->post();

    $title = $post['title'];
    $sequence = $post['sequence'];

    Banners::Update($id, $title, $sequence);

    $data = array(
        'type'=>'success',
        'msg'=>'Alterações guardadas com sucesso.'
    );

    $app->flash('alert', $data);

    $app->redirect(BASE.'banners/update/'.$id);
})->via('GET', 'POST')->name('banners-update-save');

// Create
$app->map('/banners/create/', function () use($app){
    $data = array('area'=>'banners', 'title'=>'Criar Categoria');

    $data['banners'] = Banners::Read();

    render('banners-create', $data);
})->via('GET', 'POST')->name('banners-create');

// Create Save
$app->map('/banners/create/save/', function () use($app){

    $post = $app->request()->post();

    $title = $post['title'];

    $id = Banners::Create($title);

    $app->redirect(BASE.'banners/update/'.$id);
})->via('GET', 'POST')->name('banners-create-save');

// Delete
$app->map('/banners/delete/(:id(/))', function ($id = NULL) use($app){
    Banners::Delete($id);

    $data = array(
        'type'=>'success',
        'msg'=>'Banner removido com sucesso.'
    );

    $app->flash('alert', $data);

    $app->redirect(BASE.'banners/');
})->via('GET', 'POST')->name('banners-delete');