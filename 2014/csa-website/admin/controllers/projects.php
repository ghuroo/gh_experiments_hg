<?

/* List */
$app->map('/projects(/)', function() use($app){
	$data = array('area'=>'projects', 'title'=>'Projectos');

	$data['projects'] = Projects::Read();

	$data['count'] = array();
	$data['count']['projects_total'] = count(Projects::Read());
	$data['count']['projects_active'] = count(Projects::Read('active'));
	$data['count']['projects_inactive'] = count(Projects::Read('inactive'));

    foreach ($data['projects'] as $key => $value) {
        $q = Categories::ReadOne($value['category']);

        if (!empty($q['title'])) {
            $data['projects'][$key]['category_title'] = $q['title'];
        } else {
            $data['projects'][$key]['category_title'] = "n/a";
        }
    }

  render('projects', $data);
})->via('GET', 'POST')->name('projects');

// Read
$app->get('/projects/read/(:id(/))', function ($id=NULL) use($app){
    $data = array('sucesso'=>'falso');

    $project = Projects::ReadOne($id);
	$project['images'] = Images::Read('projects', $id);
    $project['category'] = Categories::ReadOne($project['category']);
    // $project['category_uri'] = Sanitize($project['category']['title']);

    $img = null;
    foreach ($project['images'] as $key => $value) {
    	$img .= '<li><img src="'.BASE.'uploads/projects/'.$project['uri'].'/'.$value.'" width="80" height="80"/></li>';
    }

    switch ($project["active"]) {
    	case 'yes': $project["active"] = 'Sim'; break;
    	case 'no': $project["active"] = 'Não'; break;
    }

    $data['html'] = '<p><b>Dados Projecto:</b></p>
                    <ul>
                        <li><b>Título:</b> '.$project['title'].'</li>
                        <li><b>Descrição:</b> '.$project['description'].'</li>
                        <li><b>Categoria:</b> '.$project['category']['title'].'</li>
                    </ul>';

    $data['html'] .= '<p><b>Dados Projecto EN:</b></p>
                    <ul>
                        <li><b>Título:</b> '.$project['title_en'].'</li>
                        <li><b>Descrição:</b> '.$project['description_en'].'</li>
                        <li><b>Categoria:</b> '.$project['category']['title_en'].'</li>
                    </ul>';

    $data['html'] .= '<p><b>Actividade:</b></p>
					          <ul>
					              <li><b>Adicionado em:</b> '.$project['created_at'].'</li>
					              <li><b>Actualizado em:</b> '.$project['updated_at'].'</li>
					              <li><b>Activo:</b> '.$project["active"].'</li>
					          </ul>';

    $data['html'] .= '<p><b>Imagens ('.count($project['images']).'):</b></p>
                              <ul class="popup-images">
                                '.$img.'
                              </ul>';

    $data['btns'] = '<a class="btn btn-default" data-dismiss="modal">Fechar</a>';

    renderJSON($data);
})->via('GET', 'POST')->name('projects-read');

// Activate
$app->map('/projects/activate/(:id(/))', function ($id=NULL) use($app){

    Projects::Activate($id);

    $data = array(
        'type'=>'success',
        'msg'=>'Alterações guardadas com sucesso.'
    );

    $app->flash('alert', $data);

    $app->redirect(BASE.'projects/');
})->via('GET', 'POST')->name('projects-activate');

// Delete
$app->map('/projects/deactivate/(:id(/))', function ($id=NULL) use($app){

    Projects::Deactivate($id);

    $data = array(
        'type'=>'success',
        'msg'=>'Alterações guardadas com sucesso.'
    );

    $app->flash('alert', $data);

    $app->redirect(BASE.'projects/');
})->via('GET', 'POST')->name('projects-deactivate');

// Update
$app->map('/projects/update/(:id(/))', function ($id=NULL) use($app){
    $data = array('area'=>'projects', 'title'=>'Projectos');

    $data['project'] = Projects::ReadOne($id);
    $data['project']['images'] = Images::Read('projects', $id);
    $data['project']['category'] = Categories::ReadOne($data['project']['category']);

    $data['categories'] = Categories::Read();

    render('projects-update', $data);
})->via('GET', 'POST')->name('projects-update');

// Update - Save
$app->map('/projects/update/save/(:id(/))', function ($id=NULL) use($app){

    $post = $app->request()->post();

    $title = $post['title'];
    $title_en = $post['title_en'];
    $description = $post['description'];
    $description_en = $post['description_en'];
    $category = $post['category'];

    Projects::Update($id, $title, $title_en, $description, $description_en, $category);

    $data = array(
        'type'=>'success',
        'msg'=>'Alterações guardadas com sucesso.'
    );

    $app->flash('alert', $data);

    $app->redirect(BASE.'projects/update/'.$id);
})->via('GET', 'POST')->name('projects-update-save');

// Create
$app->map('/projects/create/', function () use($app){
    $data = array('area'=>'projects', 'title'=>'Criar Projecto');

    $data['categories'] = Categories::Read();

    render('projects-create', $data);
})->via('GET', 'POST')->name('projects-create');

// Create Save
$app->map('/projects/create/save/', function () use($app){

    $post = $app->request()->post();

    $uri = Sanitize($post['title']);

    $projectExists = R::getRow('SELECT * FROM projects WHERE uri = "'.$uri.'" AND deleted="no"');

    if (!empty($projectExists)) {
        $data = array(
            'type'=>'error',
            'msg'=>'Já existe um projecto com o mesmo nome.'
        );

        $app->flash('alert', $data);

        $app->redirect(BASE.'projects/create/');
    } else {

        $title = $post['title'];
        $description = $post['description'];
        $title_en = $post['title_en'];
        $description_en = $post['description_en'];
        $category = $post['category'];

        $id = Projects::Create($title, $title_en, $description, $description_en, $category);
    }

    $app->redirect(BASE.'projects/update/'.$id);
})->via('GET', 'POST')->name('projects-create-save');

// Delete
$app->map('/projects/delete/(:id(/))', function ($id = NULL) use($app){
    Projects::Delete($id);

    $data = array(
        'type'=>'success',
        'msg'=>'Projecto removido com sucesso.'
    );

    $app->flash('alert', $data);

    $app->redirect(BASE.'projects/');
})->via('GET', 'POST')->name('projects-delete');