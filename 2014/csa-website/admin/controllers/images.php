<?php

// Images - Delete
$app->map('/images/delete/(:type/:id/:name(/))', function ($type = NULL, $id=NULL, $name = NULL) use($app){

    Images::Delete($type, $id, $name);

    $data = array(
        'type'=>'success',
        'msg'=>'Alterações guardadas com sucesso.'
    );

    $app->flash('alert', $data);

    $app->redirect(BASE.$type.'/update/'.$id);
})->via('GET', 'POST')->name('images-delete');