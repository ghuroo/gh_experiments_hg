<?php

// imageUpload
$app->map('/upload(/:id(/))', function ($id=NULL) use($app){

    $post = $app->request()->post();

    $beanImages = Images::Read($post['type'], $id); //get images
    if (empty($beanImages)) { $beanImages = array(); }

    if (!isset($_FILES['uploads'])) {
        echo "Não seleccionou nenhum ficheiro!";
        return;
    }

    $path = 'uploads/'.$post['type'].'/'.$post['uri'].'/';

    if (!file_exists($path)) {
        mkdir($path, 0777, true);
    }

    $imgs = array();

    $files = $_FILES['uploads'];

    $cnt = count($files['name']);
    for($i = 0 ; $i < $cnt ; $i++) {
        if ($files['error'][$i] === 0) {
            $name = $files['name'][$i];
            if (move_uploaded_file($files['tmp_name'][$i], $path . $name) === true) {
                $imgs[] = array('url' => $path . $name, 'name' => $files['name'][$i]);
                array_push($beanImages, $name); //insert new entry with file name
            }
        }
    }

    $imageCount = count($imgs);
    if ($imageCount == 0) {
        echo 'Não seleccionou nenhum ficheiro!';
        return;
    } else {
        Images::Update($post['type'], $id, $beanImages);
    }

    $plural = ($imageCount == 1) ? '' : 's';

    $data = array(
        'type'=>'success',
        'msg'=>'Alterações guardadas com sucesso.'
    );

    $app->flash('alert', $data);

    $app->redirect(BASE.$post['type'].'/update/'.$id);

})->via('GET', 'POST')->name('imageUpload');