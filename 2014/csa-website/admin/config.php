<?php
/*************** SESSION ***************/
session_name('csaadmin');
session_start();

/*************** CONSTANTS ***************/
define('PRODUCTIONSERVER', true);

if(PRODUCTIONSERVER) {
	define('BASE', 'http://csa.ghuroo.com/admin/');
	define('DEBUGMODE', true);
}else{
	define('BASE', '/csa-website/www/admin/');
	define('DEBUGMODE', true);
}

define('ASSETS', BASE.'assets/');

// Aux paths
if(!defined('ABSPATH')){define('ABSPATH', dirname(__FILE__));}
define('LIBPATH', ABSPATH . '/libs');
define('VENDORPATH', ABSPATH . '/vendor');
define('MODELSPATH', ABSPATH . '/models');
define('VIEWSPATH', ABSPATH . '/views');
define('CONTROLLERPATH', ABSPATH . '/controllers');
define('LANGPATH', ABSPATH . '/lang');

/*************** DEBUGGING ***************/
DEBUGMODE ?
ini_set('display_errors', '1') :
ini_set('display_errors', '0');

/*************** LOAD SLIM ***************/
// Slim PHP
require(VENDORPATH . '/Slim/Slim.php');
\Slim\Slim::registerAutoloader();

// Twig
require(VENDORPATH . '/Twig/Autoloader.php');
Twig_Autoloader::register();

// Twig [Template]
require(VENDORPATH . '/Slim-Extras/Views/Twig.php');

/*************** APP MULTILANG SETUP ***************/
$availableLangs = array('pt', 'en');
foreach (glob(VENDORPATH.'/Slim-Multilingual/app/lib/*.php') as $filename) { require_once $filename; }

$app = new MultilingualSlim(array(
	'session.handler' => null,
	'debug' => DEBUGMODE,
	'templates.path' => realpath(VIEWSPATH),
	'log.enable' => true,
	'log.path' => ABSPATH.'/Logs',
	'log.level' => 4
));
$translator = new Translator($app->getLog(), ABSPATH . '/lang');
$app->view(new MultilingualView($app, $translator));

/*************** APP CONFIG ***************/
\Slim\Extras\Views\Twig::$twigOptions = array( 'debug' => true );
\Slim\Extras\Views\Twig::$twigExtensions = array( 'Twig_Extensions_Slim' );
\Slim\Extras\Views\Twig::$twigTemplateDirs = array( realpath(VIEWSPATH) );

$app->getLog()->setEnabled(true);

/*************** PUBLIC LIBS ***************/
#require(VENDORPATH . '/PHPMailer/class.phpmailer.php');
require(VENDORPATH . '/RedBean/rb.php');

/*************** MY LIBS ***************/
foreach (glob(LIBPATH . '/*.php') as $filename) { require_once $filename; }

/*************** MODELS ***************/
foreach (glob(MODELSPATH . '/*.php') as $filename) { require_once $filename; }

/*************** CONTROLLERS ***************/
foreach (glob(CONTROLLERPATH . '/*.php') as $filename) { require_once $filename; }

/*************** MYSQL CONFIG ***************/
PRODUCTIONSERVER ?
R::setup('mysql:host=185.11.166.234;dbname=ghuroo_csa','ghuroo_root','1E1E51RgVj:A7e0') :
R::setup('mysql:host=localhost;dbname=ghuroo_csa', 'root', '');
R::setStrictTyping(false);

require(VENDORPATH.'/Slim-Multilingual/app/hooks.php');
