<?php

class Banners {
  static $table = 'banners';

  // Read
  public static function Read($filter = NULL) {

    $q = 'SELECT * FROM '.self::$table.' WHERE deleted = "no"';

    if (!empty($filter)) {
      switch ($filter) {
        case 'active': $q .= ' AND active = "yes"'; break;
        case 'inactive': $q .= ' AND active = "no"'; break;
      }
    }

    $q .= ' ORDER BY sequence ASC';

    $data = R::getAll($q);

    foreach ($data as $key => $value) {
      $banner = Images::Read('banners', $value['id']);

      if (!empty($banner)) {
        $data[$key]['banner'] = $banner[0];
      }
    }

    return $data;
  }

  // ReadOne
  public static function ReadOne($id = NULL) {

    $q = 'SELECT * FROM '.self::$table. ' WHERE id = '.$id.' AND deleted = "no"';

    $data = R::getRow($q);

    $banner = Images::Read('banners', $id);

    if (empty($banner)) {
      $data['banner'] = "";
    } else {
      $data['banner'] = $banner[0];
    }

    return $data;
  }

  // Update
  public static function Update($id = NULL, $title = NULL, $sequence = NULL) {
    $bean = R::load(self::$table, $id);

    $bean->title = $title;
    $bean->sequence = $sequence;
    $bean->updated_at = date('Y-m-d H:i:s');

    return R::store($bean);
  }

  // Activate
  public static function Activate($id = NULL) {
    $bean = R::load(self::$table, $id);
    $bean->active = 'yes';
    $bean->updated_at = date('Y-m-d H:i:s');

    return R::store($bean);
  }

  // Deactivate
  static function Deactivate($id = NULL) {
    $bean = R::load(self::$table, $id);
    $bean->active = 'no';
    $bean->updated_at = date('Y-m-d H:i:s');

    return R::store($bean);
  }

  // Create
  static function Create($title = NULL) {
    $bean = R::dispense(self::$table);
    $bean->title = $title;
    $bean->sequence = 0;
    $bean->images = '[]';
    $bean->active = 'yes';
    $bean->deleted = 'no';
    $bean->created_at = date('Y-m-d H:i:s');
    $bean->updated_at = date('Y-m-d H:i:s');

    return R::store($bean);
  }

  // Delete
  static function Delete($id = NULL) {
    $bean = R::load(self::$table, $id);
    $bean->active = 'no';
    $bean->deleted = 'yes';

    return R::store($bean);
  }

}