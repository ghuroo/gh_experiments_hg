<?php

class Areas {
  static $table = 'areas';

  // Read
  public static function Read($id = NULL) {

    $q = 'SELECT * FROM '.self::$table;

    if (!empty($id)) {
      $q .= ' WHERE id ="'.$id.'"';

      $data = R::getRow($q);
    } else {
      $data = R::getAll($q);
    }

    return $data;
  }

  // Update
  public static function Update($id = NULL, $content = NULL, $content_en = NULL) {
    $bean = R::load(self::$table, $id);

    $bean->content = $content;
    $bean->content_en = $content_en;
    $bean->updated_at = date('Y-m-d H:i:s');

    return R::store($bean);
  }

}