<?php

class Projects {
  static $table = 'projects';
  static $tableAux = 'categories';

  // Read
  public static function Read($filter = NULL) {

    $q = 'SELECT * FROM '.self::$table.' WHERE deleted = "no"';

    if (!empty($filter)) {
      switch ($filter) {
        case 'active': $q .= ' AND active = "yes"'; break;
        case 'inactive': $q .= ' AND active = "no"'; break;
      }
    }

    $q .= ' ORDER BY title ASC';

    $data = R::getAll($q);

    return $data;
  }

  // ReadOne
  public static function ReadOne($id = NULL) {

    $q = 'SELECT * FROM '.self::$table. ' WHERE id = '.$id.' AND deleted = "no"';

    $data = R::getRow($q);

    return $data;
  }

  // Update
  public static function Update($id = NULL, $title = NULL, $title_en = NULL, $description = NULL, $description_en = NULL, $category = NULL) {
    $bean = R::load(self::$table, $id);

    $bean->title = $title;
    $bean->description = $description;
    $bean->title_en = $title_en;
    $bean->description_en = $description_en;
    $bean->category = $category;
    $bean->updated_at = date('Y-m-d H:i:s');

    return R::store($bean);
  }

  // Activate
  public static function Activate($id = NULL) {
    $bean = R::load(self::$table, $id);
    $bean->active = 'yes';
    $bean->updated_at = date('Y-m-d H:i:s');

    return R::store($bean);
  }

  // Deactivate
  static function Deactivate($id = NULL) {
    $bean = R::load(self::$table, $id);
    $bean->active = 'no';
    $bean->updated_at = date('Y-m-d H:i:s');

    return R::store($bean);
  }

  // Create
  static function Create($title = NULL, $title_en = NULL, $description = NULL, $description_en = NULL, $category = NULL) {
    $bean = R::dispense(self::$table);

    $bean->title = $title;
    $bean->description = $description;
    $bean->title_en = $title_en;
    $bean->description_en = $description_en;
    $bean->category = $category;
    $bean->images = '[]';
    $bean->uri = Sanitize($title);
    $bean->active = 'yes';
    $bean->deleted = 'no';
    $bean->created_at = date('Y-m-d H:i:s');
    $bean->updated_at = date('Y-m-d H:i:s');

    return R::store($bean);
  }

  // Delete
  static function Delete($id = NULL) {
    $bean = R::load(self::$table, $id);
    $bean->active = 'no';
    $bean->deleted = 'yes';

    return R::store($bean);
  }

}