<?php

class Banners {
  static $table = 'banners';

  // Read
  public static function ReadAll($u = NULL) {
    $data = R::getAll('SELECT * FROM '.self::$table.' WHERE active="yes" ORDER BY sequence ASC');

    foreach ($data as $key => $value) {
      $banner = Images::Read('banners', $value['id']);

      if (!empty($banner)) {
        $data[$key]['banner'] = $banner[0];
      }
    }

  	return $data;
  }

}