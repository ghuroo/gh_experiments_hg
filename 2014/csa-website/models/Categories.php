<?php

class Categories {
  static $table = 'categories';

  // Read
  public static function ReadOne($u = NULL) {
    $data = R::getRow('SELECT * FROM '.self::$table.' WHERE uri = "'.$u.'" AND active="yes"');

      $banner = Images::Read('categories', $data['id']);
      if (!empty($banner)) { $data['banner'] = $banner[0]; }

      $lang = '';
      if (!empty($_COOKIE['csa-language'])) { $lang = $_COOKIE['csa-language']; }
      if ($lang == '_pt') { $lang = ''; }

      $data['title'] = $data['title'.$lang];

  	return $data;
  }

  // Read
  public static function ReadAll() {

    $data = R::getAll('select * FROM '.self::$table.' WHERE active="yes" ORDER BY title ASC');

    $lang = '';
    if (!empty($_COOKIE['csa-language'])) { $lang = $_COOKIE['csa-language']; }
    if ($lang == '_pt') { $lang = ''; }

    foreach ($data as $key => $value) {
      $banner = Images::Read('categories', $value['id']);

      $data[$key]['title'] = $data[$key]['title'.$lang];

      if (!empty($banner)) {
        $data[$key]['banner'] = $banner[0];
      }
    }

  	return $data;
  }
}