<?php

class Locale {
  static $table = 'locale';

  // Read
  public static function Read($id = NULL) {

    $data = R::getAll('SELECT * FROM '.self::$table);

      $lang = '';
      if (!empty($_COOKIE['csa-language'])) { $lang = $_COOKIE['csa-language']; }
      if ($lang == '_pt') { $lang = ''; }

    foreach ($data as $key => $value) {
      $data[$key]['title'] = $data[$key]['title'.$lang];
    }

  	return $data;
  }

}