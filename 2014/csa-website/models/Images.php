<?php

class Images {

    static $tableProjects = 'projects';
    static $tableCategories = 'categories';

// Create
    public static function Update($type = NULL, $id = NULL, $array = NULL) {
        $bean = R::load($type, $id);
        $bean->images = json_encode($array);
        $bean->updated_at = date('Y-m-d H:i:s');

        return R::store($bean);
    }

// Read
    static function Read($type = NULL, $id = NULL) {
        $q = 'SELECT images FROM '.$type.' WHERE deleted="no" AND active="yes" AND id="'.$id.'"';
        $bean = R::getRow($q);

        if (empty($bean)) {
            $data = "";
        } else {
            $data = json_decode($bean['images'], true);
        }

        return $data;
    }

// Delete
    public static function Delete($type = NULL, $id = NULL, $name = NULL) {
        $bean = R::load($type, $id);

        $images = self::Read($type, $id);

        foreach ($images as $key => $value) {
            if ($value == $name) {
                unset($images[$key]);
            }
        }

        $file = 'uploads/'.$type.'/'.$bean['uri'].'/'.$name;
        unlink($file);

        $bean->images = json_encode($images);
        $bean->updated_at = date('Y-m-d H:i:s');

        return R::store($bean);
    }
}