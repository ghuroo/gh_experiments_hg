<?php

class Projects {
  static $table = 'projects';
  static $tableAux = 'categories';

  // Read One
  public static function ReadOne($t) {

    $p = R::getRow('SELECT * FROM '.self::$table.' WHERE uri = "'.$t.'" AND active = "yes"');
    $p['images'] = json_decode($p['images']);

    if (!empty($p['images'])) {
      $c = $p['category'];
      $c = R::getAll('SELECT * FROM '.self::$tableAux.' WHERE id = "'.$c.'" AND active = "yes"');

      $p['category_name'] = $c[0]['title'];

      $data = $p;

        $lang = '';
        if (!empty($_COOKIE['csa-language'])) { $lang = $_COOKIE['csa-language']; }
        if ($lang == '_pt') { $lang = ''; }

      $data['title'] = $data['title'.$lang];
      $data['description'] = $data['description'.$lang];
    } else {
      $data = null;
    }

  	return $data;
  }

  // Read All
  public static function ReadAll($c_id) {

    if (!empty($c_id)) {
      $q = 'SELECT * FROM '.self::$table.' WHERE category = "'.$c_id.'" AND active="yes" ORDER BY title ASC';
    } else {
      $q = 'SELECT * FROM '.self::$table.' WHERE active="yes" ORDER BY title ASC';
    }

    $p = R::getAll($q);

    $data = $p;

    $lang = '';
    if (!empty($_COOKIE['csa-language'])) { $lang = $_COOKIE['csa-language']; }
    if ($lang == '_pt') { $lang = ''; }

    foreach ($data as $key => $value) {
      $data[$key]['title'] = $data[$key]['title'.$lang];
      $data[$key]['description'] = $data[$key]['description'.$lang];
    }

    return $data;
  }
}