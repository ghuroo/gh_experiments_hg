'use strict';

module.exports = function (grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        project: {
            base: '../../',
            app: '<%= project.base %>/app',
            public: '<%= project.app %>/public',

            src: {
                css: '<%= project.app %>/styles/main.scss',
                cssPath: '<%= project.app %>/styles',
                jsPath: '<%= project.app %>'
            },

            public_assets: {
                css: '<%= project.public %>/css/<%= pkg.name %>.min.css',
                js: '<%= project.public %>/js/<%= pkg.name %>.min.js'
            }
        },

        copy: {
            main: {
                src: 'src/*',
                dest: 'dest/',
            },
        }

    });

    grunt.loadNpmTasks('grunt-contrib-copy');

    grunt.registerTask('default', [
        'grunt-copy'
    ]);

};