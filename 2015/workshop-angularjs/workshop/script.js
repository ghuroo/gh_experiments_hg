/* bootstrap */
var appname = 'app';
var dependencies = ['ui.router'];

angular.module(appname, dependencies);

var app = angular.module(appname);

/* config */
var configFn = function($stateProvider, $urlRouterProvider) {

    $stateProvider
    .state('home', {
        url: "/",
        templateUrl: 'home.html',
        data: {
            name: 'home',
            title: 'Bem-vindo'
        }
    })
    .state('gitfinder', {
        url: "/gitfinder",
        templateUrl: 'gitfinder.html',
        data: {
            name: 'gitfinder',
            title: 'Procurar utilizador GIT'
        }
    })
    .state('template', {
        url: "/template",
        templateUrl: 'template.html',
        data: {
            name: 'template',
            title: 'Templating'
        }
    })
    ;

    $urlRouterProvider.otherwise('/');
};
app.config(configFn);

/* run */
var runFn = function ($rootScope) {

    var stateChangeSuccess = function (event, toState, toParams, fromState, fromParams) {
        $rootScope.name = toState.data.name;
        $rootScope.title = toState.data.title;
    };

    $rootScope.$on('$stateChangeSuccess', stateChangeSuccess);

};
app.run(runFn);



/* filters */
app.filter('titleCase', function () {
    return function (input) {
        var words = input.split(' ');
        for (var i = 0; i < words.length; i++) {
            words[i] = words[i].charAt(0).toUpperCase() + words[i].slice(1);
        }
        return words.join(' ');
    }
});

/* services */
var gitF = (function ($http, $q) {
    var service = {};
    service.api = 'https://api.github.com/';

    service.user = function(_username) {
        var deferred = $q.defer();
        deferred.resolve(
            $http.get(service.api + 'users/'+_username)
            // $http.get('git.'+_username+'.json')
        );
        return deferred.promise;
    };

    service.userRepos = function(_username) {
        var deferred = $q.defer();
        deferred.resolve(
            $http.get(service.api + 'users/'+_username+'/repos')
            // $http.get('git.'+_username+'.repos.json')
        );
        return deferred.promise;
    };

    return service;
});
app.factory('gitF', gitF);

/* controllers */
var gitC = (function ($scope, gitF) {
    var vm = $scope;

    vm.input = {};
    vm.input.username = 'angular-ui';
    // vm.input.repo_search = { name: 'alias' };

    vm.submit = function (_isValid, _username) {
        if (!_isValid) return;

        /* user */
        gitF.user(_username)
        .then(function (_success) {
            data = _success.data;

            /* assign user */
            vm.user = {
                avatar_url: data.avatar_url,
                login: data.login,
                location: data.location,
                created_at: data.created_at,
                html_url: data.html_url
            };

            /* repo */
            gitF.userRepos(_username)
            .then(function (_success) {
                data = _success.data;

                /* assign repos */
                vm.user.repos = data;

            }, function (_error) {
                alert(_error.data.message);
                if (vm.user) delete vm.user;
            });

        }, function (_error) {
            alert(_error.data.message);
            if (vm.user) delete vm.user;
        });
    };
});
app.controller('gitC', gitC);

var templateC = (function () {
    var vm = this;

    var lxfreguesias = [
        {
            id: 'identificador1',
            nome: 'sta. catarina',
            desc: 'muito bonito'
        },
        {
            id: 'identificador2',
            nome: 'ajuda',
            desc: 'charmosa'
        },
        {
            id: 'identificador3',
            nome: 'parque expo',
            desc: 'luminoza'
        },
    ];

    vm.freguesias = lxfreguesias;
});
app.controller('templateC', templateC);