/* services */
var gitF = (function ($http, $q) {
    var service = {};
    // service.api = 'https://api.github.com/';
    service.api = '/json/git.';

    service.user = function(_username) {
        var deferred = $q.defer();
        deferred.resolve(
            $http.get(service.api+_username+'.json')
        );
        return deferred.promise;
    };

    return service;
});
app.factory('gitF', gitF);