/* bootstrap */
var appname = 'app';
var dependencies = ['ngRoute'];

angular.module(appname, dependencies);

var app = angular.module(appname);

/* config */
var configFn = function($routeProvider, $locationProvider) {

    $routeProvider
    .when('/', {
        templateUrl: 'views/home.html',
        controller: 'homeC'
    })
    .when('/user/show', {
        templateUrl: 'views/user.show.html',
        controller: 'userShowC'
    })
    .when('/user/detail/:login', {
        templateUrl: 'views/user.show.detail.html',
        controller: 'userShowDetailC'
    })
    .otherwise('/');
};
app.config(configFn);