var homeC = (function ($rootScope, $scope) {
    $rootScope.name = 'home';
    $rootScope.title = 'Cá estamos';

    var vm = $scope;

    vm.name = 'World';
});
app.controller('homeC', homeC);

var userShowC = (function ($rootScope, $scope) {
    $rootScope.name = 'userShow';
    $rootScope.title = 'Show users';

    var vm = $scope;

    vm.users = [
        { login: 'angular-ui'},
        { login: 'ghuroo'}
    ];
});
app.controller('userShowC', userShowC);

var userShowDetailC = (function ($rootScope, $scope, $routeParams, gitF) {
    $rootScope.name = 'userShow';
    $rootScope.title = 'Show user details';

    var vm = $scope;

    gitF.user($routeParams.login)
    .then(function (_success) {
        data = _success.data;

        vm.user = data;

    }, function (_error) {
        alert(_error.data.message);
        if (vm.user) delete vm.user;
    });
});
app.controller('userShowDetailC', userShowDetailC);