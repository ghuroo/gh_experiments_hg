TweenLite.defaultEase = Linear.easeNone;

$(window).on("orientationchange",function(event){
    // location.reload();
    eliana.orientate();
});

$(window).on("resize",function(event){
    eliana.orientate();
});

//Objecto eliana
var eliana = {};

eliana.orientate = function() {

    this.mainWidth = $('body').outerWidth();
    this.mainHeight = $('body').outerHeight();
    this.objectWidth = $('#carro').outerWidth();

    switch(window.orientation) {
        case 90:
            $('.grid-container').css('transform', 'rotate(0deg)');
            $('.grid-container').css('width', this.mainWidth);
            TweenMax.to($('#carro'), 0, { x: this.mainWidth, z: 1 });
            break;
        case -90:
            $('.grid-container').css('transform', 'rotate(180deg)');
            $('.grid-container').css('width', this.mainWidth);
            TweenMax.to($('#carro'), 0, { x: this.mainWidth, z: 1 });
            break;
        case 0:
            $('.grid-container').css('transform', 'rotate(90deg)');
            $('.grid-container').css('width', this.mainHeight);
            TweenMax.to($('#carro'), 0, { x: this.mainHeight, z: 1 });
            break;
        default:
            $('.grid-container').css('transform', 'rotate(0deg)');
            $('.grid-container').css('width', this.mainWidth);
            TweenMax.to($('#carro'), 0, { x: this.mainWidth, z: 1 });
    }

    eliana.carro.move.animation.clear();
    eliana.carro.move.init();
}

eliana.carro = {};
eliana.carro.move = {};
eliana.carro.jump = {};

eliana.carro.move.animation = new TimelineMax( {
    delay: 1,
    onComplete: function() { eliana.carro.move.animation.seek(0); }
});

eliana.carro.move.init = function() {

    eliana.carro.move.animation.
    to($('#carro'), 4, { x: -eliana.objectWidth, z: 1 });

    eliana.carro.move.animation.timeScale(1);
};

eliana.carro.jump.animation = new TimelineMax( {
    delay: 1
});

eliana.carro.jump.init = function() {
    eliana.carro.jump.animation.
    to($('#carro'), 0.1, { y: '-100%', rotation: '15', scale: '1.2' }).
    to($('#carro'), 0.1, { rotation: '-5' }).
    to($('#carro'), 0.1, { y: '0%', rotation: '0', scale: '1' });
};

$(window).load(function() {
    eliana.orientate();
    eliana.carro.jump.init();

    $('#carro').on({
        'touchstart': function() { eliana.carro.jump.animation.seek(0); },
        'click': function() { eliana.carro.jump.animation.seek(0); }
    });

});