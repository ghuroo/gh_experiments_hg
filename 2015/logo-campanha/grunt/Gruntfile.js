'use strict';

/**
* Grunt Module
*/
module.exports = function(grunt) {

/**
* Configuration
*/
grunt.initConfig({

    /**
    * Get package meta data
    */
    pkg: grunt.file.readJSON('package.json'),

        /**
        * Set project object
        */
        project: {
            app: '..',
            assets: '<%= project.app %>/assets',

            src: {
                path: '<%= project.assets %>/src',
                css: '<%= project.src.path %>/scss/main.scss',
                js: '<%= project.src.path %>/js/main.js',
            },

            public: {
                path: '<%= project.assets %>/public',
                css: '<%= project.public.path %>/css/<%= pkg.name %>.min.css',
                js: '<%= project.public.path %>/js/<%= pkg.name %>.min.js',
            }
        },

        /**
        * Project banner
        */
        tag: {
            banner: '/*!\n' +
            ' * <%= pkg.name %>\n' +
            ' * <%= pkg.title %>\n' +
            ' * <%= pkg.url %>\n' +
            ' * @author <%= pkg.author %>\n' +
            ' * @version <%= pkg.version %>\n' +
            ' * Copyright <%= pkg.copyright %>. <%= pkg.license %> licensed.\n' +
            ' */\n'
        },

        /**
        * Sass
        */
        sass: {
            dist: {
                options: {
                    style: 'compressed',
                    compass: false,
                    noCache: true,
                    banner: '<%= tag.banner %>',
                    sourcemap: 'none'
                },
                files: {
                    '<%= project.public.css %>': '<%= project.src.css %>'
                }
            }
        },

        /**
        * Watch
        */
        watch: {
            sass: {
                files: '<%= project.src.path %>/scss/{,*/}*.{scss,sass}',
                tasks: ['sass:dist', 'autoprefixer']
            },

            js: {
                files: ['<%= project.src.path %>/js/{,*/}*.js'],
                tasks: ['import:dist']
            }
        },

        import: {
            dist: {
                files: {
                    '<%= project.public.js %>': '<%= project.src.js %>'
                }
            }
        },

        autoprefixer: {
            options: {
                browsers: ['last 2 versions'],
                map: false
            },

            dist: {
                files: {
                     '<%= project.public.css %>': '<%= project.public.css %>'
                }
            }
        },

        uglify: {
            options: {
                banner: '<%= tag.banner %>',
                mangle: true,
                sourceMap: false,
                compress: true
            },

            dist: {
                files: {
                    '<%= project.public.js %>': '<%= project.public.js %>'
                }
            }
        },

    });

    // grunt.loadNpmTasks('grunt-import');
    // grunt.loadNpmTasks('grunt-contrib-uglify');
    // grunt.loadNpmTasks('grunt-autoprefixer');

    /**
    * Load Grunt plugins
    */
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    /**
    * Default task
    * Run `grunt` on the command line
    */
    grunt.registerTask('default', [
        // 'sass:dist',
        // 'autoprefixer:dist',
        // 'import:dist',
        // 'uglify:dist',
        'watch'
    ]);

};