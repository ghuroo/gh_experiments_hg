<?

class Token {
    static $tableAdmin = 'admin';
    static $tableToken = 'token';

    public static function Create ($admin_id = NULL) {
        if (empty($admin_id)) throw new Exception('Dados vazios. (' . __FUNCTION__ . ')');

        $app = \Slim\Slim::getInstance();

        $url = $app->request->getRootUri();

        $token = array(
            'admin_id' => (int) $admin_id,
            'iss' => $url,
            'iat' => time(),
            'exp' => time() + (3 * 24 * 60 * 60)
            // 'exp' => time() + (1 * 1 * 1 * 5)
        );

        $token_new = JWT::encode($token, SECRET_SERVER_KEY);
        if (empty($token_new)) throw new Exception('Erro ao inserir token. (' . __FUNCTION__ . ')');

        /* save token */
        $bean = R::load(self::$tableAdmin, $admin_id);
        $bean->token = $token_new;

        $admin = R::store($bean);
        if (empty($admin)) throw new Exception('Erro ao gravar token. (' . __FUNCTION__ . ')');

        // echo '\n'.date('Y-m-d H:i:s', time()).'\n'.date('Y-m-d H:i:s', $token['exp']);
        // die();

        return $token_new;
    }

    public static function ReadByID ($id = NULL) {
        if (empty($id)) throw new Exception('Dados vazios. (' . __FUNCTION__ . ')');

        $q = 'SELECT '.self::$tableToken.' as token FROM admin WHERE id = :id';
        $b = array('id' => $id);

        return R::getCell($q, $b);
    }

    public static function Check ($token = NULL) {
        $app = \Slim\Slim::getInstance();

        try {
            $token_decoded = JWT::decode($token, SECRET_SERVER_KEY);

            $admin_id = $token_decoded->admin_id;

            $token_valid = admin::CheckToken($admin_id, $token);

            if (empty($token_valid)) throw new Exception();

            return $token_decoded;

        } catch (Exception $e) {
            return false;
        }
    }

}