<?

class admin {
    static $tableAdmin = 'admin';

    /**
    read by username and password
    */

    public static function ReadBySignIn ($username = NULL, $password = NULL) {
        if (empty($username) || empty($password)) throw new Exception('Dados vazios. ' . __FUNCTION__);

        $q = 'SELECT id,
            username,
            name,
            email,
            UNIX_TIMESTAMP(last_signin) as last_signin,
            UNIX_TIMESTAMP(last_activity) as last_activity,
            token
            FROM '.self::$tableAdmin.' as admin
            WHERE username = :username
            AND password = :password
        ';

        $b = array(
            'username' => $username,
            'password' => md5($password)
        );

        $admin = R::getRow($q, $b);
        if (empty($admin)) throw new Exception('Utilizador inválido. ' . __FUNCTION__);

        $admin['last_signin'] = (int) $admin['last_signin'];
        $admin['last_activity'] = (int) $admin['last_activity'];

        return $admin;
    }

    /**
    read by id
    */

    public static function ReadByID ($id = NULL) {
        if (empty($id)) throw new Exception('Dados vazios. ' . __FUNCTION__);

        $q = 'SELECT
            username,
            name,
            email,
            UNIX_TIMESTAMP(last_signin) as last_signin,
            UNIX_TIMESTAMP(last_activity) as last_activity,
            token
            FROM '.self::$tableAdmin.' as admin
            WHERE id = :id
        ';

        $b = array(
            'id' => $id
        );

        $admin = R::getRow($q, $b);
        if (empty($admin)) throw new Exception('O utilizador não existe. ' . __FUNCTION__);

        $admin['last_signin'] = (int) $admin['last_signin'];
        $admin['last_activity'] = (int) $admin['last_activity'];

        return $admin;
    }

    /**
    read by email
    */

    public static function ReadByEmail ($email = NULL) {
        if (empty($email)) throw new Exception('Dados vazios. ' . __FUNCTION__);

        $q = 'SELECT id,
            name,
            password
            FROM '.self::$tableAdmin.' as admin
            WHERE email = :email
        ';

        $b = array(
            'email' => $email
        );

        $admin = R::getRow($q, $b);
        if (empty($admin)) throw new Exception('Utilizador inválido. ' . __FUNCTION__);

        return $admin;
    }

    /**
    update signin time
    */

    public static function UpdateSignInTime ($id = NULL) {
        if (empty($id)) throw new Exception('Dados vazios. ' . __FUNCTION__);

        $bean = R::load(self::$tableAdmin, $id);
        $bean->last_signin = date(DATE_FORMAT);
        $bean->last_activity = date(DATE_FORMAT);

        $admin = R::store($bean);
        if (empty($admin)) throw new Exception('Erro ao gravar alterações. ' . __FUNCTION__);

        return $admin;
    }

    /**
    update activity
    */

    public static function UpdateActivity ($id = NULL) {
        if (empty($id)) throw new Exception('Dados vazios. ' . __FUNCTION__);

        $bean = R::load(self::$tableAdmin, $id);
        $bean->last_activity = date(DATE_FORMAT);

        $admin = R::store($bean);
        if (empty($admin)) throw new Exception('Erro ao gravar alterações. ' . __FUNCTION__);

        return $admin;
    }

    /**
    check token
    */

    public static function CheckToken ($admin_id = NULL, $token = NULL) {
        if (empty($admin_id) || empty($token)) throw new Exception('Dados vazios. ' . __FUNCTION__);

        $q = 'SELECT
            admin.id
            FROM '.self::$tableAdmin.' as admin
            WHERE admin.id = :admin_id
            AND admin.token = :token
        ';

        $b = array(
            'admin_id' => $admin_id,
            'token' => $token
        );

        $admin_id = R::getCell($q, $b);

        return (int) $admin_id;
    }

}