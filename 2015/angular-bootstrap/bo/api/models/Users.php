<?

class Users {
    static $tableUser = 'user';

    public static function ReadAll() {

        $q = 'SELECT
            id,
            name,
            age,
            UNIX_TIMESTAMP(timestamp) as timestamp,
            active
            FROM '.self::$tableUser.' as user
        ';

        $user = R::getAll($q);

        if (!empty($user)) {
            foreach ($user as $key => $value) {
                $user[$key]['id'] = (double) $value['id'];
                $user[$key]['age'] = (double) $value['age'];
                $user[$key]['active'] = (double) $value['active'];
            }
        }

        return $user;
    }

    public static function Create($input = NULL) {
        if (empty($input)) throw new Exception('Dados vazios. ' . __FUNCTION__);

        $bean = R::dispense(self::$tableUser);

        $bean->name = $input->name;
        $bean->age = $input->age;
        $bean->timestamp = date(DATE_FORMAT, time());
        $bean->active = 1;

        return R::store($bean);
    }

    public static function Update($input = NULL, $user_id = NULL) {
        if (empty($input) || empty($user_id)) throw new Exception('Dados vazios. ' . __FUNCTION__);

        $bean = R::load(self::$tableUser, $user_id);
        if (empty($bean)) throw new Exception('Erro ao ler o utilizador. ' . __FUNCTION__);

        $bean->name = $input->name;
        $bean->age = $input->age;

        return R::store($bean);
    }

    public static function Delete($user_id = NULL) {
        if (empty($user_id)) throw new Exception('Dados vazios. ' . __FUNCTION__);

        $bean = R::load(self::$tableUser, $user_id);
        if (empty($bean)) throw new Exception('Erro ao ler o utilizador. ' . __FUNCTION__);

        $bean->active = 0;

        return R::store($bean);
    }
}