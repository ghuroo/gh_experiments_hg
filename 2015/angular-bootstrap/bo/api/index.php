<?php

// environment
if ($_SERVER['SERVER_NAME'] === 'localhost') {
    define( 'ENVIRONMENT', 'local' );
} else {
    // define( 'ENVIRONMENT', 'dev' );
    define( 'ENVIRONMENT', 'local' );
}

// uploads
define( 'UPLOADS', __DIR__ . '/upload' );

// keys
$privateKey = file_get_contents('keys/privkey.pem');
// $publicKey  = file_get_contents('keys/pubkey.pem');

define( 'SECRET_SERVER_KEY', $privateKey );

// jwt
require 'vendor/JWT/JWT.php';
foreach (glob('vendor/JWT/**/*.php') as $filename) { require_once $filename; }

// redbean
require 'vendor/Redbean/rb.php';

// db connection
switch (ENVIRONMENT) {
    case 'local':
        $host = 'localhost';
        $dbname = 'rd_bootstrap_angular';
        $username = 'root';
        $password = '';
        break;
}

R::setup('mysql:host='.$host.';dbname='.$dbname, $username, $password);

// http://www.redbeanphp.com/prefixes
R::ext('xdispense', function($type){ return R::getRedBean()->dispense( $type); });

// twig
require_once 'vendor/Twig/Autoloader.php';
Twig_Autoloader::register();

// slim
require 'vendor/Slim/Slim.php';
\Slim\Slim::registerAutoloader();

// slim+twig
require_once 'vendor/Slim-Views/Twig.php';
require_once 'vendor/Slim-Views/TwigExtension.php';

// email vars
define('EMAIL_FROM_ADDRESS', 'montepiocredito@ativ-mobile.com');
define('EMAIL_FROM_ADDRESS_SIM', 'simulacao@ativ-mobile.com');
define('EMAIL_TO_ADDRESS', 'info@montepio.pt');
define('EMAIL_FROM_NAME', 'Montepio Credito');
define('EMAIL_HOST', 'mail.rd-agency.info');
define('EMAIL_USERNAME', 'noreply@rd-agency.info');
define('EMAIL_PASSWORD', 'bmca6NXHbmKw');
define('EMAIL_ASSETS', UPLOADS . 'img/');

// phpmailer
require_once 'vendor/PHPMailer/class.phpmailer.php';

// create app
$app = new \Slim\Slim(array(
	'view' => new \Slim\Views\Twig(),
	'templates.path' => 'views'
));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true
);

$app->config(array(
    'debug' => true,
    'log.level' => \Slim\Log::DEBUG,
    'log.enabled' => true
));

// libs
foreach (glob('libs/*.php') as $filename) { require_once $filename; }

// controllers
foreach (glob('controllers/*.php') as $filename) { require_once $filename; }

// models
foreach (glob('models/*.php') as $filename) { require_once $filename; }

// response headers
$app->response->headers->set('Access-Control-Allow-Origin', '*');
$app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST');
$app->response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With, X-Auth-Token, Content-Type');
$app->response->headers->set('Content-Type', 'application/json;charset=UTF-8');

// timezone
date_default_timezone_set("Europe/Lisbon");
define('DATE_FORMAT', 'Y-m-d H:i:s');

// start this biatch
$app->run();