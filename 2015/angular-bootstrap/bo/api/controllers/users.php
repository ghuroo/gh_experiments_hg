<?

/**
get all
*/

$app->get('/users(/)', function () use($app) {
    $response = array();

    // validate token
    $input_token = $app->request()->headers()['X-Auth-Token'];
    try { $token = Token::Check($input_token); if (empty($token)) throw new Exception(); }
    catch (Exception $e) { $response['status'] = 'Sessão expirada.'; $app->response->setStatus(401); $token = false; }

    if ($token) {
        try {

            /* update activity */
            Admin::UpdateSignInTime($token->admin_id);

            /* get admin */
            $admin = Admin::ReadByID($token->admin_id);

            /* output */
            $response['users'] = Users::ReadAll();

        } catch (Exception $e) {
            $response['status'] = $e->getMessage();
            $app->response->setStatus(500);
        }
    }

    U::RenderJSON($response);
});

/**
create
*/

$app->post('/users/create(/)', function () use($app) {
    $response = array();

    // validate token
    $input_token = $app->request()->headers()['X-Auth-Token'];
    try { $token = Token::Check($input_token); if (empty($token)) throw new Exception(); }
    catch (Exception $e) { $response['status'] = 'Sessão expirada.'; $app->response->setStatus(401); $token = false; }

    if ($token) {
        try {

            /* update activity */
            Admin::UpdateSignInTime($token->admin_id);

            /* get admin */
            $admin = Admin::ReadByID($token->admin_id);

            /* input */
            $input = json_decode($app->request->getBody());

            /* validate input */
            Validate::this($input);

            /* create users */
            $prize_id = Users::Create($input);

            /* output */
            $response['users'] = users::ReadAll();

        } catch (Exception $e) {
            $response['status'] = $e->getMessage();
            $app->response->setStatus(500);
        }
    }

    U::RenderJSON($response);
});

/**
update
*/

$app->post('/users/update/:users_id(/)', function ($users_id = NULL) use($app) {
    $response = array();

    // validate token
    $input_token = $app->request()->headers()['X-Auth-Token'];
    try { $token = Token::Check($input_token); if (empty($token)) throw new Exception(); }
    catch (Exception $e) { $response['status'] = 'Sessão expirada.'; $app->response->setStatus(401); $token = false; }

    if ($token) {
        try {

            /* update activity */
            Admin::UpdateSignInTime($token->admin_id);

            /* get admin */
            $admin = Admin::ReadByID($token->admin_id);

            /* input */
            $input = json_decode($app->request->getBody());

            /* validate input */
            Validate::this($input);

            /* update users */
            Users::Update($input, $users_id);

            /* output */
            $response['users'] = Users::ReadAll();

        } catch (Exception $e) {
            $response['status'] = $e->getMessage();
            $app->response->setStatus(500);
        }
    }

    U::RenderJSON($response);
});

/**
delete
*/

$app->get('/users/deactivate/:users_id(/)', function ($users_id = NULL) use($app) {
    $response = array();

    // validate token
    $input_token = $app->request()->headers()['X-Auth-Token'];
    try { $token = Token::Check($input_token); if (empty($token)) throw new Exception(); }
    catch (Exception $e) { $response['status'] = 'Sessão expirada.'; $app->response->setStatus(401); $token = false; }

    if ($token) {
        try {

            /* update activity */
            Admin::UpdateSignInTime($token->admin_id);

            /* get admin */
            $admin = Admin::ReadByID($token->admin_id);

            /* remove users */
            Users::Delete($users_id);

            /* output */
            $response['users'] = Users::ReadAll();

        } catch (Exception $e) {
            $response['status'] = $e->getMessage();
            $app->response->setStatus(500);
        }
    }

    U::RenderJSON($response);
});