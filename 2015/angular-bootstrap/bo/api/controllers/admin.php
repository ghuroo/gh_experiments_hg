<?

/**
signin
*/

$app->post('/admin/signin(/)', function () use($app) {
    $response = array();

    try {

        /* input */
        $input = json_decode($app->request->getBody());

        /* admin */
        $admin = Admin::ReadBySignIn($input->username, $input->password);

        /* token */
        $token = Token::Check($admin['token']);
        if (empty($token)) Token::Create($admin['id']);

        /* get token */
        $token = Token::ReadByID($admin['id']);

        /* update activity */
        Admin::UpdateSignInTime($admin['id']);

        /* get admin */
        $admin = Admin::ReadByID($admin['id']);

        /* output */
        $response['admin'] = $admin;

    } catch (Exception $e) {
        $response['status'] = $e->getMessage();
        $app->response->setStatus(500);
    }

    U::RenderJSON($response);
});

/**
signin by token
*/

$app->get('/admin/signin/:input_token(/)', function ($input_token = NULL) use($app) {
    $response = array();

    // validate token
    try { $token = Token::Check($input_token); if (empty($token)) throw new Exception(); }
    catch (Exception $e) { $response['status'] = 'Sessão expirada.'; $app->response->setStatus(401); $token = false; }

    if ($token) {
        try {

            /* update activity */
            Admin::UpdateSignInTime($token->admin_id);

            /* get admin */
            $admin = Admin::ReadByID($token->admin_id);

            /* output */
            $response['admin'] = $admin;

        } catch (Exception $e) {
            $response['status'] = $e->getMessage();
            $app->response->setStatus(500);
        }
    }

    U::RenderJSON($response);
});