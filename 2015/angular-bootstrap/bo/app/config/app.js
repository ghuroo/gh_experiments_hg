var appname = 'backoffice';

var dependencies = [
    'ngStorage',
    'ui.router',
    'validators',
    // 'ngFileUpload',
    'angularUtils.directives.dirPagination',
    'ui.bootstrap',
    'dndLists'
];

angular
.module(appname, dependencies);

var app = angular.module(appname);