var constants = {
    'TOKEN_NAME': 'bootstrap_angular',
    'API': 'api',
    'IMG': 'app/public/img',
    'VERSION': '0.0.5'
};

app.constant('CONSTANTS', constants);