var runFn = function ($rootScope, $state, CONSTANTS) {

    $rootScope.CONSTANTS = CONSTANTS;

    var parseSection = function (toState) {
        var sectionArray = toState.name.split('.');
        var section = {};
        if (!isEmpty(sectionArray[0])) section.main = sectionArray[0];
        if (!isEmpty(sectionArray[1])) section.sub = sectionArray[1];
        if (!isEmpty(sectionArray[2])) section.partial = sectionArray[2];

        return section;
    }

    /* on stateChangeStart */
    var stateChangeStart = function (event, toState, toParams, fromState, fromParams) {
        /*console.log();*/
    };

    $rootScope.$on('$stateChangeStart', stateChangeStart);

    /* on stateChangeError */
    var stateChangeError = function (event, toState, toParams, fromState, fromParams, error) {
        console.log(error);
    };

    $rootScope.$on('$stateChangeError', stateChangeError);

    /* on stateChangeSuccess */
    var stateChangeSuccess = function (event, toState, toParams, fromState, fromParams) {
        $rootScope.section = parseSection(toState);
    };

    $rootScope.$on('$stateChangeSuccess', stateChangeSuccess);

    // FastClick
    FastClick.attach(document.body);

}

app.run(runFn);