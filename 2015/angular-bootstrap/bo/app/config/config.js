var configFn = function ($stateProvider, $urlRouterProvider, $httpProvider, CONSTANTS) {

	/* admin generic resolves */
	var adminSignedOut = function ($q , $state, adminF) {
		var deferred = $q.defer(); adminF.isSignedOut().then(function (success) { deferred.resolve(false); }, function (error) { deferred.reject(false); return $state.go('admin.detail') });
        return deferred.promise;
	};

	var adminSignedIn = function ($q , $state, adminF) {
		var deferred = $q.defer(); adminF.isSignedIn().then(function (success) { deferred.resolve(true); }, function (error) { deferred.reject(false); return $state.go('admin.signin'); });
        return deferred.promise;
	};

	/* sign out action */
	var adminSignOut = function ($q , $state, adminF) {
		var deferred = $q.defer(); adminF.signOut().then(function (success) { deferred.resolve(true); return $state.go('admin.signin'); }, function (error) { deferred.reject(false); });
        return deferred.promise;
	};

	/* other resolves */
	var users = function ($q , $state, usersF) {
		var deferred = $q.defer();
		usersF.getAll()
		.then(function (success) {
			deferred.resolve(success.users);
		}, function (error) {
			deferred.reject(false);
		});
        return deferred.promise;
	};

	/* states */
	$stateProvider

	/* area admin */
	.state('admin', {
		url: '/admin',
		abstract: true,
		template: '<div ui-view id="{{ section.sub }}"></div>'
	})
		/* subarea signin */
		.state('admin.signin', {
			url: '/signin',
			templateUrl: 'app/modules/admin/signin/view.html',
			controller: 'adminSigninC', controllerAs: 'signin',
			resolve: { adminSignedOut: adminSignedOut }
		})
		/* subarea signout */
		.state('admin.signout', {
			url: '/signout',
			resolve: { adminSignedIn: adminSignedIn, adminSignOut: adminSignOut }
		})

	/* area users */
	.state('users', {
		url: '/users',
		templateUrl: 'app/modules/users/view.html',
		controller: 'usersC', controllerAs: 'users',
		resolve: {
			adminSignedIn: adminSignedIn,
			users: users
		}
	})

		/* subarea edit */
		.state('users.edit', {
			url: '/edit/:users_id',
			templateUrl: 'app/modules/users/edit/view.html',
			controller: 'usersEditC'
		})

		/* subarea edit */
		.state('users.create', {
			url: '/create/:users_id',
			templateUrl: 'app/modules/users/create/view.html',
			controller: 'usersCreateC'
		})

		/* subarea list */
		.state('users.detail', {
			url: '/detail/:users_id',
			templateUrl: 'app/modules/users/detail/view.html',
			controller: 'usersDetailC', controllerAs: 'Detail'
		})

	;

	/* redirects */
	var home = '/users';

	$urlRouterProvider.when('', home);
	$urlRouterProvider.when('/', home);
	$urlRouterProvider.otherwise(home);

	/* interceptor */
	$httpProvider.interceptors.push('requestInterceptor');

};

app.config(configFn);