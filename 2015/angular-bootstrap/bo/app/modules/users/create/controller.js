var usersCreateC = function ($scope, flashF, usersF, $timeout, $state) {

    var vm = $scope;

    vm.submit = function (isValid, input) {
        if (!isValid) return;

        usersF
        .create(input)
        .then(function (success) {

            $timeout(function () {
                vm.users.list = success.users;
            }, 0);

            flashF.show('Utilizador criado com sucesso.', 'alert-success');

            $state.go('users');
        });
    };
};

app.controller('usersCreateC', usersCreateC);