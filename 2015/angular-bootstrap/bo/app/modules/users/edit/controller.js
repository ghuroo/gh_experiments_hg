var usersEditC = function ($scope, flashF, $stateParams, usersF, $timeout, $state) {

    var vm = $scope;

    vm.users_id = $stateParams.users_id;
    vm.users_index = findInObject(vm.users.list, 'id', vm.users_id);

    vm.submit = function (isValid, input) {
        var confirmed = confirm('Confirma a acção?');
        if (!confirmed) return;

        if (!isValid) return;

        usersF
        .update(input, vm.users_id)
        .then(function (success) {

            $timeout(function () {
                vm.users.list = success.users;
            }, 0);

            flashF.show('Utilizador editado com sucesso.', 'alert-success');

            $state.go('users');
        });
    };

    vm.deactivate = function (users_id) {
        var confirmed = confirm('Confirma a acção?');
        if (!confirmed) return;

        usersF
        .deactivate(users_id)
        .then(function (success) {

            $timeout(function () {
                vm.users.list = success.users;
            }, 0);

            flashF.show('Utilizador criado com sucesso.', 'alert-success');

            $state.go('users');
        });
    };
};

app.controller('usersEditC', usersEditC);