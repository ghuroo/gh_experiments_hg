var menuC = function($state) {
    var vm = this;
};

app.directive('menu', function() {
    return {
        restrict: 'A',
        templateUrl: 'app/modules/layout/menu.html',
        replace: false,
        scope: false,
        controller: menuC,
        controllerAs: 'menu'
    }
});