var adminSigninC = function ($state, $q, $timeout, adminF, flashF) {
	var vm = this;

    vm.input = {};

    if (window.location.href.indexOf('localhost') > -1) {
        vm.input = {
            username: 'bruno.restolho@rd-agency.com',
            password: '123456'
        };
    };

	vm.submit = function(isValid, input) {
        if(!isValid) return;

        return adminF
        .signin(input.username, input.password)
        .then(function (success) {
            adminF.changeAdmin(success.admin, true);

            $state.go('users');
        }, function (error) {
            // flashF.show('Dados inválidos.', 'alert-warning');
            alert('Dados inválidos.');
        });
	};
};

app.controller('adminSigninC', adminSigninC);