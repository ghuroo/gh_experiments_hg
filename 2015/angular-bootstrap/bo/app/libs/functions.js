var isEmpty = function (value) {
    var res = null;

    if ( angular.isArray(value) ) {
        return value.length === 0;

    } else if ( angular.isObject(value) ) {

        for (var prop in value) {
            if(value.hasOwnProperty(prop))
                return false;
        }

        return true;

    } else if ( angular.isString(value) ) {
        return value === '' || value === null;

    } else if ( angular.isUndefined(value) ) {
        return value === '' || value === null || value === undefined;

    } else {
        return value === '' || value === null || value === undefined;
    }

}

var isEmptyObject = function (_object) {
    return Object.keys(_object).length;
}

var findInObject = function (arr, key, value) {
    for (var i=0, iLen=arr.length; i<iLen; i++) {

        if (arr[i][key] == value) return i;
    }
}