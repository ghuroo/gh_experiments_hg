var adminF = function ($rootScope, $http, $localStorage, $q, $state, CONSTANTS) {

    var service = {};

    /* generic */
    service.hasToken = function () {
        return !isEmpty($localStorage[CONSTANTS.TOKEN_NAME]);
    };

    service.signin = function (username, password) {
        var deferred = $q.defer();

        $http.post(CONSTANTS.API + '/admin/signin', {
            username: username,
            password: password
        })
        .success(function (success) {
            deferred.resolve(success);
        }).error(function (error) {
            deferred.reject(false);
        });

        return deferred.promise;
    };

    service.isSignedOut = function () {
        var deferred = $q.defer();

        if ($rootScope.admin) {

            deferred.reject(false);

        } else {

            if ($localStorage[CONSTANTS.TOKEN_NAME]) {
                delete $localStorage[CONSTANTS.TOKEN_NAME];
            };

            deferred.resolve(true);
        };

        return deferred.promise;
    };

    service.isSignedIn = function () {
        var deferred = $q.defer();

        if ($rootScope.admin) {
            deferred.resolve($rootScope.admin);
        } else {

            if ($localStorage[CONSTANTS.TOKEN_NAME]) {
                return $http.get(CONSTANTS.API + '/admin/signin/' + $localStorage[CONSTANTS.TOKEN_NAME])
                .success(function (success) {

                    service.changeAdmin(success.admin, true);
                    deferred.resolve(true);

                }).error(function (error) {
                    deferred.reject(false);
                });
            };

            deferred.reject(false);
        };

        return deferred.promise;
    };

    service.changeAdmin = function (admin, tokenAlso) {
        service.admin = admin;
        $rootScope.admin = admin;

        tokenAlso ? $localStorage[CONSTANTS.TOKEN_NAME] = admin.token : delete $localStorage[CONSTANTS.TOKEN_NAME];

        return true;
    };

    service.signOut = function () {
        var deferred = $q.defer();

        delete service.admin;
        delete $rootScope.admin;
        delete $localStorage[CONSTANTS.TOKEN_NAME];

        deferred.resolve();

        return deferred.promise;
    };

    return service;
}

app.factory('adminF', adminF);