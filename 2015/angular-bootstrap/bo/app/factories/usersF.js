var usersF = function ($http, $q, CONSTANTS) {

    var service = {};

    /* getAll */
    service.getAll = function () {
        var deferred = $q.defer();

        $http.get(CONSTANTS.API + '/users/')
        .success(function (success) {
            deferred.resolve(success);
        }).error(function (error) {
            deferred.reject(false);
        });

        return deferred.promise;
    };

    service.update = function (input, users_id) {
        var deferred = $q.defer();

        $http
        .post(CONSTANTS.API + '/users/update/'+users_id, input)
        .then(function (success) {

            if (input.image) {
                if (input.image[0] != null) {

                    return Upload
                    .upload({
                        url: CONSTANTS.API + '/users/update/image/'+users_id,
                        file: input.image
                    })
                    .success(function (data, status, headers, config) {
                        deferred.resolve(data)
                    }).error(function () {
                        deferred.reject();
                    });

                } else {
                    deferred.reject();
                }
            } else {
                deferred.resolve(success.data)
            }

        }, function (error) {
            deferred.reject(error);
        });

        return deferred.promise;
    };

    service.create = function (input) {
        var deferred = $q.defer();

        $http
        .post(CONSTANTS.API + '/users/create', input)
        .then(function (success) {

            deferred.resolve(success.data);

            // return Upload
            // .upload({
            //     url: CONSTANTS.API + '/users/update/image/'+success.data.users_id,
            //     file: input.image
            // })
            // .success(function (data, status, headers, config) {
            //     deferred.resolve(data)
            // }).error(function () {
            //     deferred.reject();
            // });

        }, function (error) {
            deferred.reject(false);
        });

        return deferred.promise;
    };

    service.deactivate = function (users_id) {
        var deferred = $q.defer();

        $http
        .get(CONSTANTS.API + '/users/deactivate/'+users_id)
        .then(function (success) {
            deferred.resolve(success.data);
        }, function (error) {
            deferred.reject(false);
        });

        return deferred.promise;
    };

    return service;
}


app.factory('usersF', usersF);