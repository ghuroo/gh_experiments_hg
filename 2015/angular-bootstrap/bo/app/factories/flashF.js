var flashF = function ($timeout, $rootScope) {
	$rootScope.alert = {};

    var timeout = null;

    return {

        show: function(message, level) {
            $timeout.cancel(timeout);

            timeout = $timeout(function() {
            	$rootScope.alert.show = true;
                $rootScope.alert.message = message;
                $rootScope.alert.level = level;

                $timeout(function() {
                    $rootScope.alert.show = false;
                    $rootScope.alert.message = null;
                }, 2000);
            }, 0);
        },

        hide: function(ms) {
            $timeout.cancel(timeout);

            timeout = $timeout(function() {
                $rootScope.alert.show = false;
                $rootScope.alert.message = null;
            }, 2000);
        }
    };
};

app.factory('flashF', flashF);