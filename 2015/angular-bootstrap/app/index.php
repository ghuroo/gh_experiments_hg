<?php $version = '0.0.1'; $version_src = '?v='.$version; ?>

<!DOCTYPE html>
<html xmlns:ng="http://angularjs.org" id="ng-app" ng-app="angular-bootstrap">
<head>

	<!-- Meta Tags -->
    <meta charset="utf-8">
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<meta http-equiv="content-language" content="pt"/>
	<meta name="author" content="R&D">
	<meta name="copyright" content="angular-bootstrap">
	<meta http-equiv="desription" content="angular-bootstrap"/>
	<meta http-equiv="keyword" content="angular-bootstrap"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- Custom Meta Tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

	<!-- Cache -->
	<meta http-equiv="cache-control" content="max-age=0" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
	<meta http-equiv="pragma" content="no-cache" />

	<!-- Title -->
	<title>angular-bootstrap</title>

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico<?=$version_src?>">
	<link rel="icon" sizes="16x16 32x32 64x64" href="favicon.ico<?=$version_src?>">
	<link rel="icon" type="image/png" sizes="196x196" href="public/favicon/favicon-196.png<?=$version_src?>">
	<link rel="icon" type="image/png" sizes="160x160" href="public/favicon/favicon-160.png<?=$version_src?>">
	<link rel="icon" type="image/png" sizes="96x96" href="public/favicon/favicon-96.png<?=$version_src?>">
	<link rel="icon" type="image/png" sizes="64x64" href="public/favicon/favicon-64.png<?=$version_src?>">
	<link rel="icon" type="image/png" sizes="32x32" href="public/favicon/favicon-32.png<?=$version_src?>">
	<link rel="icon" type="image/png" sizes="16x16" href="public/favicon/favicon-16.png<?=$version_src?>">
	<link rel="apple-touch-icon" sizes="152x152" href="public/favicon/favicon-152.png<?=$version_src?>">
	<link rel="apple-touch-icon" sizes="144x144" href="public/favicon/favicon-144.png<?=$version_src?>">
	<link rel="apple-touch-icon" sizes="120x120" href="public/favicon/favicon-120.png<?=$version_src?>">
	<link rel="apple-touch-icon" sizes="114x114" href="public/favicon/favicon-114.png<?=$version_src?>">
	<link rel="apple-touch-icon" sizes="76x76" href="public/favicon/favicon-76.png<?=$version_src?>">
	<link rel="apple-touch-icon" sizes="72x72" href="public/favicon/favicon-72.png<?=$version_src?>">
	<link rel="apple-touch-icon" href="public/favicon/favicon-57.png<?=$version_src?>">
	<meta name="msapplication-TileColor" content="#FFFFFF">
	<meta name="msapplication-TileImage" content="public/favicon/favicon-144.png<?=$version_src?>">
	<meta name="msapplication-config" content="browserconfig.xml">

    <!-- Bootstrap -->

    <!-- Styles -->
    <!-- <link href="public/css/angular-bootstrap.min.css<?=$version_src?>" rel="stylesheet" /> -->

	<!--[if IE 9]>
		<script src="vendor/js/modernizr.custom.73251.js<?=$version_src?>"></script>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js<?=$version_src?>"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js<?=$version_src?>"></script>
	<![endif]-->

	<!-- Safari -->
	<meta name="apple-mobile-web-app-capable" content="yes">

	<!-- Prevent Indexing -->
	<meta name="robots" content="noindex">

	<script type="text/javascript"> window.version = "<?=$version?>"; </script>

</head>

<body>

    <header menu></header>

	<section ui-view id="{{ section.main }}"></section>

	<!-- Vendor Vanilla -->
	<script src="vendor/js/fastclick.js<?=$version_src?>"></script>

	<!-- Vendor jQuery -->
	<script src="vendor/js/jquery-2.1.3.min.js<?=$version_src?>"></script>

	<!-- Libs -->
	<script src="libs/functions.js<?=$version_src?>"></script>

	<!-- Angular -->
	<script src="vendor/ng/angular.min.js<?=$version_src?>"></script>

	<!-- Vendor -->
	<script src="vendor/ng/angular-locale_pt-pt.js<?=$version_src?>"></script>
	<script src="vendor/ng/ngStorage.min.js<?=$version_src?>"></script>
	<script src="vendor/ng/angular-ui-router.js<?=$version_src?>"></script>

	<!-- Main -->
	<script src="config/app.js<?=$version_src?>"></script>
	<script src="config/config.js<?=$version_src?>"></script>
	<script src="config/constants.js<?=$version_src?>"></script>
	<script src="config/run.js<?=$version_src?>"></script>

	<!-- Directives -->
	<script src="directives/tools.js<?=$version_src?>"></script>
	<script src="directives/validators.js<?=$version_src?>"></script>

    <!-- Filters -->
    <script src="filters/filters.js<?=$version_src?>"></script>

    <!-- Factories -->
    <script src="factories/interceptorF.js<?=$version_src?>"></script>
    <script src="factories/userF.js<?=$version_src?>"></script>
    <script src="factories/productsF.js<?=$version_src?>"></script>

    <!-- Layout -->
    <script src="modules/layout/menu/directive.js<?=$version_src?>"></script>

	<!-- User -->
	<script src="modules/user/signin/controller.js<?=$version_src?>"></script>

	<!-- Products -->
	<script src="modules/products/controller.js<?=$version_src?>"></script>

</body>
</html>