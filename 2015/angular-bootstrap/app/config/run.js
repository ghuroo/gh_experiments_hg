var runFn = function ($rootScope, $state, CONSTANTS) {

    $rootScope.CONSTANTS = CONSTANTS;

    var parseSection = function (toState, fromState) {
        var sectionArray = toState.name.split('.');
        var section = {};
        if (!isEmpty(sectionArray[0])) section.main = sectionArray[0];
        if (!isEmpty(sectionArray[1])) section.sub = sectionArray[1];
        if (!isEmpty(sectionArray[2])) section.partial = sectionArray[2];

        var from = fromState.name.split('.')[0];
        var to = toState.name.split('.')[0];

        if (!isEmpty(from)) section.from = from;
        if (!isEmpty(to)) section.to = to;

        return section;
    }

    /* on stateChangeStart */
    var stateChangeStart = function (event, toState, toParams, fromState, fromParams) {
        /*console.log();*/
    };

    $rootScope.$on('$stateChangeStart', stateChangeStart);

    /* on stateChangeError */
    var stateChangeError = function (event, toState, toParams, fromState, fromParams, error) {
        console.log(error);
    };

    $rootScope.$on('$stateChangeError', stateChangeError);

    /* on stateChangeSuccess */
    var stateChangeSuccess = function (event, toState, toParams, fromState, fromParams) {
        $rootScope.section = parseSection(toState, fromState);
    };

    $rootScope.$on('$stateChangeSuccess', stateChangeSuccess);

    // FastClick
    FastClick.attach(document.body);

    // debug
    if (window.location.href.indexOf('localhost') > -1) {
        window.debug = true;
    } else {
        window.debug = false;
    };

}

app.run(runFn);