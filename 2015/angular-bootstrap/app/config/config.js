var configFn = function ($stateProvider, $urlRouterProvider, $httpProvider, CONSTANTS) {

	/* user resolves */
	var userIsSignedOut = function ($q, $state, userF) {
		var deferred = $q.defer();
		if (debug) console.log('here we go?');
		userF.isSignedOut().then(function (success) {
			if (debug) console.log('signed out');
			deferred.resolve(false);
		}, function (error) {
			deferred.reject(false);
			if (debug) console.log('signed in');
			return $state.go('products');
		});
        return deferred.promise;
	};

	var userSignedIn = function ($q, $state, userF) {
		var deferred = $q.defer();
		userF.isSignedIn().then(function (success) {
			deferred.resolve(true);
		}, function (error) {
			deferred.reject(false);
			return $state.go('user.signin');
		});
        return deferred.promise;
	};

	var userSignOut = function ($q, $state, userF) {
		var deferred = $q.defer();
		userF.signOut().then(function (success) {
			deferred.resolve(true);
			return $state.go('user.signin');
		}, function (error) {
			deferred.reject(false);
		});
        return deferred.promise;
	};

	/* other resolves */
	var products = function ($q , $state, productsF) {
		var deferred = $q.defer();
		productsF.getAll()
		.then(function (success) {
			deferred.resolve(success.products);
		}, function (error) {
			deferred.reject(false);
		});
        return deferred.promise;
	};

	/* states */
	$stateProvider

	/* area user */
	.state('user', {
		url: '/user',
		abstract: true,
		template: '<div ui-view id="{{ section.sub }}"></div>'
	})
		/* subarea signin */
		.state('user.signin', {
			url: '/signin',
			templateUrl: 'modules/user/signin/view.html',
			controller: 'userSigninC', controllerAs: 'signin',
			resolve: { userIsSignedOut: userIsSignedOut }
		})
		/* subarea signout */
		.state('user.signout', {
			url: '/signout',
			resolve: { userSignedIn: userSignedIn, userSignOut: userSignOut }
		})

	/* area products */
	.state('products', {
		url: '/products',
		templateUrl: 'modules/products/view.html',
		controller: 'productsC', controllerAs: 'products',
		resolve: {
			userSignedIn: userSignedIn,
			products: products
		}
	})
		/* subarea list */
		.state('products.detail', {
			url: '/detail',
			templateUrl: 'modules/products/detail/view.html',
			controller: 'productsDetailC', controllerAs: 'Detail'
		})

	;

	/* redirects */
	var home = '/products';

	$urlRouterProvider.when('', home);
	$urlRouterProvider.when('/', home);
	$urlRouterProvider.otherwise(home);

	/* interceptor */
	$httpProvider.interceptors.push('requestInterceptor');

};

app.config(configFn);