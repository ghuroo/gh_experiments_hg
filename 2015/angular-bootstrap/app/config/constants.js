var constants = {
    'TOKEN_NAME': 'bootstrap_angular',
    'API': '../api',
    'IMG': 'public/img',
    'VERSION': window.version
};

app.constant('CONSTANTS', constants);