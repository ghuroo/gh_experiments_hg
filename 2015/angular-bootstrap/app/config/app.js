var appname = 'angular-bootstrap';

var dependencies = [
    'ngStorage',
    'ui.router',
    'validators'
];

angular
.module(appname, dependencies);

var app = angular.module(appname);