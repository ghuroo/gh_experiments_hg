var menuC = function($state) {
    var vm = this;
};

app.directive('menu', function() {
    return {
        restrict: 'A',
        templateUrl: 'modules/layout/menu/view.html',
        replace: false,
        scope: false,
        controller: menuC,
        controllerAs: 'menu'
    }
});