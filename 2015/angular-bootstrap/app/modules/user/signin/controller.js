var userSigninC = function ($state, $q, $timeout, userF) {
	var vm = this;

    vm.input = {};

    if (window.debug) {
        vm.input = {
            username: 'testes@rd-agency.com',
            password: 'teste'
        };
    };

	vm.submit = function(isValid, input) {
        if(!isValid) return;

        return userF
        .signin(input.username, input.password)
        .then(function (success) {
            userF.changeUser(success.user, true, input.save);

            $state.go('products');
        }, function (error) {});
	};
};

app.controller('userSigninC', userSigninC);