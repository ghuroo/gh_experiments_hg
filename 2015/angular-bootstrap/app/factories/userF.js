var userF = function ($rootScope, $http, $localStorage, $sessionStorage, $q, $state, CONSTANTS) {

    var service = {};

    service.signin = function (username, password) {
        var deferred = $q.defer();

        $http.post(CONSTANTS.API + '/user/signin', {
            username: username,
            password: password
        })
        .success(function (success) {
            deferred.resolve(success);
        }).error(function (error) {
            deferred.reject(false);
        });

        return deferred.promise;
    };

    service.isSignedOut = function () {
        var deferred = $q.defer();
        if (debug) console.log('is signed out?');

        if ($rootScope.user) {

            deferred.reject(false);

        } else {

            service.isSignedIn().then(function (success) {
                if (debug) console.log('tried successfully');
                deferred.reject(false);
            }, function (error) {
                if (debug) console.log('empty or wrong token');
                deferred.resolve(true);
            });

        };

        return deferred.promise;
    };

    service.isSignedIn = function () {
        var deferred = $q.defer();

        if ($rootScope.user) {
            deferred.resolve($rootScope.user);
        } else {

            if ($localStorage[CONSTANTS.TOKEN_NAME]) {
                if (debug) console.log('local');
                var token = $localStorage[CONSTANTS.TOKEN_NAME];
                var local = true;
            } else if ($sessionStorage[CONSTANTS.TOKEN_NAME]) {
                if (debug) console.log('session');
                var token = $sessionStorage[CONSTANTS.TOKEN_NAME];
                var local = false;
            };

            if (token) {
                return $http.get(CONSTANTS.API + '/user/signin/' + token)
                .success(function (success) {
                    if (debug) console.log('signed in, local?', local);

                    service.changeUser(success.user, true, local);
                    deferred.resolve(true);

                }).error(function (error) {
                    deferred.reject(false);
                });
            };

            deferred.reject(false);
        };

        return deferred.promise;
    };

    service.isSignedIn = function () {
        var deferred = $q.defer();

        if ($rootScope.user) {
            deferred.resolve($rootScope.user);
        } else {

            if ($localStorage[CONSTANTS.TOKEN_NAME]) {
                if (debug) console.log('local');
                var token = $localStorage[CONSTANTS.TOKEN_NAME];
                var local = true;
            } else if ($sessionStorage[CONSTANTS.TOKEN_NAME]) {
                if (debug) console.log('session');
                var token = $sessionStorage[CONSTANTS.TOKEN_NAME];
                var local = false;
            };

            if (token) {
                return $http.get(CONSTANTS.API + '/user/signin/' + token)
                .success(function (success) {
                    if (debug) console.log('signed in, local?', local);

                    service.changeUser(success.user, true, local);
                    deferred.resolve(true);

                }).error(function (error) {
                    deferred.reject(false);
                });
            };

            deferred.reject(false);
        };

        return deferred.promise;
    };

    service.changeUser = function (user, tokenAlso, localToken) {
        service.user = user;
        $rootScope.user = user;

        if (tokenAlso) {

            if (localToken) {
                $localStorage[CONSTANTS.TOKEN_NAME] = user.token;
                delete $sessionStorage[CONSTANTS.TOKEN_NAME];
            } else {
                $sessionStorage[CONSTANTS.TOKEN_NAME] = user.token;
                delete $localStorage[CONSTANTS.TOKEN_NAME];
            }

        } else {
            delete $localStorage[CONSTANTS.TOKEN_NAME];
            delete $sessionStorage[CONSTANTS.TOKEN_NAME];
        }

        return true;
    };

    service.signOut = function () {
        var deferred = $q.defer();

        delete service.user;
        delete $rootScope.user;
        delete $localStorage[CONSTANTS.TOKEN_NAME];
        delete $sessionStorage[CONSTANTS.TOKEN_NAME];

        deferred.resolve(true);

        return deferred.promise;
    };

    return service;
}

app.factory('userF', userF);