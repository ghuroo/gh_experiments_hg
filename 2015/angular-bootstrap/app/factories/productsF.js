var productsF = function ($http, $q, CONSTANTS) {

    var service = {};

    /* getAll */
    service.getAll = function () {
        var deferred = $q.defer();

        $http.get(CONSTANTS.API + '/products/')
        .success(function (success) {
            deferred.resolve(success);
        }).error(function (error) {
            deferred.reject(false);
        });

        return deferred.promise;
    };

    /* get example */
    service.getDetail = function (product_id) {
        var deferred = $q.defer();

        $http.get(CONSTANTS.API + '/products?product_id='+product_id)
        .success(function (success) {
            deferred.resolve(success);
        }).error(function (error) {
            deferred.reject(false);
        });

        return deferred.promise;
    };

    /* post example */
    service.edit = function (product_id) {
        var deferred = $q.defer();

        $http.post(CONSTANTS.API + '/products/edit',
            {
                id: product_id
            }
        )
        .success(function (success) {
            deferred.resolve(success);
        }).error(function (error) {
            deferred.reject(false);
        });

        return deferred.promise;
    };

    return service;
}


app.factory('productsF', productsF);