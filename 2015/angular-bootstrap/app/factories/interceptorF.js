var requestInterceptor = function ($q, $localStorage, $sessionStorage, CONSTANTS) {

    var service = {};

    service.extensions = ["html", "js", "gif", "jpg", "jpeg", "png", "ico", "svg"];
    service.patt = new RegExp('.' + service.extensions.join('|.'), 'i');

    /* On request success */
    service.request = function (config) {
        if (service.patt.test(config.url) && (!config.url.indexOf('http') == 0)) {
            config.url += '?v=' + CONSTANTS.VERSION;
        };

        config.headers = config.headers || {};

        if ($localStorage[CONSTANTS.TOKEN_NAME]) {
            config.headers['X-Auth-Token'] = $localStorage[CONSTANTS.TOKEN_NAME];
        } else if ($sessionStorage[CONSTANTS.TOKEN_NAME]) {
            config.headers['X-Auth-Token'] = $sessionStorage[CONSTANTS.TOKEN_NAME];
        }

        return config || $q.when(config);
    };

    /* On request failure */
    service.requestError = function (rejection) {

        return $q.reject(rejection);
    };

    /* On response success */
    service.response = function (response) {

        return response || $q.when(response);
    };

    /* On response failure */
    service.responseError = function (rejection) {

        if(rejection.status === 401 || rejection.status === 403) {
            delete $localStorage[CONSTANTS.TOKEN_NAME];
        }

        return $q.reject(rejection);
    };

    return service;

};

app.factory('requestInterceptor', requestInterceptor);