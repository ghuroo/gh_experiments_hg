<?

class Products {

	/**
	products
	*/

	public static function getAll () {

		$query = ' SELECT
			p.id,
			name,
			description,
            UNIX_TIMESTAMP(timestamp) as timestamp
		FROM products AS p
		WHERE
			active = 1
		';

		$products = R::getAll($query);
		// if (empty($products)) throw new Exception('Não há produtos. ' . __FUNCTION__);

		foreach ($products as $key => $value) {
			$products[$key]['id'] = (int) $value['id'];
			$products[$key]['timestamp'] = (int) $value['timestamp'];
		}

		return $products;
	}

}