<?

class User {
    static $tableUser = 'user';

    /**
    read by username and password
    */

    public static function ReadBySignIn ($username = NULL, $password = NULL) {
        if (empty($username) || empty($password)) throw new Exception('Dados vazios. ' . __FUNCTION__);

        $q = 'SELECT id,
            username,
            name,
            email,
            UNIX_TIMESTAMP(last_signin) as last_signin,
            UNIX_TIMESTAMP(last_activity) as last_activity,
            token
            FROM '.self::$tableUser.' as user
            WHERE username = :username
            AND password = :password
        ';

        $b = array(
            'username' => $username,
            'password' => md5($password)
        );

        $user = R::getRow($q, $b);
        if (empty($user)) throw new Exception('Utilizador inválido. ' . __FUNCTION__);

        $user['last_signin'] = (int) $user['last_signin'];
        $user['last_activity'] = (int) $user['last_activity'];

        return $user;
    }

    /**
    read by id
    */

    public static function ReadByID ($id = NULL) {
        if (empty($id)) throw new Exception('Dados vazios. ' . __FUNCTION__);

        $q = 'SELECT
            username,
            name,
            email,
            UNIX_TIMESTAMP(last_signin) as last_signin,
            UNIX_TIMESTAMP(last_activity) as last_activity,
            token
            FROM '.self::$tableUser.' as user
            WHERE id = :id
        ';

        $b = array(
            'id' => $id
        );

        $user = R::getRow($q, $b);
        if (empty($user)) throw new Exception('O utilizador não existe. ' . __FUNCTION__);

        $user['last_signin'] = (int) $user['last_signin'];
        $user['last_activity'] = (int) $user['last_activity'];

        return $user;
    }

    /**
    read by email
    */

    public static function ReadByEmail ($email = NULL) {
        if (empty($email)) throw new Exception('Dados vazios. ' . __FUNCTION__);

        $q = 'SELECT id,
            name,
            password
            FROM '.self::$tableUser.' as user
            WHERE email = :email
        ';

        $b = array(
            'email' => $email
        );

        $user = R::getRow($q, $b);
        if (empty($user)) throw new Exception('Utilizador inválido. ' . __FUNCTION__);

        return $user;
    }

    /**
    update signin time
    */

    public static function UpdateSignInTime ($id = NULL) {
        if (empty($id)) throw new Exception('Dados vazios. ' . __FUNCTION__);

        $bean = R::load(self::$tableUser, $id);
        $bean->last_signin = date(DATE_FORMAT);
        $bean->last_activity = date(DATE_FORMAT);

        $user = R::store($bean);
        if (empty($user)) throw new Exception('Erro ao gravar alterações. ' . __FUNCTION__);

        return $user;
    }

    /**
    update activity
    */

    public static function UpdateActivity ($id = NULL) {
        if (empty($id)) throw new Exception('Dados vazios. ' . __FUNCTION__);

        $bean = R::load(self::$tableUser, $id);
        $bean->last_activity = date(DATE_FORMAT);

        $user = R::store($bean);
        if (empty($user)) throw new Exception('Erro ao gravar alterações. ' . __FUNCTION__);

        return $user;
    }

    /**
    check token
    */

    public static function CheckToken ($user_id = NULL, $token = NULL) {
        if (empty($user_id) || empty($token)) throw new Exception('Dados vazios. ' . __FUNCTION__);

        $q = 'SELECT
            user.id
            FROM '.self::$tableUser.' as user
            WHERE user.id = :user_id
            AND user.token = :token
        ';

        $b = array(
            'user_id' => $user_id,
            'token' => $token
        );

        $user_id = R::getCell($q, $b);

        return (int) $user_id;
    }

}