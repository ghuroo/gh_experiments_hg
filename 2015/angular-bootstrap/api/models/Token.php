<?

class Token {
    static $tableUser = 'user';
    static $tableToken = 'token';

    public static function Create ($user_id = NULL) {
        if (empty($user_id)) throw new Exception('Dados vazios. (' . __FUNCTION__ . ')');

        $app = \Slim\Slim::getInstance();

        $url = $app->request->getRootUri();

        $token = array(
            'user_id' => (int) $user_id,
            'iss' => $url,
            'iat' => time(),
            'exp' => time() + (3 * 24 * 60 * 60)
            // 'exp' => time() + (1 * 1 * 1 * 5)
        );

        $token_new = JWT::encode($token, SECRET_SERVER_KEY);
        if (empty($token_new)) throw new Exception('Erro ao inserir token. (' . __FUNCTION__ . ')');

        /* save token */
        $bean = R::load(self::$tableUser, $user_id);
        $bean->token = $token_new;

        $user = R::store($bean);
        if (empty($user)) throw new Exception('Erro ao gravar token. (' . __FUNCTION__ . ')');

        // echo '\n'.date('Y-m-d H:i:s', time()).'\n'.date('Y-m-d H:i:s', $token['exp']);
        // die();

        return $token_new;
    }

    public static function ReadByID ($id = NULL) {
        if (empty($id)) throw new Exception('Dados vazios. (' . __FUNCTION__ . ')');

        $q = 'SELECT '.self::$tableToken.' as token FROM user WHERE id = :id';
        $b = array('id' => $id);

        return R::getCell($q, $b);
    }

    public static function Check ($token = NULL) {
        $app = \Slim\Slim::getInstance();

        try {
            $token_decoded = JWT::decode($token, SECRET_SERVER_KEY);

            $user_id = $token_decoded->user_id;

            $token_valid = User::CheckToken($user_id, $token);

            if (empty($token_valid)) throw new Exception();

            return $token_decoded;

        } catch (Exception $e) {
            return false;
        }
    }

}