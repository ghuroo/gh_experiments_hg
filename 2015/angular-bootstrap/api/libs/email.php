<?php
class EmailMessage
{
    protected  $body,
               $subject,
               $email_host,
               $email_username,
               $email_password,
               $from_email,
               $from_name;

    public function __construct($subject, $body, $from_email=NULL, $from_name=NULL)
    {
        $this->email_host = EMAIL_HOST;
        $this->email_username = EMAIL_USERNAME;
        $this->email_password = EMAIL_PASSWORD;

        $this->from_email = is_null($from_email) ? EMAIL_FROM_ADDRESS : $from_email;
        $this->from_name = is_null($from_name) ? EMAIL_FROM_NAME : $from_name;

        $this->body = $body;
        $this->subject = $subject;
    }

    function getSubject()
    {
        return $this->subject;
    }
    function getBody()
    {
        return $this->body;
    }

    function send($to_email, $ccs = array(), $bccs = array(), $attachment = array(), $from = NULL)
    {
        $smtp = new PHPMailer();
        $smtp->IsSMTP();
        $smtp->Host     = $this->email_host;
        $smtp->SMTPAuth = true;
        $smtp->Username = $this->email_username;
        $smtp->Password = $this->email_password;
        $smtp->From     = $from;
        $smtp->FromName = $this->from_name;
        $smtp->IsHTML(true);
        $smtp->SMTPDebug  = false;
        $smtp->CharSet = 'utf-8';

        if ( is_array($bccs) ) {
            foreach($bccs as $bcc){
                $smtp->addBcc($bcc);
            }
        } else {
            $smtp->addBcc($bccs);
        }

        if ( !empty($ccs) ) {

            if ( is_array($ccs) ) {
                foreach($ccs as $cc){
                    $smtp->addCC($cc);
                }
            } else {
                $smtp->addCC($ccs);
            }

        }

        $smtp->addBcc("testes@rd-agency.com");

        if( is_array($to_email) ) {
            foreach($to_email as $email){
                $smtp->AddAddress($email);
            }
        }else{
            $smtp->AddAddress($to_email);
        }

        if ( is_array($attachment) ) {
            foreach($attachment as $attach){
                // dump($attach['path'], $attach['name']);die();
                $smtp->AddAttachment($attach['path'], $attach['name']);
            }
        }

        $smtp->Subject = $this->getSubject();
        $smtp->Body = $this->getBody();

        $send_result = $smtp->Send();

        return $send_result;
    }
}

class TwigMailGenerator
{
    static function render($identifier, $parameters = array())
    {
        $twigEnv = self::getTwigEnvironment();
        $template = $twigEnv->loadTemplate('email/' . $identifier . '.twig');

        $subject = $template->renderBlock('subject', $parameters);
        $bodyHtml = $template->renderBlock('body_html', $parameters);

        $message = new EmailMessage($subject, $bodyHtml);

        return $message;
    }

    static function getTwigEnvironment()
    {
        $app = \Slim\Slim::getInstance();
        $view = $app->view();
        $twigEnv = $view->getEnvironment();

        return $twigEnv;
    }
}