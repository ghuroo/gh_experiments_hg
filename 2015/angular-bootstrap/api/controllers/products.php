<?

/**
get all
*/

$app->get('/products(/)', function () use($app) {
    $response = array();

    // validate token
    $input_token = $app->request()->headers()['X-Auth-Token'];
    try { $token = Token::Check($input_token); if (empty($token)) throw new Exception(); }
    catch (Exception $e) { $response['status'] = 'Sessão expirada.'; $app->response->setStatus(401); $token = false; }

    try {

        /* update activity */
        User::UpdateActivity($token->user_id);

        /* output */
        $response['products'] = Products::getAll();

    } catch (Exception $e) {
        $response['status'] = $e->getMessage();
        $app->response->setStatus(500);
    }

    U::RenderJSON($response);

});

/**
edit
*/

$app->post('/products/edit(/)', function () use($app) {
    $response = array();

    // validate token
    $input_token = $app->request()->headers()['X-Auth-Token'];
    try { $token = Token::Check($input_token); if (empty($token)) throw new Exception(); }
    catch (Exception $e) { $response['status'] = 'Sessão expirada.'; $app->response->setStatus(401); $token = false; }

    try {

        /* input */
        $input = json_decode($app->request->getBody());

        /* update activity */
        User::UpdateActivity($token->user_id);

        /* logica que quiseres */

        /* output */
        $response['oqquiseres'] = $oqquiseres;

    } catch (Exception $e) {
        $response['status'] = $e->getMessage();
        $app->response->setStatus(500);
    }

    U::RenderJSON($response);

});