<?

/**
signin
*/

$app->post('/user/signin(/)', function () use($app) {
    $response = array();

    try {

        /* input */
        $input = json_decode($app->request->getBody());

        /* user */
        $user = User::ReadBySignIn($input->username, $input->password);

        /* token */
        $token = Token::Check($user['token']);
        if (empty($token)) Token::Create($user['id']);

        /* get token */
        $token = Token::ReadByID($user['id']);

        /* update activity */
        User::UpdateSignInTime($user['id']);

        /* get user */
        $user = User::ReadByID($user['id']);

        /* output */
        $response['user'] = $user;

    } catch (Exception $e) {
        $response['status'] = $e->getMessage();
        $app->response->setStatus(500);
    }

    U::RenderJSON($response);
});

/**
signin by token
*/

$app->get('/user/signin/:input_token(/)', function ($input_token = NULL) use($app) {
    $response = array();

    // validate token
    try { $token = Token::Check($input_token); if (empty($token)) throw new Exception(); }
    catch (Exception $e) { $response['status'] = 'Sessão expirada.'; $app->response->setStatus(401); $token = false; }

    if ($token) {
        try {

            /* update activity */
            User::UpdateSignInTime($token->user_id);

            /* get user */
            $user = User::ReadByID($token->user_id);

            /* output */
            $response['user'] = $user;

        } catch (Exception $e) {
            $response['status'] = $e->getMessage();
            $app->response->setStatus(500);
        }
    }

    U::RenderJSON($response);
});