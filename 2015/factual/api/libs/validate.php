<?php

class Validate {

    private static $regex = array(
        'name' => "/^.*\s.*$/",
        'password' => '/^.{4,}$/',
        'cc' => '/^\d{6,}$/',
        'email' => '/^[A-Z0-9._%-]+@[A-Z0-9][A-Z0-9.-]{0,61}[A-Z0-9]\.[A-Z]{2,6}$/i',
        'url' => '/^(([\w]+:)?\/\/)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&amp;?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/',
        'phone' => '/^\d{9,}$/',
        'text' => '/^.{1,}$/'
    );

    public static function this($object = NULL) {

        foreach ($object as $key => $value) {

            switch ($key) {

                case 'name':
                if (!preg_match(self::$regex['name'], $value)) throw new Exception('name');
                break;

                case 'enterprise':
                if (!preg_match(self::$regex['text'], $value)) throw new Exception('enterprise');
                    return true;
                break;

                case 'username':
                if (!preg_match(self::$regex['cc'], $value)) throw new Exception('username');
                break;

                case 'password':
                if (!preg_match(self::$regex['password'], $value)) throw new Exception('password');
                break;

                case 'cc':
                if (!preg_match(self::$regex['cc'], $value)) throw new Exception('cc');
                break;

                case 'email':
                if (!preg_match(self::$regex['email'], $value)) throw new Exception('email');
                break;

                case 'url':
                if (!preg_match(self::$regex['url'], $value)) throw new Exception('url');
                break;

                case 'phone':
                if (!preg_match(self::$regex['phone'], $value)) throw new Exception('phone');
                break;

            }
        }

        return true;
    }
}