<?php

// environment
if ($_SERVER['SERVER_NAME'] === 'localhost') {
    define( 'ENVIRONMENT', 'local' );
} else {
    define( 'ENVIRONMENT', 'prod' );
    // define( 'ENVIRONMENT', 'local' );
}

// redbean
require 'vendor/Redbean/rb.php';

// db connection
switch (ENVIRONMENT) {
    // case 'prod':
    //     $host = 'sql49.rd-agency.info';
    //     $dbname = 'rd_microsoft_office365_lp';
    //     $username = 'ms_off365_lp';
    //     $password = 'Zcruh_H0ElZ';
    // break;

    // case 'dev':
    //     $host = 'sql49.rd-agency.info';
    //     $dbname = 'rd_microsoft_office365_lp';
    //     $username = 'ms_off365_lp';
    //     $password = 'Zcruh_H0ElZ';
    // break;

    case 'local':
        $host = 'localhost';
        $dbname = 'gh_factual_cities';
        $username = 'root';
        $password = 'root';
    break;
}

R::setup('mysql:host='.$host.';dbname='.$dbname, $username, $password);

// http://www.redbeanphp.com/prefixes
// R::ext('xdispense', function($type){ return R::getRedBean()->dispense( $type); });

// slim
require 'vendor/Slim/Slim.php';
\Slim\Slim::registerAutoloader();

// email vars
define('EMAIL_FROM_ADDRESS', 'no-reply@emergency-agency.com');
define('EMAIL_FROM_NAME', 'Factual');
define('EMAIL_HOST', 'mail.emergency-agency.com');
define('EMAIL_USERNAME', 'info@emergency-agency.com');
define('EMAIL_PASSWORD', '2NwoD6xX0Lvu');

// phpmailer
require_once 'vendor/PHPMailer/PHPMailerAutoload.php';

// create app
$app = new \Slim\Slim();

$view = $app->view();
$view->parserOptions = array(
    'debug' => false
);

$app->config(array(
    'debug' => false
));

ini_set('display_errors', '1');
$app->error(function (\Exception $e) use ($app) { echo 'error.'; });

// libs
foreach (glob('libs/*.php') as $filename) { require_once $filename; }

// controllers
foreach (glob('controllers/*.php') as $filename) { require_once $filename; }

// models
foreach (glob('models/*.php') as $filename) { require_once $filename; }

// response headers
$app->response->headers->set('Access-Control-Allow-Origin', '*');
$app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST');
$app->response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With, X-Auth-Token, Content-Type');
$app->response->headers->set('Content-Type', 'application/json;charset=UTF-8');

// timezone
date_default_timezone_set("Europe/Lisbon");
define('DATE_FORMAT', 'Y-m-d H:i:s');

// start this biatch
$app->run();