<?

$app->get('/places(/)', function () use($app) {
    $response = array();

    try {

        $places = R::getAll(
            'SELECT
                id,
                name,
                latitude,
                longitude,
                gmaps_place_id
            FROM place'
        );

        $response['places'] = $places;

    } catch (Exception $e) {
        $response['status'] = $e->getMessage();
        $app->response->setStatus(500);
    }

    U::RenderJSON($response);
});