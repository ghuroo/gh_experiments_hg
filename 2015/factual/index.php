<?php $version = '?v=2.5'; ?>

<!DOCTYPE html>
<html lang="pt">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	    <meta name="description" content="Factual">
		<meta name="keywords" content="Factual">
		<meta name="author" content="Factual">

	    <title>Factual</title>

	    <!-- CSS -->
	    <link rel="stylesheet" href="css/reset.css<?=$version?>">
	    <link rel="stylesheet" href="css/normalize.css<?=$version?>">
	    <link rel="stylesheet" href="css/unsemantic-grid-responsive-tablet-no-ie7.css<?=$version?>">

		<!-- Custom CSS -->
		<link rel="stylesheet" href="css/style.css<?=$version?>">

	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script defer="defer" src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script defer="defer" src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	</head>

  	<body class="grid-container">

		<button type="button" id="lezgo">Lezgo</button>

		<!-- Scripts -->
		<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="js/script.js<?=$version?>"></script>
	</body>
</html>