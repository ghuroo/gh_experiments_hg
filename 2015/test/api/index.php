<?php

// environment

// uploads
define( 'UPLOADS', __DIR__ . '/upload' );

// keys
$privateKey = file_get_contents('keys/privkey.pem');
// $publicKey  = file_get_contents('keys/pubkey.pem');

define( 'SECRET_SERVER_KEY', $privateKey );

// jwt
require 'vendor/JWT/JWT.php';
foreach (glob('vendor/JWT/**/*.php') as $filename) { require_once $filename; }

// redbean
require 'vendor/Redbean/rb.php';

// db connection
$host = 'localhost';
$dbname = 'test';
$username = 'root';
$password = '';

R::setup('mysql:host='.$host.';dbname='.$dbname, $username, $password);

// http://www.redbeanphp.com/prefixes
R::ext('xdispense', function($type){ return R::getRedBean()->dispense( $type); });

// twig
require_once 'vendor/Twig/Autoloader.php';
Twig_Autoloader::register();

// slim
require 'vendor/Slim/Slim.php';
\Slim\Slim::registerAutoloader();

// slim+twig
require_once 'vendor/Slim-Views/Twig.php';
require_once 'vendor/Slim-Views/TwigExtension.php';

// create app
$app = new \Slim\Slim(array(
	'view' => new \Slim\Views\Twig(),
	'templates.path' => 'views'
));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true
);

$app->config(array(
    'debug' => true,
    'log.level' => \Slim\Log::DEBUG,
    'log.enabled' => true
));

// libs
foreach (glob('libs/*.php') as $filename) { require_once $filename; }

// controllers
foreach (glob('controllers/*.php') as $filename) { require_once $filename; }

// models
foreach (glob('models/*.php') as $filename) { require_once $filename; }

// response headers
$app->response->headers->set('Access-Control-Allow-Origin', '*');
$app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST');
$app->response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With, X-Auth-Token, Content-Type');
$app->response->headers->set('Content-Type', 'application/json;charset=UTF-8');

// timezone
date_default_timezone_set("Europe/Lisbon");

// start this biatch
$app->run();