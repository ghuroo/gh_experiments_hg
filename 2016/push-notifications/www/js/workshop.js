/* o nosso init() - tem que estar antes do addEventListener('deviceready') */
var aTuaFuncao = function(argument) {
	alert('Cordova Device Ready');
    console.log(device.cordova);
    console.log(device.model);
    console.log(device.platform);
    console.log(device.uuid);
    console.log(device.version);
    console.log(device.manufacturer);
    console.log(device.isVirtual);
    console.log(device.serial);

    /* notifications plugin init */
    var push = PushNotification.init(
    	{
    		'android': {'senderID': '860197541628'},
	        'ios': {'alert': 'true', 'badge': 'true', 'sound': 'true'},
	        'windows': {}
    	}
    );

    /* cada vez que gera um gcm id */
    push.on('registration', function(data) {
        console.log(data.registrationId);
        $('#gcm_id').html(data.registrationId);
    });

    /* quando recebe uma notificacao */
    push.on('notification', function(data) {
        console.log(data.message);
        alert(data.title+' Message: ' +data.message);
    });

    /* quando dá erro num push */
    push.on('error', function(e) {
        console.log(e.message);
        alert(data.message);
    });
};

/* cordova device ready */
document.addEventListener('deviceready', aTuaFuncao, false);