<?php

$app->get('/', function () use($app) {

    $input = $app->request->params();

    if (empty($input['app_id'])) throw new Exception("empty 'app_id'");
    if (empty($input['href'])) throw new Exception("empty 'href'");
    if (empty($input['title'])) throw new Exception("empty 'title'");
    if (empty($input['caption'])) throw new Exception("empty 'caption'");
    if (empty($input['image'])) throw new Exception("empty 'image'");
    if (empty($input['redirect_uri'])) throw new Exception("empty 'redirect_uri'");

    $input['random_string'] = U::randomString();

    U::renderVIEW('share', $input);

});