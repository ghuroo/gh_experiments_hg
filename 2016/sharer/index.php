<?php

// environment
if ($_SERVER['SERVER_NAME'] === 'localhost') {
    define('DEBUG', true);
    define('ENVIRONMENT', 'local');
} else {
    define('DEBUG', false);
    define('ENVIRONMENT', 'prod');
}

// twig
require_once 'vendor/Twig/Autoloader.php';
Twig_Autoloader::register();

// slim
require 'vendor/Slim/Slim.php';
\Slim\Slim::registerAutoloader();

// slim+twig
require_once 'vendor/Slim-Views/Twig.php';
require_once 'vendor/Slim-Views/TwigExtension.php';

// create app
$app = new \Slim\Slim(array(
	'view' => new \Slim\Views\Twig(),
	'templates.path' => 'views',
	'debug' => true
));

$view = $app->view();
// $view->parserOptions = array('debug' => false);

$app->setName('fnac-passatempo-tipo-leitor-2016');

ini_set('display_errors', '1');
$app->error(function (\Exception $e) use ($app) { echo $e; });

// libs
foreach (glob('libs/*.php') as $filename) { require_once $filename; }

// controllers
foreach (glob('controllers/*.php') as $filename) { require_once $filename; }

// models
foreach (glob('models/*.php') as $filename) { require_once $filename; }

// timezone
date_default_timezone_set("Europe/Lisbon");
define('DATE_FORMAT', 'Y-m-d H:i:s');

// start this biatch
$app->run();