<?php

class U {

	public static function renderJSON($array = NULL, $callback = NULL) {
		$app = \Slim\Slim::getInstance();

		if (empty($array)) {
			return false;
		} else {
			return $app->response->write( json_encode($array) );
		}
	}

    public static function renderVIEW($viewname = NULL, $array = NULL) {
        $app = \Slim\Slim::getInstance();

        if (empty($viewname)) {
            return false;
        } else {
            $app->response->headers->set('Content-Type', 'text/html;charset=UTF-8');
            return $app->render($viewname . '.twig', $array);
        }
    }

    public static function randomString() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }

        return implode($pass); //turn the array into a string
    }

    public static function parseKeyValue($array = NULL) {
        $res = array_map(function($obj) {
            return array(
                'key' => (int) $obj['key'],
                'value' => (string) $obj['value']
            );
        }, $array);

        return $res;
    }

}