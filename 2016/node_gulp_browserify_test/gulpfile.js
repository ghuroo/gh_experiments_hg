// express
var EXPRESS_PORT = 4000;
var EXPRESS_IP = '0.0.0.0';
var EXPRESS_ROOT = './dist';
// livereload
var LIVERELOAD_PORT = 4002;
// sass
var SASS_SOURCE = './src/sass/main.scss';
var SASS_DESTINATION = './dist/css/';
// sourcemaps
var SOURCEMAPS_DESTINATION = '/';
// autoprefixer
var AUTOPREFIXER_SOURCE = './src/css/main.css';
var AUTOPREFIXER_DESTINATION = './dist/css/';

// new
var gulp = require('gulp');

gulp.task('express', function() {
	var express = require('express');
	var app = express();

	app.use(express.static(EXPRESS_ROOT));
	app.listen(EXPRESS_PORT, EXPRESS_IP);
});

gulp.task('sass', function () {
	var sass = require('gulp-sass');
	var sourcemaps = require('gulp-sourcemaps');
	var autoprefixer = require('gulp-autoprefixer');

	return gulp
	.src(SASS_SOURCE)
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compact'}))
    .pipe(sourcemaps.write())
    .pipe(autoprefixer({browsers: ['last 2 versions']}))
    .pipe(gulp.dest(SASS_DESTINATION));
});

var BROWSERIFY_SOURCE = './src/app/app.js';
var BROWSERIFY_DESTINATION = './dist/js/';

gulp.task('browserify', function() {
	var browserify = require('browserify');
	var source = require('vinyl-source-stream');

	return browserify(BROWSERIFY_SOURCE)
	// bundles it and creates a file called main.js
	.bundle()
	.pipe(source('main.js'))
	// saves it the public/js/ directory
	.pipe(gulp.dest(BROWSERIFY_DESTINATION));
})

// copy-html
var COPY_HTML_INPUT = './src/**/*.html';
var COPY_HTML_OUTPUT = './dist/';
// copy-img
var COPY_IMG_INPUT = './src/img/**';
var COPY_IMG_OUTPUT = './dist/img/';
// copy-favicon
var COPY_FAVICON_INPUT = './src/img/favicon/favicon.ico';
var COPY_FAVICON_OUTPUT = './dist/';

gulp.task('copy-html', function() {
    return gulp.src(COPY_HTML_INPUT).pipe(gulp.dest(COPY_HTML_OUTPUT));
});

gulp.task('copy-img', function() {
    return gulp.src(COPY_IMG_INPUT).pipe(gulp.dest(COPY_IMG_OUTPUT));
});

gulp.task('copy-favicon', function() {
    return gulp.src(COPY_FAVICON_INPUT).pipe(gulp.dest(COPY_FAVICON_OUTPUT));
});

gulp.task('copy', ['copy-html', 'copy-img', 'copy-favicon']);

var WATCH_SASS = './src/sass/**/*.scss';
var WATCH_HTML = './src/**/*.html';
var WATCH_BROWSERIFY = './src/app/**/*.js';

gulp.task('watch', function () {
	gulp.watch(WATCH_SASS, ['sass']);
	gulp.watch(WATCH_HTML, ['copy-html']);
	gulp.watch(WATCH_BROWSERIFY, ['browserify'])
});

// default task
gulp.task('default', ['copy', 'sass', 'express', 'browserify', 'watch'], function() {

});