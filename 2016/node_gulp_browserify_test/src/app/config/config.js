module.exports = function($stateProvider, $urlRouterProvider) {

	$stateProvider

	.state('home', {
		url: '/home',
		templateUrl: 'app/modules/home/view.html'
	});

	var home = '/home';

	$urlRouterProvider.when('', home);
	$urlRouterProvider.when('/', home);
	$urlRouterProvider.otherwise(home);

}