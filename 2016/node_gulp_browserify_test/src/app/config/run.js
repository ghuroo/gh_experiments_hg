module.exports = function($rootScope, $state, CONSTANTS) {
    $rootScope.app = {};
    $rootScope.app.CONSTANTS = CONSTANTS;

    /* on stateChangeStart */
    var stateChangeStart = function (event, toState, toParams, fromState, fromParams) {};

    $rootScope.$on('$stateChangeStart', stateChangeStart);

    /* on stateChangeError */
    var stateChangeError = function (event, toState, toParams, fromState, fromParams, error) {
        console.log(error);
    };

    $rootScope.$on('$stateChangeError', stateChangeError);

    /* on stateChangeSuccess */
    var stateChangeSuccess = function (event, toState, toParams, fromState, fromParams) {};

    $rootScope.$on('$stateChangeSuccess', stateChangeSuccess);
}