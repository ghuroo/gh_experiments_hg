require('angular')
require('angular-ui-router')

angular.module('app', ['ui.router']);
var app = angular.module('app')

var config = require('./config/config')
app.config(config)

var constants = require('./config/constants');
app.constant('CONSTANTS', constants);

var run = require('./config/run')
app.run(run);

var home = require('./modules/home/controller')
app.controller('home', home)