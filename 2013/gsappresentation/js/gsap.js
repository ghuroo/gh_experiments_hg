/* After Page Load */
$(window).load(function() {

  Articles.popup(); //Define & start our animation

});

/* Articles Animation */
var Articles = {
  animation: new TimelineMax({repeat:0, yoyo:false, delay: 0.5, onUpdate:function(){Controls.updateSlider();}}),

  popup: function() {
    ArrayConstructor.filter($('ul li div')); //Create an array with the desired node list

    this.animation.staggerFrom(ArrayConstructor.array, 0.5, {opacity:0, scale: 0.8}, 0.1) //Run first animation, total speed 0.5, delay between each element 0.1
    .staggerFrom(ArrayConstructor.array, 0.5, {borderRadius: '50px'}, 0.1, "0.2") //Run second animation, total speed 0.5, start time 0.5
    .staggerTo(ArrayConstructor.array, 0.5, {rotation: -160, scale: 0.5}, 0.1, "1")
    .staggerTo(ArrayConstructor.array, 0.5, {marginTop: 50}, 0.1)
    .staggerTo(ArrayConstructor.array, 0.5, {rotation: 0}, 0.1);
  }
};

/* Controls */
var Controls = {
  timeline: Articles.animation,

  play: function() { this.timeline.play(); },
  pause: function() { this.timeline.pause(); },
  stop: function() { this.timeline.seek(0); this.timeline.pause(); },
  reverse: function() { this.timeline.reverse(); },
  slowMotion: function() { this.timeline.timeScale(0.2); },
  normalSpeed: function() { this.timeline.timeScale(1); },
  fastSpeed: function() { this.timeline.timeScale(4); },
  updateSlider: function() { $(".slider").slider("value", this.timeline.progress() *100); $('footer h1').html(Articles.animation.progress().toFixed(1)); }
};

$('.play').on('click', function() { Controls.play(); });
$('.pause').on('click', function() { Controls.pause(); });
$('.stop').on('click', function() { Controls.stop(); $(".slider").slider("value",0); $('footer h1').html('0'); });
$('.reverse').on('click', function() { Controls.reverse();  });
$('.slow-motion').on('click', function() { Controls.slowMotion(); });
$('.normal-speed').on('click', function() { Controls.normalSpeed(); });
$('.fast-speed').on('click', function() { Controls.fastSpeed(); });

$('.slider').slider({
  range: false,
  min: 0,
  max: 100,
  step:.1,
  slide: function ( event, ui ) {
    Articles.animation.pause();
    //adjust the timeline's progress() based on slider value
    Articles.animation.progress( ui.value/100 );
  }
});

/* Array Constructor */
var ArrayConstructor = {
  array: [], //Create array for future use

  filter: function(element) {
    this.array.length = 0; //Clear array

    var i=0; //Start counter
    for (var i=0; i<element.length; i++) {
      this.array.push(element.eq(i)); //Add each element to the array
    }
  }
};