<!DOCTYPE html>
<html>
<head>

<!-- Meta Tags -->
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta http-equiv="content-language" content="pt"/>
<meta name="author" content="Author">
<meta http-equiv="desription" content="website description"/>
<meta http-equiv="keyword" content="related keywords"/>

<!-- OpenGraph Meta Data -->
<meta property="og:title" content="Title">
<meta property="og:description" content="Description">
<meta property="og:type" content="website">
<meta property="og:url" content="url">
<meta property="og:image" content="img/share.jpg">

<!-- Title-->
<title>Test</title>

<!-- Favicon -->
<link rel="icon" type="image/x-icon" href="img/favicon.ico">

<!-- Cross-Browser -->
<link rel="stylesheet" href="styles/css/normalize.css">
<script type="text/javascript" src="js/plugins/modernizr.min.js"></script>

<!-- Styles -->
<link rel="stylesheet" href="styles/css/unsemantic-grid-responsive-tablet.css">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="styles/css/gsap.css">

</head>
<body>

<!-- Unsemantic -->
<div class="grid-container">

<!--Body -->
<div class="grid-100 grid-parent">

<header>greensock's gsap.js</header>

<ul class="grid-100">
  <li class="grid-25"><div></div></li>
  <li class="grid-25"><div></div></li>
  <li class="grid-25"><div></div></li>
  <li class="grid-25"><div></div></li>
  <li class="grid-25"><div></div></li>
  <li class="grid-25"><div></div></li>
  <li class="grid-25"><div></div></li>
  <li class="grid-25"><div></div></li>
</ul>

<footer>
  <button class="play">Play</button>
  <button class="pause">Pause</button>
  <button class="stop">Stop</button>
  <button class="reverse">Reverse</button>
  <button class="slow-motion">Slow Motion</button>
  <button class="normal-speed">Normal Speed</button>
  <button class="fast-speed">Fast Speed</button>
  <br/>
  <div class="slider"></div>
  <h1></h1>
</footer>

</div>

</div>

<!-- Plug-ins-->
<script type="text/javascript" src="js/plugins/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/plugins/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/plugins/TweenMax.min.js"></script>

<!-- Scripts -->
<script type="text/javascript" src="js/min/gsap.min.js"></script>

</body>
</html>