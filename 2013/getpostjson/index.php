<!DOCTYPE html>
<html>
<head>

<!-- Title-->
<title>Get & Post AJAX</title>

</head>
<body>


<!-- Header -->
<header>
  <h1>Get & Post AJAX</h1>
</header>

<!-- Container -->
<div id="container">

	<section id="main">
		<button>get</button>
	</section>

</div>


<!-- Plug-ins-->
<script type="text/javascript" src="js/plugins/jquery-1.10.2.min.js"></script>

<!-- Scripts -->
<script type="text/javascript" src="js/getpostajax.js"></script>

</body>
</html>