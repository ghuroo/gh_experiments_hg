/* JavaScript Document */

var numPoints = 50;
var dotsPerSeg = 10;
var pointsList = [[500, 50], [300, 150], [500, 250], [300, 350], [300, 600], [500, 600], [800, 400], [800, 200], [800, 200]];
var dot = $.crSpline.buildSequence(pointsList);
var h = 0;

// Criar pontos ao longo do caminho
for (i = 0; i < numPoints; i++) { pointsList.push([])}
for (i = 0; i < numPoints; i++) {
    for (var j = 0; j < dotsPerSeg; j++) {
        var t = (i + j / dotsPerSeg) / pointsList.length;
        var pos = dot.getPos(t);      
        $('<div class="ponto">.</div>').appendTo($(document.body)).css({left: pos.left, top: pos.top});
    }
}

$('.ponto:eq(0)').append('<div id="aqui"></div>')
//function countPontos(h) {
//    var item = $('.ponto')[h];
//    var x = item.offsetLeft;
//    var y = item.offsetTop;
//    $('#barco').css({'top': y, 'left': x});
//}
//function start() {
//h = setInterval(function() {
//    countPontos(h);
//    h++;
//}, 10);
//}

//Mover o barco
$(document).ready(function(e) {
    $('#barco').on("mousedown", function() {
       $('.ponto').on("mouseover", function() {
            var x = this.offsetLeft;
            var y = this.offsetTop;
            $('#barco').css({'top': y, 'left': x});
        });
    });
    $('#barco').on("mouseup", function() {
        $('.ponto').off("mouseover");
    });
});

//$(document).on("mousemove",function(e) {
//    $.each($(".ponto"),function(i,item) {
//        var x = e.pageX - item.offsetLeft;
//        var y = e.pageY - item.offsetTop;
//        $('#texto').html(x + ', ' + y);
//    console.log(x + ', ' + y);
//    });
//});



