/* Refresh Sizes on Resize & MoveTo */
$(window).resize(function() {
  fgNavigation.refresh();
  fgNavigation.moveTo(fgNavigation.currentArea);
});

$(window).load(function() {

/* Event Triggers */
$('header nav a').on('click',function(event) {
  event.preventDefault()
  fgNavigation.moveTo($(this));
  fgNavigation.currentArea = $(this);
});

});

/* Main Scripts */
fgNavigation = {
  scrollDiv: null,
  currentArea: null,
  parentHeight: null,
  parentWidth: null,

  setScroller: function(scrollDiv) {
    this.scrollDiv = scrollDiv;
    this.parentHeight = parseInt(scrollDiv.css('height').replace('px',''));
    this.parentWidth = parseInt(scrollDiv.css('width').replace('px',''));
  },

  moveTo: function(element) {
    var destination = $(element).attr('href');
    var top = $(destination).position().top / this.parentHeight * 100;
    var left = $(destination).parent().position().left / this.parentWidth * 100;

    TweenLite.to(this.scrollDiv, 0.5, {top:-top+'%', left:-left+'%'});
  },

  refresh: function() {
    this.parentHeight = parseInt(this.scrollDiv.css('height').replace('px',''));
    this.parentWidth = parseInt(this.scrollDiv.css('width').replace('px',''));
  }

}