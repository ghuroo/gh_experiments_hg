<!DOCTYPE html>
<html>
<head>

<!-- Meta Tags -->
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta http-equiv="content-language" content="pt"/>
<meta name="author" content="Author">
<meta http-equiv="desription" content="website description"/>
<meta http-equiv="keyword" content="related keywords"/>

<!-- OpenGraph Meta Data -->
<meta property="og:title" content="Title">
<meta property="og:description" content="Description">
<meta property="og:type" content="website">
<meta property="og:url" content="url">
<meta property="og:image" content="img/share.jpg">

<!-- Title-->
<title>Test</title>

<!-- Favicon -->
<link rel="icon" type="image/x-icon" href="img/favicon.ico">

<!-- Styles -->
<link rel="stylesheet" href="styles/css/unsemantic-grid-responsive-tablet.css">
<link rel="stylesheet" href="styles/css/onepage-scroll.css">
<link rel="stylesheet" href="styles/css/default.css">

<!-- Cross-Browser -->
<link rel="stylesheet" href="styles/css/normalize.css">
<script type="text/javascript" src="js/plugins/modernizr.min.js"></script>

</head>
<body>

<!-- Unsemantic -->
<div class="grid-container">

<!--Body -->

<div class="main grid-100 grid-parent">
  <section><h1>asd1</h1></section>
  <section><h1>asd2</h1></section>
  <section><h1>asd3</h1></section>
</div>

</div>

<!-- Plug-ins-->
<script type="text/javascript" src="js/plugins/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/plugins/TweenMax.min.js"></script>
<script type="text/javascript" src="js/plugins/jquery.onepage-scroll.min.js"></script>

<!-- Scripts -->
<script type="text/javascript" src="js/min/default.min.js"></script>

</body>
</html>