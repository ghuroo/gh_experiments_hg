<section class="title">
	<h4>Adicionar <?=$single?></h4>
</section>

<section class="item">
<div class="content">

<?php echo form_open_multipart() ?>

<div class="form_inputs">

	<ul>
    	<?php
			$i = 0;
			foreach($module_fields as $f){
				echo rdform_create($f,NULL,$i);
				if(!$f['_hidden']) $i++;
			}
		?>
	</ul>

</div>

<div class="buttons">
	<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))) ?>
</div>
<?php echo form_close() ?>

</div>
</section>