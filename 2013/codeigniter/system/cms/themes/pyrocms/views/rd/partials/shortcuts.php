<?php if(!empty($template['actions'])){ ?>
<nav id="shortcuts">
	<ul>
    	<?php if(isset($template['actions']['add'])){ ?>
        	<li><a href="<?=site_url($template['actions']['add']['uri'])?>/create" class="add">Adicionar <?=$template['actions']['add']['single']?></a></li>
		<?php } ?>
    	<?php if(isset($template['actions']['back'])){ ?>
        	<li><a href="<?=site_url($template['actions']['back']['uri'])?>/" class="back">Voltar</a></li>
		<?php } ?>
	</ul>
</nav>
<?php } ?>