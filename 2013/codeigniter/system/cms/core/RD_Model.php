<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author      Actualsales
 * @package     PyroCMS
 * @subpackage  Press Module
 * @category    Modules
 * @license     Apache License v2.0
 */
class RD_Model extends MY_Model {


    protected $fields = array();
    protected $actions = array(); 
    protected $actions_common = array(); 
    protected $actions_bottom = array(); 
    protected $_table_alias;
    protected $_module_name;
    protected $_where_opts = array( 'c' => 'create',
                                    'r' => 'read',
                                    'u' => 'update',
                                    'h' => 'hidden',
                                    'e' => 'export',
                                    'f' => 'filter' );
        
    public function __construct($module_name, $table_name = null)
    {
        parent::__construct();
        
        $this->_module_name = $module_name;
        $this->_table = $table_name;
        $this->_table_alias = $table_name;
    }
    
    
    /**********
    * Actions *
    **********/
    
    /*
     * Create a new entry of this module
     */
    function create($data=null){
        if(is_null($data)){
            $data = $this->get_Input_Data('c');
        }
        
        if(!$data){
            return array(false, $data);
        }
        
        return array($this->insert($data), null);
    }
    
    /*****************
    * Module helpers *
    *****************/
    
    public function get_all($limit = 0, $offset = 0, $filters = FALSE){
        $this->set_filters($filters);
        if($limit>0){
            $this->db->limit($limit, $offset);
        }
       return $this->db->get($this->_table)->result();
   }
    
    public function query_all($where = NULL, $filters = FALSE, $translate = true){
        $this->set_filters($filters);
        $fields = $this->get_fields($where);
        
        $select_query = array();
        
        if($translate){
            $select = array();
            foreach($fields as $name => $f){
                $select[] = "{$name} as {$f['label']}";
            }
            $select_query = implode(", ", $select);
        }
        
        return $this->db
            ->select($select_query)
            ->get($this->_table);
    }
    
    public function count_all($filters=FALSE){
        $this->set_filters($filters);
        return $this->db->count_all_results($this->_table);
    }
    
    
    /*
     * Set search filters for the next DB query
     */
    protected function set_filters($filters){
        if(!$filters){return;}
        
        $filters_main = $this->get_filters();
        
        foreach($filters as $f){
            $how = form_how_translate($f->how); 
            $filter_main = $filters_main[$f->ftype];
            $type = $filter_main['type'];
            
            foreach($f->values as $k => $v){
                
                if($type == 'file' || $type == 'image'){
                    if($v == '0'){
                        $how = 'is';
                    }else{
                        $how = 'is not';
                    }
                    $v = 'null';
                }else if($f->how == 'like' || $f->how == 'notlike'){
                    $v = "'%{$v}%'";
                }else{
                    $v = filter_value_parse($v, $type);
                }
                
                
                if(isset($joiner) && $joiner == 'or'){
                    $this->db->or_where($this->_table_alias.'.'.$k.' '.$how.' ', $v, false);
                }else{
                    $this->db->where($this->_table_alias.'.'.$k.' '.$how.' ', $v, false);
                }
                
                
            }
            $joiner = $f->joiner;
        }
    }
    
    
    /*
     * Get all registerd fields
     */
    public function get_fields($where=null){
        if(!is_null($where) && isset($this->_where_opts[$where])){
            $filter = '_'.$this->_where_opts[$where];
        }else{
            return $this->fields;
        }
        
        $fields = array();
        foreach($this->fields as $field){
            if($field[$filter]){
                switch($field['type']){
                    case 'image': 
                        if(isset($_FILES[$field['field']]) && $where == 'c'){
                            $_POST[$field['field']] = $_FILES[$field['field']]['name'];
                        } 
                        break;
                    case 'textarea':
                    case 'html':
                    case 'htmlAdv':
                        if($where=='f'){
                            $field['type'] = 'text';
                        }
                        break;
                }
                $field['form_type'] = $where;
                $fields[$field['field']] = $field;
            }
        }
        if($where == 'r'){
            $order = array();
            foreach ($fields as $key => $row) {
                $order[$key]  = $row['order'];
            }
            array_multisort($order, SORT_ASC, SORT_NUMERIC, $fields);
        }
        return $fields;
        
    }
    
    /*
     * Get validation rules for the specified location
     */
    function get_Validation_Rules($where=null){
        $rules = array();
        $fields = $this->get_fields($where);
        
        foreach($fields as $f){
            $rules[] = array(
                    'rules' => $f['rules'],
                    'field' => $f['field'],
                    'label' => $f['label']
                    );
        }
        
        return $rules;
    }
    
    /*
     * Register a new field in$field module
     */
    protected function add_field($f){
        if(!isset($f['field'])){
            throw new Exception('RD_Model: The FIELD key is required.');
        }
        
        
        // default values 
        $params_force = array(
                'type' => NULL, //field type
                'default' => NULL, //default value used in create/edit
                'rules' => '',
                'noe' => false, //if empty write null to the database
                'where' => 'crufe',
                'notwhere' => '',
                'order' => 100 + count($this->fields)
                );
        
        // set values for this field
        foreach($params_force as $k => $v){
            $f[$k] = isset($f[$k])?$f[$k]:$v;
        }
        $f['label'] = isset($f['label']) ? $f['label'] : ucwords(str_replace('_',' ',$f['field']));
            
        foreach($this->_where_opts as $k => $v){
            if(strpos($f['notwhere'], $k) !== false){
                $f["_{$v}"] = false;
            }elseif(strpos($f['where'], $k) !== false){
                $f["_{$v}"] = true;
            }else{
                $f["_{$v}"] = false;
            }
        }

        //dependecy defaults
        //if(isset($f['parent'])){
        //$f['parent']['field'] =
        //  (isset($f['parent']['field'])?
        //  $f['parent']['field']:
        //  $f['parent']['table'] . "_id");
        //}
        
        
        //file validation translate
        if($f['type'] == 'file'){
            $rules_translate = array(
                'required' => 'callback__required_file['.$f['field'].']'
            );
            $rules_aux = explode('|',$f['rules']);
            $rules_new = array();
            foreach($rules_aux as $r){
                $rules_new[] = isset($rules_translate[$r])?$rules_translate[$r]:$r;
            }
            $f['rules'] = implode('|',$rules_new);
        }
        
        $this->fields[$f['field']] = $f;
        
        
        //$order = array();
        //foreach ($this->fields as $key => $row) {
        //    $order[$key]  = $row['order'];
        //}
        //array_multisort($order, SORT_ASC, SORT_NUMERIC, $this->fields);
        
        
        return $f;
    }
    
    /*
     * Register an action (own column)
     */
    protected function add_action($oLabel, $oType, $oValue, $oExtra=null){
        $oAction = array(
                'label'=>$oLabel,
                'type'=>$oType,
                'value'=>$oValue,
                'display'=>$oLabel
            );
        
        if(!is_null($oExtra)){
            foreach($oExtra as $key=>$v){
                $oAction[$key] = $v;
            }
        }
        
        $this->actions[$oLabel] = $oAction;
        
        return $oAction;
    }
    
    /*
     * Register an action (actions column)
     */
    protected function add_action_common($oLabel, $oType, $oValue, $oExtra=null){
        $oAction = array(
                'label'=>$oLabel,
                'type'=>$oType,
                'value'=>$oValue,
                'display'=>$oLabel
            );
        
        if(!is_null($oExtra)){
            foreach($oExtra as $key=>$v){
                $oAction[$key] = $v;
            }
        }
        
        $this->actions_common[$oLabel] = $oAction;
        
        return $oAction;
    }
    
    /*
     * Register an action (actions column)
     */
    protected function add_action_bottom($oLabel, $oType, $oValue, $oExtra=null){
        $oAction = array(
                'label'=>$oLabel,
                'type'=>$oType,
                'value'=>$oValue,
                'display'=>$oLabel
            );
        
        if(!is_null($oExtra)){
            foreach($oExtra as $key=>$v){
                $oAction[$key] = $v;
            }
        }
        
        $this->actions_bottom[$oLabel] = $oAction;
        
        return $oAction;
    }
    
    /*
     * Get all the registered fields posted values for the given location
     */
    function get_Input_Data($where){
        $fields = $this->get_fields($where);
        $data = array();

        foreach($fields as $f){
            if($f['type'] == 'file' || $f['type'] == 'image'){
                if(isset($_FILES[$f['field']]) && empty($_FILES[$f['field']]['name'])){
                    continue;
                }else if($upload_data = $this->upload_file($f)){
                        $data[$f['field']] = $upload_data['file_name'];
                }else{
                    continue;
                }
            }else{
                if($f['type'] == 'info'){
                    continue;
                }
                $val = $this->input->post($f['field']);
                if(empty($val) && $f['noe']){
                    $val  = null;
                }
                $data[$f['field']] = $val;
            }
        }
        return $data;
    }
    
    /*
     * Extension of the original dropdown
     * Now its possible to choose a different table and sorting methods
     */
    function dropdown(){
        $args = func_get_args();
        $table = $this->_table;
        $first_empty = false;
        $where = '1=1';
        
        if(count($args) == 3){
            list($key, $value, $extra) = $args;
            
            if(!is_array($extra)){
                $query = $extra;    
            }else{
                if(isset($extra['table'])){$table=$extra['table'];}
                if(isset($extra['first_empty'])){$first_empty=$extra['first_empty'];}
                if(isset($extra['sort_by'])){$sort_by=$extra['sort_by'];}
                if(isset($extra['where'])){$where=$extra['where'];}
            }
        }elseif(count($args) == 2){
            list($key, $value) = $args;
        }else{
            $key = $this->primary_key;
            $value = $args[0];
        }
        if(!isset($sort_by)){$sort_by = $key;}

        if(isset($query)){
            $res = $this->db->query($query);
        }else{
            $res = $this->db->select(array($key, $value))
                ->where($where, null, false)
                ->order_by($sort_by)
                ->get($table);
        }
        
        $options = array();
        if($first_empty){$options['']=$first_empty;}
        
        foreach ($res->result() as $row){
            $options[$row->{$key}] = $row->{$value};
        }

        return $options;
    }

    
    
    /* File manipulation */
    
    /**
     * Upload a file to the server and add it to the DB
     *
     * @author Actualsales
     * @access public
     * @param array $input The data sent by the form
     * @return bool
     */
    protected function upload_file($field){
        $this->load->library('upload');
        //$field = $this->get_field($field_name);
        $field_name = $field['field'];
        if(is_null($field) 
                || ($field['type'] != 'file' && $field['type'] != 'image')
                || !isset($field['path'])
            ){
            throw new Exception("RD_Model: The field '{$field_name}' is an invalid file type.");
        }
        
        //check if file was sent
        if(!isset($_FILES[$field_name])
                || empty($_FILES[$field_name]['name'])
            ){
            $this->form_validation->set_message('required_file', "O campo %1\$s é obrigatório.");
            RETURN FALSE;
        }
        
        $path = BASEPATH.'../../'.$field['path'];
        
        if(!is_dir($path)){
            mkdir($path, 0, true);
        }
        
        // First we need to upload the file to the server
        $upload_conf = array();
        $upload_conf['upload_path'] = $path;
        $upload_conf['encrypt_name'] = isset($field['encrypt_name']) ? $field['encrypt_name'] : TRUE;
        $upload_conf['allowed_types']   =  isset($field['allowed_types']) ? $field['allowed_types'] : 'txt|jpg|pdf|doc|png|gif|zip|rar';
        $this->upload->initialize($upload_conf);
        
        // Let's see if we can upload the file
        if ($this->upload->do_upload($field_name)){ 
            $uploaded_data  = $this->upload->data();
            return $uploaded_data;
        }
        return $upload_data['file_name'] = '';
    }
    
    
    
    /*
     * Exports
     */
     
    function export_csv($filter=false){
        $data = $this->query_all('e',$filter);
        
        $this->load->dbutil();
        $delimiter = ';';
        $newline = '\r\n';
        $res = $this->dbutil->csv_from_result($data, $delimiter, $newline);
        
        return $res;
    }
    
    function export_xls($filter=false){
        
        $data = $this->query_all('e',$filter, false)->result();
        $fields = $this->get_fields();
        
        $html = '<TABLE border="1">';
        $header = 0;
        $line_number = 0;
        foreach($data as $line){
            if($header == 0){
                $html .= '<TR>';
                foreach($line as $key=>$cell){
                    $html .= '<TH>';
                    if(isset($fields[$key]['label'])){
                        $html .= _e($fields[$key]['label']);
                    }else{
                        $html .= _e($key);
                    }
                    $html .= '</TH>';
                }
                $html .= '</TR>';
                $header = 1;
            }
            $html .= '<TR>';
            $line_number++;
            foreach($line as $key=>$cell){
            
                if($line_number%2 != 0){
                    $html .= '<TD valin="top" style="background:#eee;">';
                }else{
                    $html .= '<TD>';
                }
                
                if(isset($fields[$key]) && $fields[$key]['type'] == 'select'){
                    if(isset($fields[$key]['values'][$cell])){
                        $html .= _e($fields[$key]['values'][$cell]);
                    }else{
                        $html .= '-';
                    }
                }else{
                    $html .= _e($cell);
                }
                
                $html .= '</TD>';
            }
            $html .= '</TR>';
        }
        $html .= '</TABLE>';
        
        return $html;
    }
    
    
    
    /* Getters and Setters */
    
    function get_filters(){
        return $this->get_fields('f');
    }
    
    function get_primaryKey(){
        return $this->primary_key;
    }
    
    function set_primaryKey($key){
        $this->primary_key = $key;
    }
    
    function set_tableAlias($alias){
        $this->_table_alias = $alias;
    }
    
    function get_actions(){
        return $this->actions;
    }
    
    function get_actions_common(){
        return $this->actions_common;
    }
    function get_actions_bottom(){
        return $this->actions_bottom;
    }
    
    function get_field($name){
        if($this->fields[$name]){
            return $this->fields[$name];
        }else{
            return NULL;
        }
    }
    
    /*order*/
    function changeMainOrder($id, $col){
        $table = $this->_table;
        
        $url = '';
        if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != ''){
            $url = $_SERVER['HTTP_REFERER'];
        }
        $response = array('res'=>false, 'redirect' => $url);

        $this->db->where('id',$id);
        $obj = $this->db->get($table)->row();
        $neworder = (int)$_POST['neworder'];
        $olderorder = $obj->{$col};
        
        if($neworder <=0){
            echo json_encode($response);
            return true;
        }
        //up order
        if($olderorder>$neworder){
            $this->db->where($col.' >=',$neworder);
            $this->db->where($col.' <=',$olderorder);
            $this->db->order_by($col.' asc');
            $list = $this->db->get($table)->result();
            foreach($list as $item){
                if($item->id == $obj->id){
                    $item->{$col} = $neworder;
                }else{
                    $item->{$col} += 1;     
                }
                $this->db->where('id', $item->id);
                $res = $this->db->update($table, $item);
            }
        //down order    
        }else{
            $this->db->where($col.' >=',$olderorder);
            $this->db->where($col.' <=',$neworder);
            $this->db->order_by($col.' asc');
            $list = $this->db->get($table)->result();
            foreach($list as $item){
                if($item->id == $obj->id){
                    $item->{$col} = $neworder;
                }else{
                    $item->{$col} -= 1;     
                }
                $this->db->where('id', $item->id);
                $res = $this->db->update($table, $item);
            }
        }
        $this->session->set_flashdata('success', 'Ordenação realizada com sucesso.');

        $response['res'] = true;
        echo json_encode($response);
        return true;
    }
}

/*
 *
 *
 * Class para o módulo Media (R&D)
 *
 *
 **/
class RD_Media extends MY_Model
{
    public      static  $table,
                        $module_name,
                        $path = 'uploads/',
                        $allowed_file_ext = array(
                            'a' => array('mpga', 'mp2', 'mp3', 'ra', 'rv', 'wav'),
                            'v' => array('mpeg', 'mpg', 'mpe', 'mp4', 'flv', 'qt', 'mov', 'avi', 'movie'),
                            'd' => array('pdf', 'xls', 'ppt', 'pptx', 'txt', 'text', 'log', 'rtx', 'rtf', 'xml', 'xsl', 'doc', 'docx', 'xlsx', 'word', 'xl', 'csv', 'pages', 'numbers'),
                            'i' => array('bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'png', 'tiff', 'tif'),
                            'o' => array('psd', 'gtar', 'swf', 'tar', 'tgz', 'xhtml', 'zip', 'css', 'html', 'htm', 'shtml') 
                        );
    protected   static  $_cache_path,
                        $_ext,
                        $_type = '',
                        $_filename = null,
                        $_mimetype;


    public function __construct()
    {
        parent::__construct();
        self::$table = $this->db->dbprefix('_media');
    }

    /* vai buscar todos os ficheiros de um item */
    public function get_all_files($post)
    {
        $this->db->where('parent_id', $post['parent_id']);
        $this->db->where('parent_table', $post['parent_table']);
        $this->db->order_by('sort');
        $files = $this->db->get(self::$table)->result();
        return self::result(true, null, null, array('folder' => [], 'file' => $files, 'parent_id' => $post['parent_id']));
    }

    /* vai buscar item por id */
    public function get_file_by_id($id)
    {
        $this->db->where('id', $id);
        $file = $this->db->get(self::$table)->result();
        return $file[0];
    }

    /* update info do item */
    public function update_item($post)
    {
        $data = array();
        try{

            if(isset($post['alt_attribute'])) $data['alt_attribute'] = $post['alt_attribute'];
            if(isset($post['keywords'])) $data['keywords'] = $post['keywords'];
            if(isset($post['description'])) $data['description'] = $post['description'];

            $this->db->where('id', $post['id']);
            $this->db->update(self::$table, $data);

            return self::result(true, lang('files:description_saved'));
        
        }catch(Exception $e){
            return self::result(false, lang('files:description_saved'));
        }
    }

    /* faz o upload do item */
    public function upload($name = false, $field = 'userfile', $width = false, $height = false, $ratio = false, $allowed_types = false, $alt = NULL, $module_name = '', $parent_id = 1, $parent_table = '')
    {
        if(!$check_dir = self::check_dir(self::$path)){ return $check_dir; }
        if(!$check_cache_dir = self::check_dir(self::$_cache_path)){ return $check_cache_dir; }
        if(!$check_ext = self::_check_ext($field)){ return $check_ext; }
        // this keeps a long running upload from stalling the site
        session_write_close();

        $upload_config = array(
            'upload_path'   => self::$path,
            'file_name'     => self::$_filename
        );

        // If we don't have allowed types set, we'll set it to the
        // current file's type.
        $upload_config['allowed_types'] = ($allowed_types) ? $allowed_types : self::$_ext;

        ci()->load->library('upload', $upload_config);

        if (ci()->upload->do_upload($field))
        {
            $file = ci()->upload->data();

            $data = array(
                'parent_id'     => (int) $parent_id,
                'parent_table'  => $parent_table,
                'user_id'       => (int) ci()->current_user->id,
                'type'          => self::$_type,
                #'name'          => $file['orig_name'],
                'path'          => self::$path,#.$file['file_name'],
                'description'   => '',
                'alt_attribute' => $alt,
                'filename'      => $file['file_name'],
                'extension'     => $file['file_ext'],
                'mimetype'      => $file['file_type'],
                'filesize'      => $file['file_size'],
                'width'         => (int) $file['image_width'],
                'height'        => (int) $file['image_height'],
                'date_added'    => now()
            );

            // perhaps they want to resize it a bit as they upload
            if ($file['is_image'] and ($width or $height))
            {
                ci()->load->library('image_lib');

                $config['image_library']    = 'gd2';
                $config['source_image']     = self::$path.$data['filename'];
                $config['new_image']        = self::$path.$data['filename'];
                $config['maintain_ratio']   = (bool) $ratio;
                $config['width']            = $width ? $width : 0;
                $config['height']           = $height ? $height : 0;
                ci()->image_lib->initialize($config);
                ci()->image_lib->resize();

                $data['width'] = ci()->image_lib->width;
                $data['height'] = ci()->image_lib->height;                  
            }

            $this->db->insert(self::$table, $data);
            $id = $this->db->insert_id();
            
            /*if ($data['type'] !== 'i')
            {
                $data = array('path' => '{{ url:site }}'.$module_name.'/media/download/'.$id);
                $this->db->where('id', $id);
                $this->db->update(self::$table, $data); 
            }*/

            header("Connection: close");

            return self::result(true, lang('files:file_uploaded'), $data['filename'], $data);
            
        }
        else
        {
            $errors = ci()->upload->display_errors();

            header("Connection: close");

            return self::result(false, $errors);
        }
    }

    /* ordena os items */
    public function order($collection=null)
    {

        if ($collection)
        {
            $data = array();
            foreach ($collection as $type => $item)
            {
                $i = 0;

                foreach ($item as $id) 
                {
                    array_push($data, array('id' => $id, 'sort' => $i));
                    $i++;
                }
            }

            $this->db->update_batch(self::$table, $data, 'id');

            return self::result(false, lang('files:sort_saved'));
        }
        else 
        {
            return self::result(false, lang('files:save_failed'));
        }
    }

    /* elimina um ficheiro */
    public function delete_file($id = 0)
    {
        try{
            $this->db->where('id', $id);
            $file = $this->db->get(self::$table)->result();

            $this->db->where('id', $id);
            $this->db->delete(self::$table);

            if(file_exists($file[0]->path.$file[0]->filename)){ unlink($file[0]->path.$file[0]->filename); }

            return self::result(true, lang('files:item_deleted'), $file[0]->filename);
        }catch(Exception $e){
            return self::result(false, lang('files:item_not_found'), $id);
        }
    }

    /* padroniza o return */
    public static function result($status = true, $message = '', $args = false, $data = '')
    {
        return array('status'   => $status, 
                     'message'  => $args ? sprintf($message, $args) : $message, 
                     'data'     => $data
                     );
    }

    /* verifica a pasta */
    public static function check_dir($path=NULL)
    {
        if(empty($path)) $path = self::$path;

        if (is_dir($path) and is_really_writable($path))
        {
            return self::result(true);
        }
        elseif ( ! is_dir($path))
        {
            if ( ! @mkdir($path, 0777, true))
            {
                return self::result(false, lang('files:mkdir_error'), $path);
            }
            else
            {
                // create a catch all html file for safety
                $uph = fopen($path . 'index.html', 'w');
                fclose($uph);
            }
        }
        else
        {
            if ( ! chmod($path, 0777))
            {
                return self::result(false, lang('files:chmod_error'));
            }
        }
    }

    /* verifica a extensão do ficheiro */
    private static function _check_ext($field)
    {
        if ( ! empty($_FILES[$field]['name']))
        {
            $ext        = pathinfo($_FILES[$field]['name'], PATHINFO_EXTENSION);
            $allowed    = self::$allowed_file_ext;

            foreach ($allowed as $type => $ext_arr)
            {               
                if (in_array(strtolower($ext), $ext_arr))
                {
                    self::$_type        = $type;
                    self::$_ext         = implode('|', $ext_arr);
                    self::$_filename    = trim(url_title($_FILES[$field]['name'], 'dash', true), '-');

                    break;
                }
            }

            if ( ! self::$_ext)
            {
                return self::result(false, lang('files:invalid_extension'), $_FILES[$field]['name']);
            }
        }       
        elseif (ci()->method === 'upload')
        {
            return self::result(false, lang('files:upload_error'));
        }

        return self::result(true);
    }
}
