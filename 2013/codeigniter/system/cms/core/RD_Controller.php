<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Code here is run before admin controllers
class RD_Controller extends Admin_Controller
{
    //public $module_name;
    /**
     * Validation rules for creating a new client
     *
     * @var array
     * @access protected
     */
    protected   $validation_rules = array(),
                $_uri_n = 4;
    public  $numrows = 10,
            $_returnUrl = '',
            $title = '',
            $single = '',
            $media;
    
    public function __construct($module_name, $tabs, $media=array(), $title=NULL, $single=NULL)
    {
        parent::__construct();
        
        $this->data = new stdClass();
        $this->data->title = $title;
        $this->data->single = $single;
        
        // Load all the required classes
        $this->module_name = $module_name;
        
        if(!defined('MODULE_NAME')) define('MODULE_NAME', $module_name);
        
        $this->data->module_name = $this->module_name;
        $this->data->_module_title = is_null($title) ? $module_name : $this->title;
        $this->data->_module_title_single = is_null($single) ? $module_name : $this->single;
        
        $this->load->model($this->module_name.'_m');


        if(isset($media['hasmedia']) and $media['hasmedia']===true){
            $tabs['Media']='media'; 
            $i = $this->media = new RD_Media();
            if(!empty($media['path'])) $i::$path = $media['path'];
            $this->media->check_dir();
        } 

        $this->template
                ->append_css('rd/application.css')
                ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
                ->set_layout('default', 'rd')
                ->set('tabs', $tabs);

        $this->validation_rules = $this->{$this->module_name . '_m'}->get_Validation_Rules();
    }

    /**
     * List all existing client
     *
     * @author Actualsales
     * @access public
     * @return void
     */
    public function index()
    {   
        //no access
        $this->lista();
    }
    
    /**
     * List all existing client
     *
     * @author Actualsales
     * @access public
     * @return void
     */
    public function lista($btnAdd=true)
    {
        $params = $this->uri->uri_to_assoc( $this->_uri_n );
        
        if($this->session->userdata('return_url'))
        {
            $this->session->unset_userdata('return_url');
        }
        
        $filter_key = isset($params['filter']) ? $params['filter'] : NULL;
        $offset = isset($params['offset']) ? $params['offset'] : 0;
        $offset = ((int)$offset > 0 ? $offset : 0);
        $pag_index = isset($params['offset']) ? $this->uri->total_segments() : $this->uri->total_segments()+1;

        $this->load->helper('pagination');
        
        $m = $this->{$this->module_name.'_m'};
        
        $filter = $this->_get_filter($filter_key);
        $filter_uri = $filter ? "/filter/{$filter_key}" : '';
        
        $res = $m->get_all($this->numrows, $offset, $filter);
        
        $count = $m->count_all($filter);
        
        #$this->data->nrecords = $count;
        
        if($count){
            //on delete fix
            if($count<=($offset)){
                $offset -= $this->numrows;
                redirect('admin/'.$this->module_name.'/lista'.$filter_uri.'/offset/'.$offset);
            }
            
            $this->data->pagination = create_pagination('admin/'.$this->module_name.'/lista'.$filter_uri.'/offset/', $count, $this->numrows,$pag_index);
            $this->data->list =& $res;
        }
        
        $this->session->set_userdata('offset', $offset);
        $this->data->offset = $offset;
        
        $this->data->module_fields = $m->get_fields('r');
        $this->data->module_filters = $m->get_filters();
        $this->data->primaryCol = $m->get_primaryKey();
        $this->data->module_actions = $this->{$this->module_name.'_m'}->get_actions();
        $this->data->module_actions_common = $this->{$this->module_name.'_m'}->get_actions_common();
        $this->data->module_actions_bottom = $this->{$this->module_name.'_m'}->get_actions_bottom();
        
        
        $this->data->active_filters = $filter===FALSE ? array() : $filter; 
        
        $this->data->filter_key = $filter_key;
        
        
        if($btnAdd) $this->template->add_action('add', $this->single);
        $this->template->build('rd/index', $this->data);
    }
    
    /*public function listatab()
    {
        $this->template->set_layout('tabs');
        $this->lista();
    }*/
    
    public function export($type='csv',$filter_key=null)
    {
        
        $m = $this->{$this->module_name.'_m'};
        $filter = $this->_get_filter($filter_key);
        
        switch($type){
            case "csv":
                $filename = $this->module_name.'_export.csv';
                header('Content-Type: application/csv');
                header('Content-Disposition: attachement; filename="' . $filename . '"');
                echo $m->export_csv($filter);
                exit();
                break;
            case "xls":
                $filename = $this->module_name.'_export.xls';
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachement; filename="' . $filename . '"');
                echo $m->export_xls($filter);
                exit();
                break;
            case "list":
                $filename = $this->module_name.'_export.xls';
                //header('Content-Type: application/vnd.ms-excel');
                //header('Content-Disposition: attachement; filename="' . $filename . '"');
                echo $m->export_xls($filter);
                exit();
                break;
        }
    }
    

    /**
     * Create new
     */
    public function create()
    {
        // Set the validation rules
        $this->form_validation->set_rules($this->validation_rules);
        
        //prep data
        $this->data->module_fields = $this->{$this->module_name . '_m'}->get_fields('c');
        
        if($this->form_validation->run()){
            //insert data
            list($res, $res_extra) = $this->{$this->module_name . '_m'}->create();
            if($res){
                //success message
                $message = 'Entrada inserida com sucesso.';
                $status = 'success';
                $this->session->set_flashdata($status, $message);
                if(empty($this->_returnUrl)){
                    if(isset($this->returnID)){
                        return $this->db->insert_id();
                    }else{
                        redirect('admin/'.$this->module_name);
                    }
                }else{
                    redirect('admin/'.$this->_returnUrl);
                }
                
            }
            else{
                $error = !is_null($res_extra) ? '<br/>'.$this->upload->display_errors() : '';
                
                //failure message
                $message= 'Erro ao tentar guardar.'. $error;
                $status = 'error';
                $this->session->set_flashdata($status, $message);
            }
            $this->template->build('rd/create', $this->data);
        }
        
        if($this->_is_ajax() && strtoupper($_SERVER['REQUEST_METHOD']) == 'POST'){
            $errors = validation_errors();
            echo json_encode(array('status' => 'error','message' => $errors ));
        }
        else{
            // Load the view
            $this->template
                    ->add_action('back', $this->single)
                    ->build('rd/create', $this->data);
        }
    }
    
    
    /**
     * Edit an existing item
     *
     * @author Actualsales
     * @param id the ID to edit
     * @access public
     * @return void
     */
    public function edit($id)
    {
        $module = new $this->{$this->module_name.'_m'}();
        $id_rule = array(   'field' => $module->get_PrimaryKey(),
                            'label' => 'Primary Col',
                            'rules' => 'required|is_numeric|trim' );
                            
        array_push($this->validation_rules, $id_rule);
        
        //get the product we want to edit
        $item = $module->get($id);
        $this->data->item =& $item;
        
        if($this->session->userdata('return_url')) $this->_returnUrl = $this->session->userdata('return_url');
        
        // Set the validation rules
        $this->form_validation->set_rules($this->validation_rules);
        
        //prep data
        $this->data->module_fields = $this->{$this->module_name . '_m'}->get_fields('u');
        
        if($this->form_validation->run()){
            //insert data
            $data = $module->get_Input_Data('u');
            
            if($module->update($this->input->post('id'), $data, TRUE)){
                //success message
                $message = 'Entrada editada com sucesso.';
                $status = 'success';
                $this->session->set_flashdata($status, $message);
                if(empty($this->_returnUrl)){
                    redirect('admin/'.$this->module_name);
                }else{
                    if($this->session->userdata('return_url')){
                        redirect($this->_returnUrl);
                    }else{
                        redirect('admin/'.$this->_returnUrl);
                    }
                }
                //redirect($this->_get_last_page());
            }
            else{
                //failure message
                $message= 'Erro ao tentar editar.';
                $status = 'error';
                $this->session->set_flashdata($status, $message);
                $this->template->build('rd/edit', $this->data);
            }
        }else{
            if(isset($_SERVER['HTTP_REFERER']) && !$this->session->userdata('return_url')){
                $this->session->set_userdata('return_url', $_SERVER['HTTP_REFERER']);
                $this->_returnUrl = $_SERVER['HTTP_REFERER'];
            }
        }
        
        if($this->_is_ajax() && strtoupper($_SERVER['REQUEST_METHOD']) == 'POST'){
            $errors = validation_errors();
            echo json_encode(array( 'status' => 'error', 'message' => $errors ));
        }
        else{
            // Load the view
            $this->template
                    ->add_action('back', $this->single)
                    ->set('tab', 'edit')
                    ->build('rd/base_edit', $this->data);
        }
    }  


    /**
     *
     *
     * MEDIA
     *
     *
     */
    public function media($id)
    {
        $module = new $this->{$this->module_name.'_m'}();
        $item = $module->get($id);
        $this->data->item = $item;

        $allowed_extensions = '';
        foreach (RD_Media::$allowed_file_ext as $type) { $allowed_extensions .= implode('|', $type).'|'; }

        $post_max = str_replace('M', '', ini_get('post_max_size'));
        $file_max = str_replace('M', '', ini_get('upload_max_filesize'));
        // set the largest size the server can handle and the largest the admin set
        $max_size_possible = ($file_max > $post_max ? $post_max : $file_max) * 1048576; // convert to bytes
        $max_size_allowed = Settings::get('files_upload_limit') * 1048576; // convert this to bytes also

        $this->template->append_metadata(
            "<script>
                pyro.lang.fetching = '".lang('files:fetching')."';
                pyro.lang.fetch_completed = '".lang('files:fetch_completed')."';
                pyro.lang.start = '".lang('files:start')."';
                pyro.lang.width = '".lang('files:width')."';
                pyro.lang.height = '".lang('files:height')."';
                pyro.lang.ratio = '".lang('files:ratio')."';
                pyro.lang.full_size = '".lang('files:full_size')."';
                pyro.lang.cancel = '".lang('buttons:cancel')."';
                pyro.lang.synchronization_started = '".lang('files:synchronization_started')."';
                pyro.lang.untitled_folder = '".lang('files:untitled_folder')."';
                pyro.lang.exceeds_server_setting = '".lang('files:exceeds_server_setting')."';
                pyro.lang.exceeds_allowed = '".lang('files:exceeds_allowed')."';
                pyro.files = { permissions : ['wysiwyg','upload','download_file','edit_file','delete_file','set_location'] };
                pyro.files.max_size_possible = '".$max_size_possible."';
                pyro.files.max_size_allowed = '".$max_size_allowed."';
                pyro.files.valid_extensions = '/".trim($allowed_extensions, '|')."$/i';
                pyro.lang.file_type_not_allowed = '".addslashes(lang('files:file_type_not_allowed'))."';
                pyro.lang.new_folder_name = '".addslashes(lang('files:new_folder_name'))."';
                pyro.lang.alt_attribute = '".addslashes(lang('files:alt_attribute'))."';

                // deprecated
                pyro.files.initial_folder_contents = 0;
            </script>");
        
        $this->template
            ->append_css('jquery/jquery.tagsinput.css')
            ->append_css('rd/media.css')
            ->append_js('jquery/jquery.tagsinput.js')
            ->append_js('rd/media_jquery.fileupload.js')
            ->append_js('rd/media_jquery.fileupload-ui.js')
            ->append_js('rd/media_functions.js');
        /* /files_m */

        $this->template
                    ->add_action('back', $this->single)
                    ->set('tab', 'media')
                    ->set('parent_id', $id)
                    ->set('parent_table', $this->db->dbprefix($module->tablename))
                    ->set('modulename', $module->modulename)
                    ->build('rd/base_edit', $this->data);
    }
    /* AJAX CALLS */
    /* Get Files */
    public function media_get_all_files()
    {
        echo json_encode($this->media->get_all_files($this->input->post()));
    }
    /* Upload Files */
    public function media_upload_file()
    {
        $input = $this->input->post();
        $result = null;

        $result = $this->media->upload($input['name'], 'file', $input['width'], $input['height'], $input['ratio'], null, $input['alt_attribute'], $input['module_name'], $input['parent_id'], $input['parent_table']);
        $result['status'] AND Events::trigger('file_uploaded', $result['data']);

        echo json_encode($result);
    }
    /* Delete Files */
    public function media_delete_file()
    {
        $result = $this->media->delete_file($this->input->post('file_id'));
        $result['status'] AND Events::trigger('file_deleted', 1);
        echo json_encode($result);
    }
    /* Download File */
    public function media_download_file($id = 0)
    {
        $this->load->helper('download');

        $file = $this->media->get_file_by_id($id);
        $data = file_get_contents($file->path . $file->filename);

        $name = (strpos($file->filename, $file->extension) !== false ? $file->filename : $file->filename . $file->extension);

        force_download($name , $data);
    }
    /* Show Thumbnail */
    public function media_thumb($id = 0, $width = 75, $height = 50, $mode = 'fit')
    {
        $file = $this->media->get_file_by_id($id);
        #dump($file);die;
        if ( ! $file)
        {
            set_status_header(404);
            exit;
        }

        $cache_dir = 'uploads/cache/';
        $this->media->check_dir($cache_dir);

        $modes = array('fill', 'fit');

        $args = func_num_args();
        $args = $args > 3 ? 3 : $args;
        $args = $args === 3 && in_array($height, $modes) ? 2 : $args;

        switch ($args)
        {
            case 2:
                if (in_array($width, $modes))
                {
                    $mode   = $width;
                    $width  = $height; // 100

                    continue;
                }
                elseif (in_array($height, $modes))
                {
                    $mode   = $height;
                    $height = empty($width) ? null : $width;
                }
                else
                {
                    $height = null;
                }

                if ( ! empty($width))
                {
                    if (($pos = strpos($width, 'x')) !== false)
                    {
                        if ($pos === 0)
                        {
                            $height = substr($width, 1);
                            $width  = null;
                        }
                        else
                        {
                            list($width, $height) = explode('x', $width);
                        }
                    }
                }
            case 2:
            case 3:
                if (in_array($height, $modes))
                {
                    $mode   = $height;
                    $height = empty($width) ? null : $width;
                }

                foreach (array('height' => 'width', 'width' => 'height') as $var1 => $var2)
                {
                    if (${$var1} === 0 or ${$var1} === '0')
                    {
                        ${$var1} = null;
                    }
                    elseif (empty(${$var1}) or ${$var1} === 'auto')
                    {
                        ${$var1} = (empty(${$var2}) OR ${$var2} === 'auto' OR ! is_null($mode)) ? null : 100000;
                    }
                }
                break;
        }

        // Path to image thumbnail
        $thumb_filename = $cache_dir . ($mode ? $mode : 'normal');
        $thumb_filename .= '_' . ($width === null ? 'a' : ($width > $file->width ? 'b' : $width));
        $thumb_filename .= '_' . ($height === null ? 'a' : ($height > $file->height ? 'b' : $height));
        $thumb_filename .= '_' . md5($file->filename) . $file->extension;

        $expire = 60 * Settings::get('files_cache');
        if ($expire)
        {
            header("Pragma: public");
            header("Cache-Control: public");
            header('Expires: ' . gmdate('D, d M Y H:i:s', time()+$expire) . ' GMT');
        }

        $source_modified = file_exists($file->path.$file->filename) ? filemtime($file->path.$file->filename) : 0 ;
        $thumb_modified = file_exists($thumb_filename) ? filemtime($thumb_filename) : 0 ;

        if ( ! file_exists($thumb_filename) or ($thumb_modified < $source_modified))
        {
            if ($mode === $modes[1])
            {
                $crop_width     = $width;
                $crop_height    = $height;

                $ratio      = $file->width / $file->height;
                $crop_ratio = (empty($crop_height) OR empty($crop_width)) ? 0 : $crop_width / $crop_height;
                
                if ($ratio >= $crop_ratio and $crop_height > 0)
                {
                    $width  = $ratio * $crop_height;
                    $height = $crop_height;
                }
                else
                {
                    $width  = $crop_width;
                    $height = $crop_width / $ratio;
                }

                $width  = ceil($width);
                $height = ceil($height);
            }

            if ($height or $width)
            {
                // LOAD LIBRARY
                $this->load->library('image_lib');

                // CONFIGURE IMAGE LIBRARY
                $config['image_library']    = 'gd2';
                $config['source_image']     = $file->path.$file->filename;
                $config['new_image']        = $thumb_filename;
                $config['maintain_ratio']   = is_null($mode);
                $config['height']           = $height;
                $config['width']            = $width;
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $this->image_lib->clear();

                if ($mode === $modes[1] && ($crop_width !== null && $crop_height !== null))
                {
                    $x_axis = floor(($width - $crop_width) / 2);
                    $y_axis = floor(($height - $crop_height) / 2);

                    // CONFIGURE IMAGE LIBRARY
                    $config['image_library']    = 'gd2';
                    $config['source_image']     = $thumb_filename;
                    $config['new_image']        = $thumb_filename;
                    $config['maintain_ratio']   = false;
                    $config['width']            = $crop_width;
                    $config['height']           = $crop_height;
                    $config['x_axis']           = $x_axis;
                    $config['y_axis']           = $y_axis;
                    $this->image_lib->initialize($config);
                    $this->image_lib->crop();
                    $this->image_lib->clear();
                }
            }

            else
            {
                $thumb_modified = $source_modified;
                $thumb_filename = $file->path.$file->filename;
            }
        }
        else if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) &&
            (strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $thumb_modified) && $expire )
        {
            // Send 304 back to browser if file has not been changed
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $time) . ' GMT', true, 304);
            exit;
        }

        header('Content-type: ' . $file->mimetype);
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($thumb_filename)) . ' GMT');
        if(ob_get_level()) ob_clean();
        readfile($thumb_filename);
    }
    /* Order Files */
    public function media_order()
    {
        echo json_encode($this->media->order($this->input->post('order')));
    }
    /* Save Info */
    public function media_save_info()
    {
        if ($id = $this->input->post('id'))
        {
            echo json_encode($this->media->update_item($this->input->post()));
        }
    }
    /*
     *
     *
     * / MEDIA
     *
     *
     **/


    /**
     * View an existing item
     *
     * @author Actualsales
     * @param id the ID to edit
     * @access public
     * @return void
     */
    /*public function view($id)
    {
        $module = new $this->{$this->module_name."_m"}();
        $id_rule = array(
                'field' => $module->get_PrimaryKey(),
                'label' => "Primary Col",
                'rules' => 'required|is_numeric|trim'
            );
        array_push($this->validation_rules, $id_rule);
        
        //get the product we want to edit
        $item = $module->get($id);
        $this->data->item =& $item;
        
        if($this->session->userdata('return_url')){
            $this->_returnUrl = $this->session->userdata('return_url');
        }
        
        // Set the validation rules
        $this->form_validation->set_rules($this->validation_rules);
        
        //prep data
        $this->data->module_fields = $this->{$this->module_name . "_m"}->get_fields("u");
        
        if($this->form_validation->run()){
            //insert data
            $data = $module->get_Input_Data("u");
            
            if($module->update($this->input->post('id'), $data, TRUE)){
                //success message
                $message = 'Entrada editada com sucesso.';
                $status = 'success';
                $this->session->set_flashdata($status, $message);
                if(empty($this->_returnUrl)){
                    redirect('admin/'.$this->module_name);
                }else{
                    if($this->session->userdata('return_url')){
                        redirect($this->_returnUrl);
                    }else{
                        redirect('admin/'.$this->_returnUrl);
                    }
                }
                //redirect($this->_get_last_page());
            }
            else{
                //failure message
                $message= 'Erro ao tentar editar.';
                $status = 'error';
                $this->session->set_flashdata($status, $message);
                $this->template->build('rd/view', $this->data);
            }
        }else{
            if(isset($_SERVER["HTTP_REFERER"]) && !$this->session->userdata('return_url')){
                $this->session->set_userdata('return_url', $_SERVER["HTTP_REFERER"]);
                $this->_returnUrl = $_SERVER["HTTP_REFERER"];
            }
        }
        
        if($this->_is_ajax() && strtoupper($_SERVER['REQUEST_METHOD']) == 'POST'){
            $errors = validation_errors();
            echo json_encode(array('status' => 'error','message' => $errors ));
        }
        else{
            // Load the view
            $this->template->build('rd/view', $this->data);
        }
    }*/
    


    /**
     * Delete an single or many items
     *
     * @author Actualsales
     * @access public
     * @return void
     */
    public function delete($id=null)
    {
        if(is_null($id)){
            $ids = $this->input->post('action_to');
        }else{
            $ids = array($id);
        }
        
        if(!empty($ids)){
            $i = 0;
            $count = count($ids);

            //loop through each id and try to delete
            foreach($ids as $id){
                //delete success
                if($this->{$this->module_name . '_m'}->delete($id)){
                    $i++;
                }
            }
            if($i>0){
                $this->session->set_flashdata('success', sprintf('%1$d de %2$d produtos foram removidos com sucesso.', $i, $count));
            }
        }
        else{
            //oops no ids.. ids required here.  GTFO.
            $this->session->set_flashdata('notice', 'Tentou fazer uma acção sem ter seleccionado um item.');
        }
        //no need to keep hanging around here,  redirect back to faq list
        redirect($this->_get_last_page());
    }
    
    public function action($action, $id)
    {
        $this->{$this->module_name . '_m'}->{$action}($id);
        if(isset($_SERVER['HTTP_referer']) && $_SERVER['HTTP_referer'] != ''){
            redirect($_SERVER['HTTP_referer']);
        }else{
            redirect($this->_get_last_page());
        }
    }
    
    public function filter()
    {
        $filter_json = $this->input->post('fq');
        $filter = json_decode($filter_json);
        $filter_key = sha1($filter_json);
        $_SESSION['filter_'.$filter_key] = $filter;
        redirect('admin/'.$this->module_name.'/lista/filter/'.$filter_key);
    }

    public function ajaxaction($action, $id)
    {
        $this->{$this->module_name . '_m'}->{$action}($id);
    }
    
    /* Aux functions */
    protected function _get_last_page()
    {
        if(false){
            //todo, session last url
        }elseif (isset($_SERVER['HTTP_REFERER'])){
            return $_SERVER['HTTP_REFERER'];
        }else{
            return 'admin/'.$this->module_name;
        }  
    }
    
    // get filters if any
    protected function _get_filter($filter_key)
    {
        $filter = FALSE;
        if(!is_null($filter_key)){
            if(isset($_SESSION['filter_'.$filter_key])){
                $filter = $_SESSION['filter_'.$filter_key];
            }
        }
        
        return $filter;
    }
    
    
    /*public function thumb()
    {
        $path = BASEPATH."../../";
        
        //dump($path);
        //dump($_GET);
        
        $_file_path = "";
        $_width = 100;
        $_height = 100;
        $_crop = 0;
        if(isset($_GET) && isset($_GET["path"])){
            $_file_path = $_GET["path"];
            $_width = $_GET["width"];
            $_height = $_GET["height"];
            $_crop = $_GET["crop"];
            $file = "";
            $thumbfile = "";
            if($_file_path != ""){
                $file = $path . $_file_path;
                $thumbfile = $path  ."../uploads/thumbs/".substr(basename($_file_path),0,-4)."_".$_width."x".$_height."-c".$_crop.substr(basename($_file_path),-4);
            }
            
            if(file_exists($file) && !file_exists($thumbfile)){
                $config['image_library'] = 'gd2';
                $config['source_image'] = $file;
                $config['create_thumb'] = TRUE;
                $config['maintain_ratio'] = TRUE;
                $config["quality"] = "80%";
                $config["new_image"] = $thumbfile;
                $config['width']     = ($_GET["width"]>0?$_GET["width"]:10);
                $config['height']   = ($_GET["height"]>0?$_GET["height"]:10);
                $config["dynamic_output"] = false;
                $config["thumb_marker"] = false;
                $this->load->library('image_lib', $config); 
                
                $res = $this->image_lib->resize();
            }
            
            if(file_exists($thumbfile)){
                //echo base_url()."../uploads/thumbs/".basename($thumbfile);
                header('Content-Type: image/jpeg');
                readfile($thumbfile);
            }
        }
        exit;
    }*/
    
    /*public function download_file()
    {
        
        $route = $this->uri->segments;
        
        $path = "";
        foreach($route as $key=>$value){
            if($key >= 4){
                $path .= "/". $value;
            }
        }
    
        $mainpath = BASEPATH.'../../../uploads';
        if(file_exists($mainpath . $path)){
            $this->load->helper(array('download'));
            
            $data = file_get_contents($mainpath.$path); // Read the file's contents
            $name = basename($path);
            force_download($name, $data);
            //echo $path;
            //download($path);
        }else{
            if(isset($_SERVER["HTTP_REFERER"])){
                $this->session->set_flashdata('error', 'Erro ao tentar aceder ao ficheiro');
                redirect($_SERVER["HTTP_REFERER"]);
            }
        }
        return false;
    }*/

    /*public function ajaxGetDependencies($field_name, $parent_val = null)
    {
        $res = array('field'=>$field_name,'values'=>array());
        $f = $this->{$this->module_name . "_m"}->get_field($field_name);
        if(!is_null($parent_val) && isset($f['parent'])){
            $table = $f['parent']['table'];
            $where = $f['parent']['parent_field'] . ' = "' . $parent_val . '"';
            $id_field = $f['parent']['id_field'];
            $name_field = $f['parent']['name_field'];
            $sort_by = (isset($f['parent']['sort_by']) ? $f['parent']['sort_by'] : $f['parent']['name_field'] );
            
            $values = $this->{$this->module_name . "_m"}->dropdown($id_field, $name_field, array("table"=>$table, "sort_by"=> $sort_by,"where"=>$where));
            
            $res['values'][] = array("key"=>'', "val"=>'Seleccione');
            foreach($values as $key=>$val){
                $res['values'][] = array("key"=>$key, "val"=>$val); 
            }
        }
        echo json_encode($res);
        die;
    }*/
    
    protected function _is_ajax()
    {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') ? TRUE : FALSE;
    }
}
