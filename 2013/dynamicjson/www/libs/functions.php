<?php
/*************** APP ***************/
function render($template, $data = array()){
    $app = \Slim\Slim::getInstance();

    $dataExtra = array(
        'current_page_url' => getCurrentUrl()
    );

    if(file_exists(VIEWSPATH.'/'.$template.'_'.LANG.'.twig')){
        $template = $template.'_'.LANG.'.twig';
    }else{
        $template = $template.'.twig';
    }

    $app->render($template, array_merge($dataExtra, $data));
}
function getCurrentUrl(){
    $pageURL = (@$_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
	$pageURL .= $_SERVER['SERVER_NAME'];
	
    if ($_SERVER['SERVER_PORT'] != '80')
		{ $pageURL .= ':'.$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI']; }
	else
		{ $pageURL .= $_SERVER['REQUEST_URI']; }
		
    return $pageURL;
}
function renderJSON($json){
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    print(is_array($json) ? json_encode($json) : $json);
    die;
}
/*************** XTRAS ***************/
function dump($dump){
	echo'<pre class="debug">';print_r($dump);echo'</pre>';
}
function permalink($str){
	$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
	$clean = preg_replace('/[^a-zA-Z0-9\/_| -]/', '', $clean);
	$clean = strtolower(trim($clean, '-'));
	$clean = preg_replace('/[\/_| -]+/', '-', $clean);
	
	return $clean;
}
function validateEmail($email){
	if(preg_match('/^[A-Z0-9._%-]+@[A-Z0-9][A-Z0-9.-]{0,61}[A-Z0-9]\.[A-Z]{2,6}$/i', $email) > 0)
		{ return true; }
	else
		{ return false; }
}
function randomString($length, $letters=true, $numbers=true){
	$chars = '';
	
	if($letters) $chars .= 'abcdefghijklmnopqrstuvwxyz';
	if($numbers) $chars .= '0123456789';
	
	return substr(str_shuffle($chars), 0, $length);
}
function validaURL($url){		
	return preg_match('/^(([\w]+:)?\/\/)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&amp;?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/', $url);
}
function truncate($str, $limit, $append='...'){
	if(strlen($str) > $limit) {
		$str = substr($str, 0, $limit);
		$str .= $append;
	}
	
	return $str;
}
function calculateAge($day, $month, $year){
	$age = date('Y') - $year;
	$months = date('m') - $month;
	$days = date('d') - $day;
	
	if($months<0 || $days<0) --$age;
	
	return $age;
}
function checkFileExtention($filename=NULL,$allowed=array()){
	$return = false;
	
	if(!empty($filename) && !empty($allowed)){
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if(in_array($ext,$allowed) ) $return = true;
	}
	
	return $return;
}
function num2Month($num, $lang='pt', $min=false){
	switch($num){
		case 1: 
			$lang == 'en' ?
			$mes = 'January': 
			$mes = 'Janeiro'; 
			break;
		case 2: 
			$lang == 'en' ?
			$mes = 'February': 
			$mes = 'Fevereiro'; 
			break;
		case 3: 
			$lang == 'en' ?
			$mes = 'March': 
			$mes = 'Março'; 
			break;
		case 4: 
			$lang == 'en' ?
			$mes = 'April': 
			$mes = 'Abril'; 
			break;
		case 5: 
			$lang == 'en' ?
			$mes = 'May': 
			$mes = 'Maio'; 
			break;
		case 6: 
			$lang == 'en' ?
			$mes = 'June': 
			$mes = 'Junho'; 
			break;
		case 7: 
			$lang == 'en' ?
			$mes = 'July': 
			$mes = 'Julho'; 
			break;
		case 8: 
			$lang == 'en' ?
			$mes = 'August': 
			$mes = 'Agosto'; 
			break;
		case 9: 
			$lang == 'en' ?
			$mes = 'September': 
			$mes = 'Setembro'; 
			break;
		case 10: 
			$lang == 'en' ?
			$mes = 'October': 
			$mes = 'Outubro'; 
			break;
		case 11: 
			$lang == 'en' ?
			$mes = 'November': 
			$mes = 'Novembro'; 
			break;
		default: 
			$lang == 'en' ?
			$mes = 'December': 
			$mes = 'Dezembro';
	}
	if($min==true) $mes = substr($mes, 0, 3);
	return $mes;
}