<?php

/* Show Object */
$app->get('/(:id(/))', function ($id = NULL) use($app){
    $data = array();

    if (!isset($id)) { $id = 1; }

    $data['object'] = R::getRow("SELECT * FROM objects WHERE id='".$id."' AND active='yes' ;");

    $content = json_decode($data['object']['content'], true);
    $data['object']['content'] = $content;

    render('home', $data);
});

/* Update */
$app->post('/update(/)', function () use($app){
    $data = array();
    $post = $app->request()->post();

    $obj = R::load("objects",$post['id']);
    $obj->content = $post['content'];
    $obj->updated_at = date("Y-m-d h:m:s",time());

    R::store($obj);

    render('home', $data);
})->name('update');