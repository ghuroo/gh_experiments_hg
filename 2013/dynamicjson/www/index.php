<?php
define('VERSION', '2.0.0');
define('ABSPATH', dirname(__FILE__));
require(ABSPATH . '/config.php');


/*************** EXCEPTIONS ***************/
$app->notFound(function () use ($app) { render('404',array()); });
$app->error(function (\Exception $e) use ($app) { $app->render('error'); });

/*************** APP INIT ***************/
$app->run();
