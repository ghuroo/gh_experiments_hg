<?php

/*************** SESSION ***************/
session_name('__SESSION');
session_start();

/*************** CONSTANTS ***************/
define('PRODUCTIONSERVER', false);

if(PRODUCTIONSERVER) {
	define('BASE', '/');
	define('DEBUGMODE', false);
}else{
	define('BASE', '/gh/experiments/dynamicjson/www/');
	define('DEBUGMODE', true);
}

define('ASSETS', BASE.'assets/');

define('EMAIL_FROM_ADDRESS', 'noreply@rd-agency.com');
define('EMAIL_TO_ADDRESS', 'kupgest@gmail.com');
define('EMAIL_FROM_NAME', 'TESTE');
define('EMAIL_HOST', 'mail.actualsales.info');
define('EMAIL_USERNAME', 'mx@actualsales.info');
define('EMAIL_PASSWORD', 'sonsire');
define('EMAIL_ASSETS', 'http://rd-agency.com/assets/img/');
define('ANALYTICS', '');

// Aux paths
if(!defined('ABSPATH')){define('ABSPATH', dirname(__FILE__));}
define('LIBPATH', ABSPATH . '/libs');
define('VENDORPATH', ABSPATH . '/vendor');
define('MODELSPATH', ABSPATH . '/models');
define('VIEWSPATH', ABSPATH . '/views');
define('CONTROLLERPATH', ABSPATH . '/controllers');
define('LANGPATH', ABSPATH . '/lang');


/*************** DEBUGGING ***************/
DEBUGMODE ?
ini_set('display_errors', '1') :
ini_set('display_errors', 0);

/*************** LOAD SLIM ***************/
// Slim PHP
require(VENDORPATH . '/Slim/Slim.php');
\Slim\Slim::registerAutoloader();

// Twig
require(VENDORPATH . '/Twig/Autoloader.php');
Twig_Autoloader::register();

// Twig [Template]
require(VENDORPATH . '/Slim-Extras/Views/Twig.php');

/*************** APP MULTILANG SETUP ***************/
$availableLangs = array('pt');
foreach (glob(VENDORPATH.'/Slim-Multilingual/app/lib/*.php') as $filename) { require_once $filename; }

$app = new MultilingualSlim(array(
	'session.handler' => null,
	'debug' => DEBUGMODE,
	'templates.path' => realpath(VIEWSPATH),
	'log.enable' => true,
	'log.path' => ABSPATH.'/Logs',
	'log.level' => 4
));
$translator = new Translator($app->getLog(), ABSPATH . '/lang');
$app->view(new MultilingualView($app, $translator));

/*************** APP CONFIG ***************/
\Slim\Extras\Views\Twig::$twigOptions = array( 'debug' => true );
\Slim\Extras\Views\Twig::$twigExtensions = array( 'Twig_Extensions_Slim' );
\Slim\Extras\Views\Twig::$twigTemplateDirs = array( realpath(VIEWSPATH) );

$app->getLog()->setEnabled(true);

/*************** PUBLIC LIBS ***************/
require(VENDORPATH . '/RedBean/rb.php');
require(VENDORPATH . '/PHPMailer/class.phpmailer.php');

/*************** MY LIBS ***************/
foreach (glob(LIBPATH . '/*.php') as $filename) { require_once $filename; }

/*************** MODELS ***************/
require(MODELSPATH . '/MyModel.php');
foreach (glob(MODELSPATH . '/*.php') as $filename) { require_once $filename; }

/*************** CONTROLLERS ***************/
foreach (glob(CONTROLLERPATH . '/*.php') as $filename) { require_once $filename; }

/*************** MYSQL CONFIG **************/
PRODUCTIONSERVER ?
R::setup('mysql:host=db.rd-agency.com;dbname=dynamicjson','rd01','pqrSD3q23') :
R::setup('mysql:host=localhost;dbname=dynamicjson','root','');
R::setStrictTyping(false);

require(VENDORPATH.'/Slim-Multilingual/app/hooks.php');
