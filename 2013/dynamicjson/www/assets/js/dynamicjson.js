FazJSON = {
	json: [],
	currentTitle: null,
	currentText: null,
	currentKey1: null,
	currentKey2: null,
	item: null,

	values: {
		get: function(_el) {
			FazJSON.currentKey1 = _el.children('td').children('input').eq(0).val();
			FazJSON.currentKey2 = _el.children('td').children('input').eq(1).val();

			FazJSON.currentValue1 = _el.children('td').children('textarea').eq(0).val();
			FazJSON.currentValue2 = _el.children('td').children('textarea').eq(1).val();
		},

		attach: function() {
			FazJSON.item = {};
			FazJSON.item[FazJSON.currentKey1] = FazJSON.currentValue1;
			FazJSON.item[FazJSON.currentKey2] = FazJSON.currentValue2;

			FazJSON.json.push(FazJSON.item);
		}
	},

	init: function(_el) {
		this.json.length = 0;

		_el.each(function() {
			FazJSON.values.get($(this));
			FazJSON.values.attach($(this));
		});

		return JSON.stringify(FazJSON.json);
	}

};

/* Submit */
$('form#update').on('submit', function(e) {
	e.preventDefault();
	json = FazJSON.init($('table#object tr'));
	console.log('\n\n'+json);
	identifier = $('.identifier').attr('id');
	
	$.ajax({
	    type: "POST",
	    url: $(this).attr('action'),
	    data: { id: identifier, content: json },
	    dataType: "json",
	    success: function(data){ console.log('\n\nSuccess.'); },
	    failure: function(errMsg) {
	        alert(errMsg);
	    }
	});

});

/* Add */
$('.btn.add').on('click', function() {
	var rows = $('table#object tr').length;
	var new_row = parseInt($('table#object tr').eq(rows-1).attr('id').split('-').pop())+1;

	var content = $('table#object tr').eq(rows-1).html();

	$('table#object tbody').append('<tr id="row-'+(rows+1)+'">'+content+'</tr>');
	$('table#object tr').eq(rows).children('td').children('input').attr('value', 'key');
	$('table#object tr').eq(rows).children('td').children('textarea').html('value');
});

var ola;

/* Delete */
$('.btn.delete').on('click', function() {
	ola = $(this).parents('tr');
	$(this).parents('tr').remove();
});

